SDP-Project-Group-4
===================

![Group4 logo](group_4_logo.jpg)


So far, most of the stuff in this repository is simply copied from last year's group 9.

ArduinoProject is the client program that runs on the XinoRF Arduino Microcontroller.

To use the Arduino IDE on DICE, download the file "arduino-$1.5.8-linux64.tgz".
After extracting the contents, rename the folder, and change the name from 
"arduino-${version} to" arduino-$1.5.8. This should work on any DICE machine (not just the vision ones).

When plugging in the Arduino's USB cable of the wireless SRF-stick, they are usually named
/dev/ttyACM0 or /dev/ttyACM1.


Instructions for Starting the Vision System:
===================

* Checking the computer works with the camera:
    1. Open VLC media player from "Applications > Sound & Video > VLC media player"
	2. Open the Stream menu from "Media > Stream..."
	3. Switch to the "Capture Device" tab.
	4. In the "Device Selection" panel, set the Video device name to "/dev/video0" from the drop down menu.
	5. Leave the other values on the defaults of "Video for Linux 2" for the capture mode, adn "Undefined" for the video standard.
	6. Click the little arrow next to the "Stream" button, and select "Play" from the drop down menu.
	7. If it works, you should see live footage of the pitch on screen.
	8. If it does not work (e.g. it shows an error message, or has a completely green or black screen), then switch to a different computer.


* Setting up the LD_LIBRARY_PATH variable in eclipse
	1. Open Eclipsefrom "Applications > Programming > Eclipse 4.2"
	2. If you have cloned the repository correctly and imported it into eclipse, you should see 4 projects.
	3. Open the file "PcProject > src > pc.vision > RunVision.java".
	4. If you run this file now, you will get an error message saying something like: `Cant load v4l4j JNI library Exception in thread "main" java.lang.UnsatisfiedLinkError: /afs/inf.ed.ac.uk/user/s12/s1203531/git/SDP/PcProject/lib/libv4l4j.so: libvideo.so.0: cannot open shared object file: No such file or directory`
    5. To fix this, click the little arrow next to Eclipse's run button, and select "Run Configurations..." from the dropdown menu.
    6. Change to the "Environment" tab
    7. Click the "New..." button to add a new environmental variable.
    8. Give this variable the name "LD_LIBRARY_PATH". If there was already a variable with this name, edit the existing one rather than adding a new one.
    9. In the variable's "Value:" box, you will need to put the path to the "lib" folder of the PC project for your system. On mine, this is at "/afs/inf.ed.ac.uk/user/s12/s1203531/git/SDP/PcProject/lib" , so it is likely that it will be very similar on yours.
    10. You should now be able to run the "RunVision.java" file again, and see a live feed of the pitch on screen. This means it is working.
    11. If it is not working, then it will hopefully have an error message explaining why on the Eclipse terminal. On a normal run, all it says is `[ libvideo.c:68 ] Using libvideo version 0.8-10` in red text. Anything else is likely to indicate some other error. If the video feed works in VLC (see step1, then it is likely a problem with the project's code rather than the camera).




ReadMe from 2014 Group 9:
===================
BrickProject is the client program to run on an NXT brick flashed with LeJOS 0.9.1.

PcProject is the server application to run on a DICE machine with a usb bluetooth dongle inserted. To start the server you should run the RunVision class in Eclipse.

SharedLib is simply a library project that is shared between the Brick and Pc Projects.

In order to get the v4l4j library to see the libvideo.so.0 shared object file you will need to change LD_LIBRARY_PATH to point to the folder on your DICE login that contains the shared object file (namely the lib folder of the PcProject). This can either be done within Eclipse by altering the Run Configuration for RunVision, or outside of Eclipse by creating a new shortcut to Eclipse 4.2 with "env LD_LIBRARY_PATH=[your path to file] /opt/eclipse4.2/eclipse" as the command.

Note: The final presentation was created in Office 2013, and will NOT show correctly in LibreOffice or the like.
