package pc.util;

public class CharUtils {

	public static String intToCharacterString(int value){
		char c = (char) value;
		return charToString(c);
	}
	
	public static String charToString(char c){
		switch(c){
		case '\n' : return "\\n";
		case '\t' : return "\\t";
		case '\r' : return "\\r";
		case '\0' : return "\\0";
		default : return ""+c;
		}
	}
}
