package pc.util;

import java.nio.ByteBuffer;

/**
 * A utility class containing useful functions for
 * converting to and from byte arrays. Mainly used by comms code
 * when sending bytes to the robot.
 */
public class ByteArrayUtils {
	
	/**
	 * Convert the given float to a 4 byte array.
	 */
	public static byte[] floatToByteArray(float value) {
		byte[] bytes = new byte[4];
		ByteBuffer.wrap(bytes).putFloat(value);

		for (int i = 0; i < 2; i++) {
			byte temp = bytes[i];
			bytes[i] = bytes[3 - i];
			bytes[3 - i] = temp;
		}

		return bytes;
	}

	/**
	 * Convert the given 4 byte array to a float.
	 */
	public static float byteArrayToFloat(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getFloat();
	}
}
