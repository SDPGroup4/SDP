package pc.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

/**
 * A utility class for watching what is being
 * sent to an OutputStream by mirroring all
 * output to a PrintStream. Used for debugging IO.
 */
public class SplittingDataStream extends OutputStream {

	//The two streams to write to.
	private final OutputStream realOutput;
	private final PrintStream debugOutput;

	/**
	 * Wrap the given OutputStream so that writing to it will
	 * mirror what is sent to the debug stream.
	 * @param out The real output stream that data needs written to.
	 * @param debugOut A PrintStream to display what is being written (usually System.out).
	 */
	public SplittingDataStream(OutputStream out, PrintStream debugOut) {
		super();
		this.realOutput = out;
		this.debugOutput = debugOut;
	}

	/**
	 * Write the given int to the real stream, while
	 * echoing it to the debug stream (one int per line).
	 */
	@Override
	public void write(int b) throws IOException {
		if(realOutput != null){
			realOutput.write(b);
		}
		if(debugOutput != null){
			debugOutput.print('\n');
			debugOutput.print(b);
		}
	}

}
