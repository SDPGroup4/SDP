/**
 * Name : MovingObject.java
 * Author : Dimitar Petrov
 * Description : Stores data relating to an object capable of movement:
 * Coordinates, velocity, angular orientation
 * */
package pc.world.oldmodel;

import java.awt.geom.Point2D;

public class MovingObject {
	//x,y, representation on the grid
	//x,y are mm representations
	public float x;
	public float y;
	
	public double velocity;
	
	//Orientation coordinates
	public int orientationDegrees;
	
	public MovingObject(float x, float y, float degrees){
		this(x, y, (int) degrees);
	}
	
	/**
	 * Initializes a moving object
	 * @param x represents the X coordinate
	 * @param y represents the Y coordinate
	 * @param degrees represents the orientation angle of the object
	 * */
	public MovingObject(float x, float y, int degrees){
		this.x = x;
		this.y = y;
		
		while(degrees > 180){
			degrees -= 360;
		}
		while(degrees < -180){
			degrees += 360;
		}
		
		this.orientationDegrees = degrees;

	}
	
	/**
	 * Initializes a moving object
	 * @param x represents the X coordinate
	 * @param y represents the Y coordinate
	 * */
	public MovingObject(float x, float y){
		this.x = x;
		this.y = y;
	}
	
	//rotating point of robots, need to represent
	//robot dimension extension from plates
	
	public Point2D asPoint() {
		return new Point2D.Double(x, y);
	}
}
