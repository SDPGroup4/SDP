package pc.world;

import java.awt.Point;

import pc.movement.DistanceThreshold;
import pc.vision.PitchConstants;
import pc.world.oldmodel.MovingObject;
import pc.world.oldmodel.WorldState;


/**
 * A useful object that represents a single are of the pitch.
 * 
 * It stores information such as the zone boundaries, zone centre,
 * and type of zone (e.g. ATTACKER or ENEMY_ATTACKER).
 * 
 * It is used to hide the fact that the robot might be in different
 * positions and on different teams, and avoid having loads of calls
 * to worldState.weAreShootingRight all over the strategy code.
 * 
 * Using this class (or the corresponding enum values), this
 * information can be stored in a much more manageable way,
 * and allows the robot strategy code to act in such a way that
 * it does not need to know where the robot is or which way it is shooting
 * to make every decision.
 */
public class PitchZone{
	//The 4 types of zone on the pitch that the robot can be in.
	//These are used to describe zones in a way that
	//does not depend which way we are shooting.
	public static enum PitchZoneType {
		ATTACKER("ATTACKER"),
		DEFENDER("DEFENDER"),
		ENEMY_ATTACKER("ENEMY_ATTACKER"),
		ENEMY_DEFENDER("ENEMY_DEFENDER");
		
		private final String name;
		private PitchZoneType(String name){
			this.name = name;
		}
		@Override
		public String toString() {
			return name;
		}
	}
	
	//The edges of the zone.
	public int leftEdge;
	public int rightEdge;
	public int topEdge;
	public int bottomEdge;
	
	//The zone's central point.
	private final Point zoneOrigin;

	
	/**
	 * Using the current world state, set up a zone
	 * representing an area of the pitch of the given type.
	 * 
	 * E.g. create a representation of the ATTACKER zone, or
	 * the robot's current zone.
	 * 
	 * @param zoneType Which area of the pitch this zone represents (e.g. ATTACKER, ENEMY_ATTACKER)
	 * @param worldState The current state of the world. Allows dynamic switching of sides or pitch resizing.
	 */
	public PitchZone(PitchZoneType zoneType, WorldState worldState){
		calculateZoneEdges(zoneType, worldState);
		
		int resetY = (topEdge + bottomEdge) / 2;
		int resetX = (leftEdge + rightEdge) / 2;
		
		zoneOrigin = new Point(resetX, resetY);
	}
	
	/**
	 * Get the central point of the zone that the robot treats as its origin.
	 */
	public Point getOriginPoint(){
		return zoneOrigin;
	}
	

	/**
	 * Work out and store the boundaries of the given zone type
	 * in a way that takes into account the fact we may be shooting to
	 * either side.
	 * @param zoneType Which area of the pitch this zone represents (e.g. ATTACKER, ENEMY_ATTACKER)
	 * @param worldState The current state of the world. Allows dynamic switching of sides or pitch resizing.
	 */
	private void calculateZoneEdges(PitchZoneType zoneType, WorldState worldState){
		topEdge = PitchConstants.getPitchOutlineTop();
		bottomEdge = PitchConstants.getPitchOutlineBottom();
		
		
		if(worldState.weAreShootingRight){
			switch(zoneType){
			case DEFENDER:
				leftEdge = PitchConstants.getPitchOutlineLeft();
				rightEdge = worldState.dividers[0];
				break;
			case ENEMY_ATTACKER:
				leftEdge = worldState.dividers[0];
				rightEdge = worldState.dividers[1];
				break;
			case ATTACKER:
				leftEdge = worldState.dividers[1];
				rightEdge = worldState.dividers[2];
				break;
			case ENEMY_DEFENDER:
				leftEdge = worldState.dividers[2];
				rightEdge = PitchConstants.getPitchOutlineRight();
				break;
			}
		}else{
			switch(zoneType){
			case ENEMY_DEFENDER:
				leftEdge = PitchConstants.getPitchOutlineLeft();
				rightEdge = worldState.dividers[0];
				break;
			case ATTACKER:
				leftEdge = worldState.dividers[0];
				rightEdge = worldState.dividers[1];
				break;
			case ENEMY_ATTACKER:
				leftEdge = worldState.dividers[1];
				rightEdge = worldState.dividers[2];
				break;
			case DEFENDER:
				leftEdge = worldState.dividers[2];
				rightEdge = PitchConstants.getPitchOutlineRight();
				break;
			}
		}
	}
	
	/**
	 * Determine whether the given moving object is in this zone.
	 */
	public boolean isInZone(MovingObject obj){
		return obj != null ? isInZone(obj.x, obj.y) : false;
	}
	
	public boolean isInZoneIgnoreHeight(MovingObject obj){
		return obj != null ? isInZoneIgnoreHeight(obj.x) : false;
	}
	
	/**
	 * Determine whether the given point is within this zone.
	 */
	public boolean isInZone(Point point){
		return point != null ? isInZone(point.x, point.y) : false;
	}
	
	public boolean isInZoneIgnoreHeight(Point point){
		return point != null ? isInZoneIgnoreHeight(point.x) : false;
	}
	
	/**
	 * Determine whether the given coordinates are within this zone.
	 */
	public boolean isInZone(float x, float y){
		return		x > leftEdge 
				&& 	x < rightEdge 
				&& 	y > Math.min(topEdge, bottomEdge) 
				&&	y < Math.max(topEdge, bottomEdge);
	}
	
	public boolean isInZoneIgnoreHeight(float x){
		//System.out.println("Left edge "+leftEdge+" right edge "+rightEdge);
		return		x > leftEdge 
				&& 	x < rightEdge;
	}
	
	public int getRunUpXCoordinateForKickingTowards(PitchZone shootingToZone){
		int offsetFromCentre = DistanceThreshold.ROUGH.getThreshold();
		
		int ourZoneX = getOriginPoint().x;
		int allyZoneX = shootingToZone.getOriginPoint().x;
		
		offsetFromCentre = ourZoneX > allyZoneX ?  offsetFromCentre : -offsetFromCentre;
		return ourZoneX + offsetFromCentre;
	}
	
	// getter method of the left side (goal) of the pitch
	public int getLeftEdge(){
		int leftEdge = this.leftEdge;
		return leftEdge;
	}
	
	// getter method of the right side (goal of the pitch)
	public int getRightEdge(){
		int rightEdge = this.rightEdge;
		return rightEdge;
	}
	
}



