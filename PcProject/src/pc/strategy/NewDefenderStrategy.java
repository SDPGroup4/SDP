//package pc.strategy;
//
//import pc.command.RobotCommand;
//import pc.strategynew.StrategyController;
//import pc.strategynew.operation.Operation;
//import pc.vision.PitchConstants;
//import pc.world.oldmodel.WorldState;
//
//public class NewDefenderStrategy extends GeneralStrategy{
//
//	private static final int KICK_TIMEOUT = 120000000;
//
//	private BrickCommServer brick;
//	private ControlThread controlThread;
//	private boolean stopControlThread;
//	private boolean ballInEnemyAttackerArea = false;
//	private boolean justCaught = true;
//	private boolean fromSide = false;
//	private boolean timerOn = false;
//	private long kickTimer = 0;
//
//	public NewDefenderStrategy(BrickCommServer brick) {
//		System.out.println("New defender strategy.");
//		this.brick = brick;
//		controlThread = new ControlThread();
//	}
//
//	@Override
//	public void stopControlThread() {
//		stopControlThread = true;
//	}
//
//	@Override
//	public void startControlThread() {
//		stopControlThread = false;
//		controlThread.start();
//	}
//
//	@Override
//	public void sendWorldState(WorldState worldState) {
//		super.sendWorldState(worldState);
//
//		if (	worldState.weAreShootingRight && ballX > defenderCheck && ballX < leftCheck 
//			|| !worldState.weAreShootingRight && ballX < defenderCheck && ballX > rightCheck) {
//			this.ballInEnemyAttackerArea = true;
//		} else {
//			this.ballInEnemyAttackerArea = false;
//		}
//		
//		if (	(worldState.weAreShootingRight && ballX > defenderCheck)
//			|| (!worldState.weAreShootingRight && ballX < defenderCheck)){
//				//(ballX < leftCheck || ballX > rightCheck) && !ballInEnemyAttackerArea) {
//			synchronized (controlThread) {
//				controlThread.operation.op = Operation.OperationType.DO_NOTHING;
//			}
//			return;
//		}
//		if (Math.abs(ballY - PitchConstants.getPitchOutlineTop()) < 20 || Math.abs(ballY - PitchConstants.getPitchOutlineBottom()) < 20 ) {
//			fromSide = true;
//		} else {
//			fromSide = false;
//		}
//		synchronized (controlThread) {
//			if (ballInEnemyAttackerArea) {
//				controlThread.operation = returnToOrigin(RobotType.DEFENDER);
//			} else {
//				if (!ballCaughtAttacker) {
//					controlThread.operation = catchBall(RobotType.DEFENDER);
//					justCaught = true;
//					timerOn = false;
//				} else {
//					controlThread.operation = scoreGoal(RobotType.DEFENDER);
//					if (!timerOn) {
//						timerOn = true;
//						kickTimer = System.currentTimeMillis();
//					}
//					if (justCaught && fromSide) {
//
//						controlThread.operation.op = Operation.OperationType.DEFROTATE;
//						controlThread.operation.rotateBy = (int) calculateAngle(attackerRobotX, attackerRobotY, attackerOrientation, leftCheck, attackerRobotY);
//						controlThread.operation.rotateSpeed = 50;
//						// changed thresh
//						if (Math.abs(controlThread.operation.rotateBy) < 5) {
//							controlThread.operation.op = Operation.OperationType.DO_NOTHING;
//						}
//						if (controlThread.operation.op == Operation.OperationType.DO_NOTHING) {
//							justCaught = false;
//						}
//					}
//
//				}
//				// kicks if detected false catch
//				if ((timerOn && (System.currentTimeMillis() - kickTimer) > KICK_TIMEOUT) || (ballCaughtAttacker
//						&& (Math.hypot(ballX - attackerRobotX, ballY
//								- attackerRobotY) > 60) && !worldState.ballNotOnPitch)) {
//					controlThread.operation.op = Operation.OperationType.DEFKICK;
//				}
//			}
//		}
//	}
//
//	private class ControlThread extends Thread {
//		public Operation operation = new Operation();
//		private long lastKickerEventTime = 0;
//
//		public ControlThread() {
//			super("Robot control thread");
//			setDaemon(true);
//		}
//
//		@Override
//		public void run() {
//			try {
//				//					Operation.Type prevOp = null;
//				while (!stopControlThread) {
//					int travelDist, rotateBy, rotateSpeed, travelSpeed;
//					Operation.OperationType op;
//					double radius;
//					synchronized (this) {
//						op = this.operation.op;
//						rotateBy = this.operation.rotateBy;
//						rotateSpeed = this.operation.rotateSpeed;
//						travelDist = this.operation.travelDistance;
//						travelSpeed = this.operation.travelSpeed;
//						radius = this.operation.radius;
//						showDebugMessageOnStrategyTick("Operation: "+this.operation);
//					}
//
//					//						if (prevOp != null) {
//					//							if (!op.equals(prevOp)){
//					//								showDebugMessageOnFrameUpdate("justCaught: " + justCaught + " op: " + op.toString() + " rotateBy: "
//					//									+ rotateBy + " travelDist: " + travelDist
//					//									+ "radius: " + radius);
//					//							}
//					//						}
//					//						prevOp = op;
//
//					switch (op) {
//					case DO_NOTHING:
//						break;
//					case DEFCATCH:
//						if (System.currentTimeMillis() - lastKickerEventTime > 1000) {
//							brick.execute(new RobotCommand.Catch());
//							ballCaughtAttacker = true;
//							lastKickerEventTime = System.currentTimeMillis();
//						}
//						break;
//					case DEFKICK:
//						if (System.currentTimeMillis() - lastKickerEventTime > 1000) {
//							brick.execute(new RobotCommand.Kick(100));
//							ballCaughtAttacker = false;
//							lastKickerEventTime = System.currentTimeMillis();
//						}
//						break;
//					case DEFROTATE:
//						brick.execute(new RobotCommand.Rotate(-rotateBy,
//								rotateSpeed));
//						break;
//					case DEFTRAVEL:
//						brick.execute(new RobotCommand.Travel(travelDist,
//								travelSpeed));
//						break;
//					case DEFARC_LEFT:
//						brick.execute(new RobotCommand.TravelArc(radius,
//								travelDist, travelSpeed));
//						break;
//					case DEFARC_RIGHT:
//						brick.execute(new RobotCommand.TravelArc(-radius,
//								travelDist, travelSpeed));
//						break;
//					case ATKMOVEKICK:
//						if (System.currentTimeMillis() - lastKickerEventTime > 1000) {
//							brick.execute(new RobotCommand.Travel(100, 10000));
//							brick.execute(new RobotCommand.Kick(100));
//							ballCaughtAttacker = false;
//							lastKickerEventTime = System.currentTimeMillis();
//						}
//						break;
//					default:
//						break;
//					}
//					Thread.sleep(StrategyController.STRATEGY_TICK);
//				}
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//
//		}
//
//	}
//
//
//
//}
