//package pc.strategy;
//
//public class Operation {
//
//	public enum Type{
//		DO_NOTHING("DO_NOTHING"), 
//		ATKTRAVEL("ATKTRAVEL"), 
//		ATKMOVEKICK("ATKMOVEKICK"), 
//		ATKROTATE("ATKROTATE"), 
//		ATKPREPARE_CATCH("ATKPREPARE"), 
//		ATKCATCH("ATKCATCH"), 
//		ATKKICK("ATKKICK"), 
//		ATKARC_LEFT("ATKARC_LEFT"), 
//		ATKARC_RIGHT("ATKARC_RIGHT"), 
//		DEFTRAVEL("DEFTRAVEL"), 
//		DEFROTATE("DEFROTATE"), 
//		DEFPREPARE_CATCH("DEFPREPARE_CATCH"), 
//		DEFCATCH("DEFCATCH"), 
//		DEFKICK("DEFKICK"), 
//		DEFCONFUSEKICK("DEFCONFUSEKICK"), 
//		DEFKICKSTRONG("DEFKICKSTRING"), 
//		ROTATENMOVE("ROTATENMOVE"), 
//		MOVENROTATE("MOVENROTATE"), 
//		DEFARC_LEFT("DEFARC_LEFT"), 
//		DEFARC_RIGHT("DEFARC_RIGHT"), 
//		ATKCONFUSEKICKRIGHT("ATKCONFUSEKICKRIGHT"), 
//		ATKCONFUSEKICKLEFT("ATKCONFUSEKICKLEFT");
//		
//		private final String name;
//		private Type(String name){
//			this.name = name;
//		}
//		@Override
//		public String toString() {
//			return name;
//		}
//	}
//
//	
//	public Type op = Type.DO_NOTHING;
//	public double radius;
//	public int travelDistance, travelSpeed, rotateBy, rotateSpeed;
//	public String message = "";
//	
//	
//	@Override
//	public String toString() {
//		return message+" "+op.toString()+" dist "+travelDistance+" speed "+travelSpeed+" rot "+rotateBy+" rotSpeed"+rotateSpeed;
//	}
//	// rotateBy is cast as degrees in 
//	// calculateAngle(float robotX, float robotY, float robotOrientation,
//	//                float targetX, float targetY) of GeneralStrategy
//	// rotateSpeed is commonly the absolute value of the angle to the ball
//	// travelSpeed is the absolute value of the distance to the ball
//	
//}
