//package pc.strategy;
//
//import pc.prediction.Calculations;
//import pc.strategy.interfaces.Strategy;
//import pc.strategynew.StrategyController;
//import pc.strategynew.operation.Operation;
//import pc.strategynew.operation.Operation.OperationType;
//import pc.vision.PitchConstants;
//import pc.world.oldmodel.WorldState;
//
//public class GeneralStrategy implements Strategy {
//
//	private static final int ATTACKER_SPEED_CONSTANT = 50;
//	private static final int DEFENDER_SPEED_CONSTANT = 0;
//	private static final double CATCHER_DIST = 50;
//	private static final int CATCHER_THRESH = 40;
//
//	protected ControlThread controlThread;
//	protected float attackerRobotX;
//	protected float attackerRobotY;
//	protected float defenderRobotX;
//	protected float defenderRobotY;
//	protected float enemyAttackerRobotX;
//	protected float enemyAttackerRobotY;
//	protected float enemyDefenderRobotX;
//	protected float enemyDefenderRobotY;
//	protected float ballX;
//	protected float ballY;
//	protected float defenderOrientation;
//	protected float attackerOrientation;
//	protected float enemyAttackerOrientation;
//	protected int leftCheck;
//	protected int rightCheck;
//	protected int defenderCheck;
//	protected int topOfPitch;
//	protected int botOfPitch;
//	protected float goalX;
//	protected float ourGoalX;
//	protected float[] goalY;
//	protected float[] ourGoalY;
//	protected float[] ourGoalEdges = new float[3];
//	protected int topY;
//	protected int bottomY;
//	protected float defenderResetX;
//	protected float defenderResetY;
//	protected float attackerResetX;
//	protected float attackerResetY;
//	protected boolean ballCaughtDefender;
//	protected boolean ballCaughtAttacker;
//	protected boolean attackerHasArrived;
//	protected boolean passingAttackerHasArrived;
//	protected boolean defenderHasArrived;
//	protected boolean isBallCatchable;
//	protected boolean scoringAttackerHasArrived;
//	protected boolean enemyDefenderNotOnPitch;
//	protected boolean attackerNotOnPitch;
//	private int BOUNCE_SHOT_DISTANCE = 50;
//
//	@Override
//	public void stopControlThread() {
//		controlThread.stop();
//	}
//
//	@Override
//	public void startControlThread() {
//		controlThread.start();
//	}
//
//	private class ControlThread extends Thread {
//
//		public ControlThread() {
//			super("Robot control thread");
//			setDaemon(true);
//		}
//
//		@Override
//		public void run() {
//		}
//	}
//
//	public enum RobotType {
//		ATTACKER, DEFENDER
//	}
//
//	public Operation catchBall(RobotType robot) {
//		showDebugMessageOnFrameUpdate("CATCH_BALL");
//		defenderHasArrived = false;
//		scoringAttackerHasArrived = false;
//		Operation toExecute = new Operation();
//		toExecute.message = "CATCH_BALL ";
//		boolean isAttacker = robot == RobotType.ATTACKER;
//		isBallCatchable = true;
//
//		double distanceToBall = isAttacker ? Math.hypot(ballX - attackerRobotX,
//				ballY - attackerRobotY) : Math.hypot(ballX - defenderRobotX,
//						ballY - defenderRobotY);
//		double angToBall = isAttacker ? calculateAngle(attackerRobotX,
//				attackerRobotY, attackerOrientation, ballX, ballY)
//				: calculateAngle(defenderRobotX, defenderRobotY,
//						defenderOrientation, ballX, ballY);
//		double catchDist = CATCHER_DIST;
//		int catchThresh = CATCHER_THRESH;
//		float targetY = ballY;
//		float targetX = ballX;
//		double slope = 0;
//		float c = (float) (ballY - slope * ballX);
//		int ballDistFromTop = (int) Math.abs(ballY
//				- PitchConstants.getPitchOutlineTop());
//		int ballDistFromBot = (int) Math.abs(ballY
//				- PitchConstants.getPitchOutlineBottom());
//		// attacker's case
//		if (isAttacker) {
//			if (ballDistFromBot < 20) {
//				targetY = ballY - 40;
//				catchDist = CATCHER_DIST;
//				catchThresh = CATCHER_THRESH;
//				//if (Math.abs(leftCheck - ballX) < 15
//				//		|| Math.abs(rightCheck - ballX) < 15) {
//				//	isBallCatchable = false;
//				//}
//			} else if (ballDistFromTop < 20) {
//				targetY = ballY + 40;
//				catchDist = CATCHER_DIST;
//				catchThresh = CATCHER_THRESH;
//				//if (Math.abs(leftCheck - ballX) < 15
//				//		|| Math.abs(rightCheck - ballX) < 15) {
//				//	isBallCatchable = false;
//				//}
//			} else {
//				attackerHasArrived = false;
//			}
//			if (!attackerHasArrived && isBallCatchable) {
//				showDebugMessageOnFrameUpdate("Travelling to ball");
//				toExecute = travelTo(robot, ballX, targetY, catchThresh);
//				if (toExecute.op == Operation.OperationType.DO_NOTHING) {
//					attackerHasArrived = true;
//				}
//			} else if (isBallCatchable) {
//				// was " > 2"; increasing angle threshold to avoid small turns
//				if (Math.abs(angToBall) > 5) {
//					showDebugMessageOnFrameUpdate("Rotating to ball");
//					toExecute.op = Operation.OperationType.ATKROTATE;
//					toExecute.rotateBy = (int) (isAttacker ? angToBall
//							: angToBall / 3);
//				} else if (Math.abs(distanceToBall) > catchDist) {
//					showDebugMessageOnFrameUpdate("Traveling to catch distance");
//					toExecute.op = Operation.OperationType.ATKTRAVEL;
//					toExecute.travelDistance = (int) (isAttacker ? distanceToBall
//							: distanceToBall / 3);
//					toExecute.travelSpeed = (int) (isAttacker ? Math
//							.abs(distanceToBall) : Math.abs(distanceToBall) / 3);
//				}
//				toExecute.rotateSpeed = (int) (isAttacker ? Math.abs(angToBall)
//						: Math.abs(angToBall));
//			}
//
//			// defender's case
//		} else {
//			passingAttackerHasArrived = false;
//			if (Math.abs(defenderCheck - ballX) > 20
//					&& toExecute.op == Operation.OperationType.DO_NOTHING) {
//				toExecute = travelTo(robot, ballX, ballY, catchThresh - 2);
//			}
//		}
//		if (toExecute.op == Operation.OperationType.DO_NOTHING && isBallCatchable
//				&& Math.abs(defenderCheck - ballX) > 25) {
//			showDebugMessageOnFrameUpdate("Attempting to catch");
//			toExecute.op = isAttacker ? Operation.OperationType.ATKCATCH
//					: Operation.OperationType.DEFCATCH;
//		}
//		return toExecute;
//	}
//
//	public Operation scoreGoal(RobotType robot) {
//
//		attackerHasArrived = false;
//		showDebugMessageOnFrameUpdate("SCORE_GOAL");
//		Operation toExecute = new Operation();
//		toExecute.message = "SCORE_GOAL ";
//
//		float toTravelY;
//		boolean isInCenter;
//		int distanceFromTop = (int) Math.abs(attackerRobotY
//				- PitchConstants.getPitchOutlineTop());
//		int distanceFromBot = (int) Math.abs(attackerRobotY
//				- PitchConstants.getPitchOutlineBottom());
//		if ((Math.abs(enemyDefenderRobotX - rightCheck) < BOUNCE_SHOT_DISTANCE || Math
//				.abs(enemyDefenderRobotX - leftCheck) < BOUNCE_SHOT_DISTANCE)
//				&& (enemyDefenderRobotY < goalY[2] + 15 && enemyDefenderRobotY > goalY[0] - 15)) {
//			isInCenter = false;
//			if (distanceFromBot > distanceFromTop) {
//				toTravelY = goalY[0] - 60;
//			} else {
//				toTravelY = goalY[2] + 60;
//			}
//		} else {
//			toTravelY = goalY[1];
//			isInCenter = true;
//		}
//
//		// If bounce shots aren't enabled, always go to the centre.
//		if (!StrategyController.bounceShotEnabled) {
//			toTravelY = goalY[1];
//		}
//		if (!scoringAttackerHasArrived) {
//			toExecute = travelToNoArc(robot, (leftCheck + rightCheck) / 2,
//					toTravelY, 20);
//			if (toExecute.op == Operation.OperationType.DO_NOTHING) {
//				scoringAttackerHasArrived = true;
//			}
//		}
//		if (toExecute.op == Operation.OperationType.DO_NOTHING) {
//			float aimY = goalY[1];
//			if (robot == RobotType.ATTACKER) {
//				// Determine which side of the goal we should shoot at, and
//				// which way
//				// we should fake shot.
//				if (enemyDefenderRobotY > goalY[1]) {
//					aimY = goalY[0];
//					toExecute.op = (goalX == 640) ? OperationType.ATKCONFUSEKICKRIGHT
//							: OperationType.ATKCONFUSEKICKLEFT;
//				} else {
//					aimY = goalY[2];
//					toExecute.op = (goalX == 640) ? OperationType.ATKCONFUSEKICKLEFT
//							: OperationType.ATKCONFUSEKICKRIGHT;
//				}
//
//				// If the enemy defender is not on pitch, or we don't want you
//				// to do it, then don't fake shot.
//				if (enemyDefenderNotOnPitch
//						|| !StrategyController.confusionEnabled) {
//					toExecute.op = OperationType.ATKMOVEKICK;
//				}
//
//				// Straight forward case
//				double ang1 = calculateAngle(attackerRobotX, attackerRobotY,
//						attackerOrientation, goalX, aimY);
//
//				// Cases for when the defending robot is close to the line and
//				// we need to try
//				// a bounce shot against the wall. If we are doing a bounce shot
//				// there is no need to fake.
//				if (StrategyController.bounceShotEnabled && !isInCenter) {
//					float goalTarget = goalY[1];
//					boolean doBounce = false;
//					if (distanceFromBot > distanceFromTop) {
//						if (Math.abs(enemyDefenderRobotX - rightCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY > goalY[0])) {
//							goalTarget = goalY[0];
//							doBounce = true;
//						} else if (Math.abs(enemyDefenderRobotX - leftCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY > goalY[0])) {
//							goalTarget = goalY[0];
//							doBounce = true;
//						}
//					} else {
//						if (Math.abs(enemyDefenderRobotX - rightCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY < goalY[2])) {
//							goalTarget = goalY[2];
//							doBounce = true;
//						} else if (Math.abs(enemyDefenderRobotX - leftCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY < goalY[2])) {
//							goalTarget = goalY[2];
//							doBounce = true;
//						}
//					}
//
//					if (doBounce){
//						ang1 = Calculations.GetBounceAngle(attackerRobotX,
//								attackerRobotY, attackerOrientation, goalX,
//								goalTarget, 0, goalY[1]);
//						toExecute.op = Operation.OperationType.ATKMOVEKICK;
//					}
//				}
//
//				// Check we are pointing in the correct direction to score.
//				// 2 degree threshold seems to work best.
//				// was " > 2"; increasing angle threshold
//				if (Math.abs(ang1) > 5) {
//					toExecute.op = Operation.OperationType.ATKROTATE;
//					toExecute.rotateBy = (int) ang1;
//					toExecute.rotateSpeed = (int) (Math.abs(ang1) * 1.5);
//				}
//			}else{
//
//				// If the enemy defender is not on pitch, or we don't want you
//				// to do it, then don't fake shot.
//				if (enemyDefenderNotOnPitch
//						|| !StrategyController.confusionEnabled) {
//					toExecute.op = OperationType.ATKMOVEKICK;
//				}
//
//				// Straight forward case
//				double ang1 = calculateAngle(attackerRobotX, attackerRobotY,
//						attackerOrientation, goalX, aimY);
//
//				// Cases for when the defending robot is close to the line and
//				// we need to try
//				// a bounce shot against the wall. If we are doing a bounce shot
//				// there is no need to fake.
//				if (StrategyController.bounceShotEnabled && !isInCenter) {
//					float goalTarget = goalY[1];
//					boolean doBounce = false;
//					if (distanceFromBot > distanceFromTop) {
//						if (Math.abs(enemyDefenderRobotX - rightCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY > goalY[0])) {
//							goalTarget = goalY[0];
//							doBounce = true;
//						} else if (Math.abs(enemyDefenderRobotX - leftCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY > goalY[0])) {
//							goalTarget = goalY[0];
//							doBounce = true;
//						}
//					} else {
//						if (Math.abs(enemyDefenderRobotX - rightCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY < goalY[2])) {
//							goalTarget = goalY[2];
//							doBounce = true;
//						} else if (Math.abs(enemyDefenderRobotX - leftCheck) < BOUNCE_SHOT_DISTANCE
//								&& (enemyDefenderRobotY < goalY[2])) {
//							goalTarget = goalY[2];
//							doBounce = true;
//						}
//					}
//
//					if (doBounce){
//						ang1 = Calculations.GetBounceAngle(attackerRobotX,
//								attackerRobotY, attackerOrientation, goalX,
//								goalTarget, 0, goalY[1]);
//						toExecute.op = Operation.OperationType.ATKMOVEKICK;
//					}
//				}
//
//				// Check we are pointing in the correct direction to score.
//				// 2 degree threshold seems to work best.
//				// was " > 2"; increasing angle threshold
//				if (Math.abs(ang1) > 5) {
//					toExecute.op = Operation.OperationType.DEFROTATE;
//					toExecute.rotateBy = (int) ang1;
//					toExecute.rotateSpeed = (int) (Math.abs(ang1) * 1.5);
//				}
//			}
//
//		}
//		return toExecute;
//	}
//
//
//public Operation travelToNoArc(RobotType robot, float travelToX,
//		float travelToY, float distThresh) {
//	showDebugMessageOnFrameUpdate("TRAVEL_TO_NO_ARC");
//	Operation toExecute = new Operation();
//	toExecute.message = "TRAVEL_TO_NO_ARC";
//	boolean isAttacker = robot == RobotType.ATTACKER;
//	double ang1 = isAttacker ? calculateAngle(attackerRobotX,
//			attackerRobotY, attackerOrientation, travelToX, travelToY)
//			: calculateAngle(defenderRobotX, defenderRobotY,
//					defenderOrientation, travelToX, travelToY);
//	//double dist = isAttacker ? Math.hypot(travelToX - attackerRobotX,
//	//		travelToY - attackerRobotY) : -((Math.hypot(travelToX
//	//		- defenderRobotX, travelToY - defenderRobotY)));
//	double dist = isAttacker ? Math.hypot(travelToX - attackerRobotX,
//			travelToY - attackerRobotY) : ((Math.hypot(travelToX
//					- defenderRobotX, travelToY - defenderRobotY)));
//	boolean haveArrived = (Math.abs(dist) < distThresh);
//	if (!haveArrived) {
//		if (Math.abs(ang1) > 90) {
//			// adding condition for ang1 > 5, in order to turn
//			if ((Math.abs(ang1) < 165) & (Math.abs(ang1) > 5)) {
//				toExecute.op = isAttacker ? Operation.OperationType.ATKROTATE
//						: Operation.OperationType.DEFROTATE;
//				if (ang1 > 0) {
//					ang1 = -(180 - ang1);
//				} else {
//					ang1 = -(-180 - ang1);
//				}
//				toExecute.rotateBy = (int) (isAttacker ? ang1 : ang1 / 3);
//				toExecute.rotateSpeed = (int) Math.abs(ang1);
//			} else if (Math.abs(dist) > distThresh) {
//				showDebugMessageOnFrameUpdate("Negative distance in TravelToNoArc");
//				toExecute.op = isAttacker ? Operation.OperationType.ATKTRAVEL
//						: Operation.OperationType.DEFTRAVEL;
//				toExecute.travelDistance = (int) (isAttacker ? -dist
//						: dist / 3);
//				toExecute.travelSpeed = (int) (isAttacker ? Math.abs(dist) * 3
//						: 30);
//			}
//		} else {
//			if (Math.abs(ang1) > 15) {
//				toExecute.op = isAttacker ? Operation.OperationType.ATKROTATE
//						: Operation.OperationType.DEFROTATE;
//				toExecute.rotateBy = (int) (isAttacker ? ang1 : ang1 / 3);
//				toExecute.rotateSpeed = (int) Math.abs(ang1);
//			} else if (Math.abs(dist) > distThresh) {
//				toExecute.op = isAttacker ? Operation.OperationType.ATKTRAVEL
//						: Operation.OperationType.DEFTRAVEL;
//				toExecute.travelDistance = (int) (isAttacker ? dist
//						: dist / 3);
//				toExecute.travelSpeed = (int) (isAttacker ? dist * 3 : dist);
//			}
//		}
//	}
//	return toExecute;
//
//}
//
//public Operation travelToNoArcNoReverse(RobotType robot, float travelToX,
//		float travelToY, float distThresh) {
//	showDebugMessageOnFrameUpdate("TRAVEL_TO_NO_ARC_NO_REVERSE");
//	Operation toExecute = new Operation();
//	boolean isAttacker = robot == RobotType.ATTACKER;
//	double ang1 = isAttacker ? calculateAngle(attackerRobotX,
//			attackerRobotY, attackerOrientation, travelToX, travelToY)
//			: calculateAngle(defenderRobotX, defenderRobotY,
//					defenderOrientation, travelToX, travelToY);
//	//double dist = isAttacker ? Math.hypot(travelToX - attackerRobotX,
//	//		travelToY - attackerRobotY) : -((Math.hypot(travelToX
//	//		- defenderRobotX, travelToY - defenderRobotY)));
//	double dist = isAttacker ? Math.hypot(travelToX - attackerRobotX,
//			travelToY - attackerRobotY) : ((Math.hypot(travelToX
//					- defenderRobotX, travelToY - defenderRobotY)));
//	boolean haveArrived = (Math.abs(dist) < distThresh);
//	if (!haveArrived) {
//		if (Math.abs(ang1) > 10) {
//			toExecute.op = isAttacker ? Operation.OperationType.ATKROTATE
//					: Operation.OperationType.DEFROTATE;
//			toExecute.rotateBy = (int) (isAttacker ? ang1 : ang1 / 3);
//			toExecute.rotateSpeed = (int) Math.abs(ang1) + 15;
//		} else if (Math.abs(dist) > distThresh) {
//			toExecute.op = isAttacker ? Operation.OperationType.ATKTRAVEL
//					: Operation.OperationType.DEFTRAVEL;
//			toExecute.travelDistance = (int) (isAttacker ? dist : dist / 3);
//			toExecute.travelSpeed = (int) (isAttacker ? dist * 3
//					: dist * 0.6);
//		}
//	}
//
//	return toExecute;
//
//}
//
//public Operation travelTo(RobotType robot, float travelToX,
//		float travelToY, float distThresh) {
//
//	showDebugMessageOnFrameUpdate("TRAVEL_TO");
//	Operation toExecute = new Operation();
//	boolean isAttacker = robot == RobotType.ATTACKER;
//	double ang1 = isAttacker ? calculateAngle(attackerRobotX,
//			attackerRobotY, attackerOrientation, travelToX, travelToY)
//			: calculateAngle(defenderRobotX, defenderRobotY,
//					defenderOrientation, travelToX, travelToY);
//	double dist = isAttacker ? Math.hypot(travelToX - attackerRobotX,
//			travelToY - attackerRobotY) : -((Math.hypot(travelToX
//					- defenderRobotX, travelToY - defenderRobotY)));
//	boolean haveArrived = (Math.abs(dist) < distThresh);
//	if (Math.abs(ang1) > 30) {
//		toExecute.op = isAttacker ? Operation.OperationType.ATKROTATE
//				: Operation.OperationType.DEFROTATE;
//		toExecute.rotateBy = (int) (isAttacker ? ang1 : (ang1 / 3));
//		toExecute.rotateSpeed = Math.abs(toExecute.rotateBy);
//	} else if (!haveArrived) {
//		toExecute.travelSpeed = isAttacker ? (int) (Math.abs(dist) * 1.5)
//				+ ATTACKER_SPEED_CONSTANT : (int) (Math.abs(dist) / 2.5)
//				+ DEFENDER_SPEED_CONSTANT;
//		if (Math.abs(ang1) < 90) {
//			toExecute.travelDistance = isAttacker ? (int) dist
//					: (int) (dist / 3);
//		} else {
//			showDebugMessageOnFrameUpdate("Attempting to move negative distance");
//			toExecute.travelDistance = isAttacker ? (int) -dist
//					: (int) (dist / 3);
//		}
//		if (Math.abs(ang1) > 150 || Math.abs(ang1) < 10) {
//			toExecute.op = isAttacker ? Operation.OperationType.ATKTRAVEL
//					: Operation.OperationType.DEFTRAVEL;
//		} 
//		else {
//			toExecute.op = isAttacker ? Operation.OperationType.ATKROTATE
//					: Operation.OperationType.DEFROTATE;
//			toExecute.rotateBy = (int) (isAttacker ? ang1 : (ang1 / 3));
//			toExecute.rotateSpeed = Math.abs(toExecute.rotateBy);
//		}
//		// Old functions use ARC
//		/*
//			else if (ang1 > 0) {
//				if (ang1 > 90) {
//					toExecute.op = isAttacker ? Operation.Type.ATKARC_LEFT
//							: Operation.Type.DEFARC_LEFT;
//				} else {
//					toExecute.op = isAttacker ? Operation.Type.ATKARC_RIGHT
//							: Operation.Type.DEFARC_RIGHT;
//				}
//				toExecute.radius = isAttacker ? dist * 3 : -(dist * 3);
//			} else if (ang1 < 0) {
//				if (ang1 < -90) {
//					toExecute.op = isAttacker ? Operation.Type.ATKARC_RIGHT
//							: Operation.Type.DEFARC_RIGHT;
//				} else {
//					toExecute.op = isAttacker ? Operation.Type.ATKARC_LEFT
//							: Operation.Type.DEFARC_LEFT;
//				}
//				toExecute.radius = isAttacker ? dist * 3 : -(dist * 3);
//			}*/
//	}
//	return toExecute;
//}
//
//public Operation passBall(RobotType passer, RobotType receiver) {
//	showDebugMessageOnFrameUpdate("PASS_BALL");
//	Operation toExecute = new Operation();
//
//	toExecute.message = "PASS_BALL ";
//
//	if (!defenderHasArrived) {
//		toExecute = travelToNoArc(passer, defenderResetX, defenderResetY,
//				20);
//		if (toExecute.op == Operation.OperationType.DO_NOTHING) {
//			defenderHasArrived = true;
//		}
//	} else {
//
//		float targetY = 220;
//		int bounceDirection = 0;
//		if (enemyAttackerRobotY > ((topOfPitch + botOfPitch) / 2)) {
//			targetY = topOfPitch;
//			bounceDirection = 1;
//		} else {
//			targetY = botOfPitch;
//			bounceDirection = -1;
//		}
//		float targetX = attackerRobotX;
//		if (leftCheck > defenderCheck) {
//			targetX = ((defenderCheck + leftCheck) / 2);
//		} else {
//			targetX = ((rightCheck + defenderCheck) / 2);
//		}
//		double attackerAngle =  
//				calculateAngle(attackerRobotX,
//						attackerRobotY, attackerOrientation,
//						attackerResetX, attackerResetY);
//		double angleToPass = 0;
//		if (StrategyController.bouncePassEnabled) {
//			if (leftCheck > defenderCheck) {
//				angleToPass = Calculations.GetBounceAngle(defenderRobotX,
//						defenderRobotY, defenderOrientation, attackerRobotX - 20,
//						attackerRobotY, bounceDirection, goalY[1]);
//			} else {
//				angleToPass = Calculations.GetBounceAngle(defenderRobotX,
//						defenderRobotY, defenderOrientation, attackerRobotX + 20,
//						attackerRobotY, bounceDirection, goalY[1]);
//			}
//
//		} else {
//			angleToPass = calculateAngle(defenderRobotX,
//					defenderRobotY, defenderOrientation, attackerRobotX,
//					attackerRobotY);
//		}
//		if (attackerNotOnPitch) {
//			angleToPass = calculateAngle(defenderRobotX,defenderRobotY, defenderOrientation, goalX, goalY[2]);
//		}
//		double dist = 
//				Math.hypot(attackerRobotX - attackerResetX,
//						attackerRobotY - attackerResetY);
//
//		double attackerAngleToDefender = calculateAngle(attackerRobotX,
//				attackerRobotY, attackerOrientation, defenderRobotX, defenderRobotY);
//		if (attackerNotOnPitch) {
//			toExecute.op = Operation.OperationType.DEFROTATE;
//			// increased threshold from 3 to 5
//			if (Math.abs(angleToPass) > 5) {
//				toExecute.rotateBy = (int) angleToPass / 3;
//			} else
//				toExecute.op = Operation.OperationType.DEFKICKSTRONG;
//		} else {
//			toExecute.op = Operation.OperationType.ROTATENMOVE;
//			toExecute.travelSpeed = (int) (dist * 3);
//			if (Math.abs(attackerAngle) > 15 && !passingAttackerHasArrived) {
//				toExecute.op = Operation.OperationType.ATKROTATE;
//				toExecute.rotateBy = -(int) attackerAngle;
//			} else {
//				if (Math.abs(angleToPass) > 5) {
//					toExecute.rotateBy = (int) angleToPass / 3;
//				} else {
//					toExecute.rotateBy = 0;
//				}
//				if (Math.abs(dist) > 30 && !passingAttackerHasArrived) {
//					toExecute.travelDistance = (int) (dist);
//				} else if (Math.abs(dist) < 30 || passingAttackerHasArrived) {
//					passingAttackerHasArrived = true;
//					if (Math.abs(attackerAngleToDefender) > 10) {
//						toExecute.op = Operation.OperationType.ATKROTATE;
//						toExecute.rotateBy = -(int) attackerAngleToDefender;
//					} // XXX: CHANGED THIS ELSE IF TO AN IF.
//					// thresh 4 to 5
//					if (Math.abs(angleToPass) < 5) {
//						toExecute.travelDistance = 0;
//						if (StrategyController.bouncePassEnabled) {
//							toExecute.op = Operation.OperationType.DEFKICKSTRONG;
//						} else {
//							toExecute.op = Operation.OperationType.DEFCONFUSEKICK;
//						}
//					}
//				}
//
//			}
//		}
//	}
//	return toExecute;
//}
//
//public Operation returnToOrigin(RobotType robot) {
//	showDebugMessageOnFrameUpdate("RETURN_TO_ORIGIN");
//
//	Operation toExecute = new Operation();
//	toExecute.message = "RETURN_TO_ORIGIN ";
//
//	boolean isAttacker = robot == RobotType.ATTACKER;
//
//	toExecute = isAttacker ? travelToNoArc(robot, attackerResetX,
//			attackerResetY, 10) : travelToNoArc(robot, defenderResetX,
//					defenderRobotY, 10);
//
//	return toExecute;
//}
//
//@Override
//public void sendWorldState(WorldState worldState) {
//	attackerRobotX = worldState.getAttackerRobot().x;
//	attackerRobotY = worldState.getAttackerRobot().y;
//	defenderRobotX = worldState.getDefenderRobot().x;
//	defenderRobotY = worldState.getDefenderRobot().y;
//	enemyAttackerRobotX = worldState.getEnemyAttackerRobot().x;
//	enemyAttackerRobotY = worldState.getEnemyAttackerRobot().y;
//	enemyDefenderRobotX = worldState.getEnemyDefenderRobot().x;
//	enemyDefenderRobotY = worldState.getEnemyDefenderRobot().y;
//	ballX = worldState.getBall().x;
//	ballY = worldState.getBall().y;
//	attackerOrientation = worldState.getAttackerRobot().orientationDegrees;
//	defenderOrientation = worldState.getDefenderRobot().orientationDegrees;
//	enemyAttackerOrientation = worldState.getEnemyAttackerRobot().orientationDegrees;
//	enemyDefenderNotOnPitch = worldState.enemyDefenderNotOnPitch;
//	topOfPitch = PitchConstants.getPitchOutlineTop();
//	botOfPitch = PitchConstants.getPitchOutlineBottom();
//	attackerNotOnPitch = worldState.attackerNotOnPitch;
//	if (worldState.weAreShootingRight) {
//		leftCheck = worldState.dividers[1];
//		rightCheck = worldState.dividers[2];
//		defenderCheck = worldState.dividers[0];
//		defenderResetX = ((defenderCheck - PitchConstants.getPitchOutline()[7].getX()) / 2) + PitchConstants.getPitchOutline()[7].getX() + 20;
//		attackerResetX = ((leftCheck + rightCheck) / 2) + 15;
//		goalX = 640;
//		ourGoalX = PitchConstants.getPitchOutline()[7].getX();
//		goalY = worldState.rightGoal;
//		ourGoalEdges[0] = PitchConstants.getPitchOutline()[7].getY();
//		ourGoalEdges[1] = worldState.leftGoal[1];
//		ourGoalEdges[2] = PitchConstants.getPitchOutline()[6].getY();
//		ourGoalY = worldState.leftGoal;
//	} else {
//		leftCheck = worldState.dividers[0];
//		rightCheck = worldState.dividers[1];
//		defenderCheck = worldState.dividers[2];
//		defenderResetX = ((PitchConstants.getPitchOutline()[2].getX() - defenderCheck) / 2) + defenderCheck - 20;
//		attackerResetX = ((leftCheck + rightCheck) / 2) - 15;
//		goalX = 0;
//		ourGoalX = PitchConstants.getPitchOutline()[2].getX();
//		goalY = worldState.leftGoal;
//		ourGoalEdges[0] = PitchConstants.getPitchOutline()[2].getY();
//		ourGoalEdges[1] = worldState.rightGoal[1];
//		ourGoalEdges[2] = PitchConstants.getPitchOutline()[3].getY();
//		ourGoalY = worldState.rightGoal;
//	}
//	attackerResetY = (PitchConstants.getPitchOutlineBottom() + PitchConstants.getPitchOutlineTop())/2;
//	defenderResetY = (PitchConstants.getPitchOutlineBottom() + PitchConstants.getPitchOutlineTop())/2;
//
//}
//
//public static double calculateAngle(float robotX, float robotY,
//		float robotOrientation, float targetX, float targetY) {
//	double robotRad = Math.toRadians(robotOrientation);
//	double targetRad = Math.atan2(targetY - robotY, targetX - robotX);
//
//	if (robotRad > Math.PI)
//		robotRad -= 2 * Math.PI;
//
//	double ang1 = robotRad - targetRad;
//	while (ang1 > Math.PI)
//		ang1 -= 2 * Math.PI;
//	while (ang1 < -Math.PI)
//		ang1 += 2 * Math.PI;
//	return Math.toDegrees(ang1);
//}
//
//protected void showDebugMessageOnFrameUpdate(String message){
//	//System.out.println(message);
//}
//
//protected void showDebugMessageOnStrategyTick(String message){
//	System.out.println(message);
//}
//}
