package pc.prediction;
import java.util.ArrayList;

import pc.strategynew.StrategyUtils;
import pc.vision.PitchConstants;
import pc.world.oldmodel.Point2;

public final class Calculations {
	
	/**
	 * 
	 * @return returns the distance between 2 points in a 2D plane
	 * */
	public static float GetDistance(Point2 a, Point2 b){
		double x1,x2,y1,y2;
		x1 = (float) a.getX();
		x2 = (float) b.getX();
		y1 = (float) a.getY();
		y2 = (float) b.getY();
		//System.out.println("1 (" + x1 + ", " + y1 + ") 2 (" + x2 + ", " + y2 + ")");
		
		double distance = Math.sqrt(Math.pow((y1-y2),2)+Math.pow((x1-x2),2));
		return (float) distance;
	}
	
	/**
	 * [0] is the distance in moment t (current time - 1)
	 * [1] is the distance in moment t-1 (current time - 2)
	 * [2] is the distance in moment t-2 (current time - 3)
	 * */
	public static float LinearPrediction(float[] data){
		float v1,v2,v3,a1_2,a2_3, acc_decay;
		v1 = data[data.length-1];	// most recent velocity
		v2 = data[data.length-2];	// second most recent velocity
		v3 = data[data.length-3];	// third most recent velocity
	    a1_2 = Math.abs(v2-v1);
	    a2_3 = Math.abs(v3-v2);
	    acc_decay = a2_3 - a1_2;	// negative decay is slowing down, positive is speeding up
	   // System.out.println(a1_2);
	    //System.out.println(a2_3);
	    //System.out.println(acc_decay);
		if(v1 - (a1_2 - acc_decay) > 0)	
			/* predicts point for moment t+1, v1 is most recent distance at moment t,
			   (a1_2-acc_decay) is predicted acceleration for moment t+1,
			   thus v1 - (a1_2 - acc_decay) is the predicted distance from the most recent point */
			return v1 - (a1_2 - acc_decay);
		else
			return 0;
	}
	
	public enum CorrectionType{
		TOP_OR_BOTTOM, LEFT_OR_RIGHT, 		
	}
	
	/**
	 * This method will calculate the coordinates of the end point, if its trajectory indicates it will
	 * bounce off a wall
	 * TODO: implement support for the corners
	 * */
	public static Point2 CalculateBounceCoordinate(Point2 prediction, CorrectionType type, float boundary){
		float c = prediction.getX(), d = prediction.getY();
		Point2 correction = null;
		if(type == CorrectionType.TOP_OR_BOTTOM){
			if (d < 0) { // TOP boundary
				correction = new Point2(c,-d);
			} else { // BOTTOM boundary
				float diff = d - boundary;
				correction = new Point2(c, boundary - diff);
			}
		}
		else if(type == CorrectionType.LEFT_OR_RIGHT){
			if (c < 0) { // LEFT boundary
				correction = new Point2(-c,d);	
			} else { // RIGHT boundary
				float diff = c - boundary;
				correction = new Point2(boundary - diff, d);
			}
		}
		return correction;
	}
	
	public static Point2 GetPointViaDistance(float distance, Point2 a, Point2 b){
		float aX, aY, bX, bY;
		aX = a.getX();
		aY = a.getY();
		bX = b.getX();
		bY = b.getY();
		//odd case where a == b
		if(aX == bX && aY == bY)
			return a;
		
		float dist = GetDistance(a,b);
		float sinA = Math.abs(aY-bY)/dist;
		float cosA = Math.abs(aX-bX)/dist;
//		System.out.println("a: (" + aX + ", " + aY + ") b: (" + bX + ", " + bY + ") " + 
//				"Dist: " + dist + " sinA " + sinA + " cosA " + cosA);
		
		int x, y;
		
		if (aX > bX) {	// heading negative
			x = Math.round(bX-cosA*distance);
		} else if (aX < bX) {	// heading positive
			x = Math.round(bX+cosA*distance);
		} else { // no X movement
			x = Math.round(aX);
		}
		
		if (aY > bY) {	// Negative heading
			y = Math.round(bY-sinA*distance);
		} else if (aY < bY) {	// Positive heading
			y = Math.round(bY+sinA*distance);
		} else {	// no Y movement
			y = Math.round(aY);
		}
		
		Point2 pred = new Point2(x,y);		
		return pred;		
	}
	
	
	public static Point2 PredictNextPoint(ArrayList<Point2> history){
		if(history.size() == 0)
			return new Point2(0,0);
		if(history.size() < 4){
			//if we have 1 point, need to have object orientation and apply a constant
			//if(history.size() > 0){
				//mock
				double orientation = 0.785;
				//initial kick multiplier. Needs to be adjusted
				double multiplier = 3;
				double tg = Math.tan(orientation);
				float X0 = history.get(0).getX();
				float Y0 = history.get(0).getY();
				double b = X0 -  Y0 / tg;
				//new point
				double X1,Y1;
				Y1 = Y0*multiplier;
				X1 = b + Y1/tg;
				
				Point2 prediction = new Point2((float)X1,(float)Y1);
				return prediction;
			//}
			//if we have 2 or 3 points, use a constant for prediction
			
			//throw new IllegalArgumentException("Cannot make a prediction based on less than 4 points");
		}
		else{
			int size = history.size();
			//compute distance travelled for the last 4 points
			float[] distances = new float[3];
			distances[0] = GetDistance(history.get(size - 4), history.get(size-3));
			distances[1] = GetDistance(history.get(size - 3), history.get(size-2));
			distances[2] = GetDistance(history.get(size - 2), history.get(size-1));
			//System.out.println(String.format("%f %f %f",distances[0], distances[1], distances[2]));
			//Get predicted distance
			float prediction = LinearPrediction(distances);
//			System.out.println("Pred: " + prediction);
			Point2 pred = GetPointViaDistance(prediction, history.get(size-2), history.get(size-1));
//			System.out.println("xp " + pred.getX() + " yp " + pred.getY());
			
			return pred;
		}
	}

	/**
	 * returns the angle from which a bounce shot can be scored, from the position
	 * supplied
	 * @param bounce_direction 0 for automatic, 1 for top, -1 for bottom bounce
	 * */
	public static float GetBounceAngle(float robotX, float robotY,
			float robotOrientation, float targetX, float targetY, int bounce_direction, float pitchMid){
		
		int bottom_boundary = PitchConstants.getPitchOutlineBottom();
		int top_boundary = PitchConstants.getPitchOutlineTop();
		
		//int bottom_boundary = 20;
		//int top_boundary = 600;
		
		double robotRad = Math.toRadians(robotOrientation);
		if (robotRad > Math.PI)
			robotRad -= 2 * Math.PI;
		
		//Get X and Y velocities
		double x1 = (double) robotX;
		double y1 = (double) robotY;
		double x2 = (double) targetX;
		double y2 = (double) targetY;
		double y3;
		//check which wall we are bouncing off
		if(bounce_direction == 0)
		{
			if(Math.abs(y1) > pitchMid)
				y3 = bottom_boundary;
			else
				y3 = top_boundary;		
		}
		else if(bounce_direction == -1){
			y3 = bottom_boundary + 20;
		}
		else{
			y3 = top_boundary- 50;
		}
		
		double z = Math.abs(x1-x2);
		
		double a = Math.abs(y3-y2);
		double d = Math.abs(y3-y1);
		//calculate the coordinates of the middle point
		double c = (z*d)/(a+d);
		double b = z - c;
		
		double x3;
		if(x2 > x1)
			x3 = x1 + c;
		else
			x3 = x2 + b;
		
		double choice_angle;
		//REWORK IN PROGRESS
		choice_angle = StrategyUtils.calculateRelativeAngleDeg(robotX, robotY, (float) robotOrientation, (float) x3, (float) y3);
		
		/*
		//if(left_angle > right_angle)
		//	choice_angle = right_angle + (left_angle - right_angle)*0.5;
		//else if(left_angle < right_angle)
		//	choice_angle = right_angle - (right_angle - left_angle)*0.5;
		
		//convert choice angle to the type the orientation is using
		double choice_angle_deg = Math.toDegrees(choice_angle);
		double adjusted_choice_angle = choice_angle_deg;
		if(choice_angle_deg < 90)
			adjusted_choice_angle = 
		
		
		float fl_choice_angle = (float) (Math.PI/2 - choice_angle);
		float angle_to_turn = (float) (fl_choice_angle - (robotRad));
		angle_to_turn = (float) Math.toDegrees(angle_to_turn);
		System.out.println("left angle: " + Math.toDegrees(left_angle) + " right angle: " + Math.toDegrees(right_angle) + " choice angle: " + Math.toDegrees(choice_angle) + " fl_choice_angle: " + Math.toDegrees(fl_choice_angle) + " angle to turn: " + angle_to_turn);
		//the angle the robot needs to turn
//		if(y3 == bottom_boundary)
//			//System.out.println("Bouncing off bottom boundary |Angle to turn = "+angle_to_turn);
//		if(y3 == top_boundary)
			//System.out.println("Bouncing off top boundary |Angle to turn = "+angle_to_turn);
		*/
		return (float)choice_angle;
	}
	
}
