package pc.movement;

/**
 * Represents how the robot is moving, and
 * at what speed. Allows for easy comparisons of
 * how the robot is moving. Useful for duplicate
 * detection (e.g. if the robot is already moving
 * this way, there is no need to tell it to again).
 * 
 * This class is primarily for data storage rather than
 * any heavy computations.
 */
public class RobotMovement {
	public final MovementType type;
	public final int speed;
	
	public RobotMovement(MovementType type, MovementSpeed speed) {
		this.type = type;
		
		//Make sure stopped always has a 0 speed.
		//to avoid meaningless comparisons (e.g. stopped at 80% != stopped at 40%).
		//For other movements, just use the given speed.
		this.speed = type == MovementType.STOPPED ? 0 : speed.getPower();
	}
	
	public RobotMovement(MovementType type, int speed) {
		this.type = type;
		
		//Make sure stopped always has a 0 speed.
		//to avoid meaningless comparisons (e.g. stopped at 80% != stopped at 40%).
		//For other movements, just use the given speed.
		this.speed = type == MovementType.STOPPED ? 0 : speed;
	}
	
	//Check whether two movements are the same type, and the
	//same speed. Allows for easy duplicate detection.
	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof RobotMovement){
			RobotMovement mov = (RobotMovement) obj;
			return mov.type == type && mov.speed == speed;
		}
		return false;
	}
}
