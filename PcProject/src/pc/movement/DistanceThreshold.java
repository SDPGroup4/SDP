package pc.movement;

import java.awt.Point;

import pc.world.oldmodel.MovingObject;

/**
 * A utility class for dealing with distance thresholding (checking
 * whether two points are the same within a certain level of uncertainty).
 * 
 * Offers a single central place for thresholds to be tweaked,
 * rather than scattering these checks throughout the strategy
 * code. Uses an enum to allow precision level to be specified
 * without having to hard-code numbers everywhere.
 * 
 * Has several utility methods for dealing with Points,
 * MovingObjects, or numerical coordinates (or combinations
 * of these) to make it easier to use for common comparisons.
 */
public enum DistanceThreshold {
	//Distances in millimetres. Can add more levels of
	//granularity here if necessary, and can also tweak
	//values here when testing with the robot.
	PRECISE(10, "PRECISE"),
	MEDIUM(20, "MEDIUM"),
	INTERCEPT(20, "INTERCEPT"),
	ROUGH(40, "ROUGH"),
	VERYROUGH(100, "VERYROUGH"),
	;

	public static int DEFAULT_DISTANCE_THRESH = -1;
	
	private final int threshHold;
	private final String name;
	private DistanceThreshold(int threshHold, String name) {
		this.threshHold = threshHold;
		this.name = name;
	}
	
	//Methods for checking whether two points are close enough
	//together within the given precision range.
	
	public boolean isCloseEnough(MovingObject mov, int destX, int destY){
		return isCloseEnoughSquared(getDistanceSquared((int)mov.x, (int)mov.y, destX, destY));
	}

	public boolean isCloseEnough(MovingObject mov, MovingObject target){
		return isCloseEnoughSquared(getDistanceSquared((int)mov.x, (int)mov.y, (int) target.x, (int) target.y));
	}
	
	public boolean isCloseEnough(MovingObject mov, Point dest){
		return isCloseEnoughSquared(getDistanceSquared((int)mov.x, (int)mov.y, dest.x, dest.y));
	}
	
	public boolean isCloseEnough(Point start, Point dest){
		return isCloseEnoughSquared(getDistanceSquared(start.x, start.y, dest.x, dest.y));
	}
	
	public boolean isCloseEnough(int x, int y, int targetX, int targetY){
		return isCloseEnoughSquared(getDistanceSquared(x, y, targetX, targetY));
	}

	public boolean isCloseEnoughSquared(int distanceSquared){
		return distanceSquared <= getThreshold()*getThreshold();
	}

	public boolean isCloseEnough(int distance){
		return Math.abs(distance) <= getThreshold();
	}
	
	public int getThreshold(){
		if(DEFAULT_DISTANCE_THRESH > -1){
			return DEFAULT_DISTANCE_THRESH;
		}
		return threshHold;
	}

	@Override
	public String toString() {
		return name+"("+getThreshold()+")";
	}

	
	//Functions for getting the distance between two points (simple maths, but
	//makes code using it easier to read than having Math.sqrt(a*a + b*b)
	//everywhere.
	
	public static int getDistance(int x1, int y1, int x2, int y2){
		return (int) Math.hypot(x2-x1, y2-y1);
	}
	
	public static int getDistanceSquared(int x1, int y1, int x2, int y2){
		int a = x2 - x1;
		int b = y2 - y1;
		return a*a + b*b;
	}
}
