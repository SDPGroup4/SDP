package pc.movement;

	/**Enum describing how the robot can act.
	 * For easy comparison of actions it may take.
	 */
public enum ActionType {
	//List of all currently supported action types.
 	//More action types such as feign kick could
 	//be added here if necessary.
	NONE("NONE"),
 	CATCH("CATCH"),
 	KICK("KICK"),
 	RESET_CATCHER("RESET_CATCHER")
 	;
 	
	private final String name;
 	private ActionType(String name) {
 		this.name = name;
 	}
 
	 @Override
	 public String toString() {
	 	return name;
	 }
 }
