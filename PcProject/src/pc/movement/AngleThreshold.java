package pc.movement;

/**
 * A utility class for dealing with angle thresholding (checking
 * whether angles are equal within a certain level of uncertainty).
 * 
 * Offers a single central place for thresholds to be tweaked,
 * rather than scattering these checks throughout the strategy
 * code. Uses an enum to allow precision level to be specified
 * without having to hard-code numbers everywhere.
 * 
 * Has several utility methods for dealing with angles in both
 * degrees and radians (with names to make the units clear).
 */
public enum AngleThreshold {
	//Angles in degrees. 
	//Can add more levels of granularity here if 
	//necessary, and can also tweak
	//values here when testing with the robot.
	PRECISE(12, "PRECISE"),
	MEDIUM(18, "MEDIUM"),
	GOAL(10, "ALIGNING_TO_GOAL"),
	ROUGH(30, "ROUGH"),
	ROUGHER(50, "ROUGHER");
	;
	
	public static int DEFAULT_ANGLE_THRESH = -1;

	//Numerical thresholds in both degrees and radians.
	private final int degreeThreshHold;
	private final double radianThreshold;
	
	private final String name;
	private AngleThreshold(int degreeThreshHold, String name) {
		this.degreeThreshHold = degreeThreshHold;
		this.radianThreshold = Math.toRadians(degreeThreshHold);
		this.name = name;
	}

	//Methods for comparing whether angles are close enough
	//for the specified level of uncertainty.
	
	public boolean isCloseEnoughDegrees(int current, int target){
		return isCloseEnoughDegrees(target-current);
	}
	
	public boolean isCloseEnoughRadians(double current, double target){
		return isCloseEnoughRadians(target-current);
	}
	
	public boolean isCloseEnoughDegrees(int degreesDifference){
		return Math.abs(degreesDifference) <= getDegreeThreshold();
	}

	public boolean isCloseEnoughRadians(double radiansDifference){
		return Math.abs(radiansDifference) <= getRadianThreshold();
	}

	public int getDegreeThreshold(){
		if(DEFAULT_ANGLE_THRESH > -1){
			return DEFAULT_ANGLE_THRESH;
		}
		return degreeThreshHold;
	}
	
	public double getRadianThreshold(){
		if(DEFAULT_ANGLE_THRESH > -1){
			return Math.toRadians(DEFAULT_ANGLE_THRESH);
		}
		return radianThreshold;
	}
	
	@Override
	public String toString() {
		return name+"("+getDegreeThreshold()+")";
	}

}
