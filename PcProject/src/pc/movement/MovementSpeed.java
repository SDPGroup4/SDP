package pc.movement;

/**
 * A utility class for dealing with the robot's movement speed.
 * 
 * Abstracts from the fact that motor power percentages are used
 * to determine how fast it goes, and provides
 * a single place to tweak speed values. Also allows for easy
 * comparison between speeds rather than having to remember
 * which numerical values were used where.
 * 
 * Offers a single central place for speed levels
 * can be tweaked  rather than scattering 
 * hard-coded numbers throughout the strategy code.
 */
public enum MovementSpeed {
	//Speeds in motor percentages
	FULL(100, "FULL"),
	FAST(80, "FAST"),
	MEDIUM(65, "MEDIUM"),
	SLOW(50 , "SLOW"),
	VERYSLOW(43,"VERYSLOW"),
	NEARBOUNDARY(40, "NEARBOUNDARY"),
	SMALL_ANGLE_SPEED(40, "SMALL_ANGLE"),
	SMALL_DISTANCE_SPEED(40, "SMALL_DISTANCE"),
	ZERO(0, "ZERO")
	;

	private final int power;
	private final String name;

	private MovementSpeed(int power, String name) {
		this.power = power;
		this.name = name;
	}

	/**
	 * Used by the command level code to determine
	 * which power to run the motors at.
	 */
	public int getPower() {
		if(this == FULL){
			return power;
		}else if(DEFAULT_MOVEMENT_SPEED > -1){
			return DEFAULT_MOVEMENT_SPEED;
		}
		return power;
	}

	@Override
	public String toString() {
		return name+"("+getPower()+")";
	}
	
	public static int DEFAULT_MOVEMENT_SPEED = -1;
}

