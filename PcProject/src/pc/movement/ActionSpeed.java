package pc.movement;

public enum ActionSpeed {
	// Motor speeds for actions (%)
	FULL(100, "FULL"),
	HALF(50, "HALF"),
	ZERO(0, "ZERO")
	;
	
	private final int power;
	private final String name;
	
	private ActionSpeed(int power, String name) {
		this.power = power;
		this.name = name;
	}
	
	public int getPower() {
		return power;
	}
	
	@Override
	public String toString() {
		return name+"("+power+")";
	}

}
