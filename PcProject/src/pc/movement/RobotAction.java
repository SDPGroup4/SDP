package pc.movement;

/** Represents how robot is acting.
 *  Primarily a data storage class for comparison purposes
 * @author s1235734
 *
 */
public class RobotAction {
	public final ActionType type;
	public final ActionSpeed speed;
	
	// 
	public RobotAction(ActionType type, ActionSpeed speed) {
		this.type = type;
		// Avoid meaningless comparisons by returning 0 when actionType is NONE
		this.speed = type == ActionType.NONE ? ActionSpeed.ZERO : speed;
	}
	
	// Check two actionTypes are the same type. For duplicate detection.
	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof RobotAction){
			RobotAction act = (RobotAction) obj;
			return act.type == type && act.speed == speed;
		}
		return false;
	}

}
