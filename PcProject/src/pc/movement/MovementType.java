package pc.movement;

/**
 * An enum for describing how the
 * robot is currently moving. Allows for
 * easy comparisons between motion types.
 */
public enum MovementType {
	//List of all currently supported motion types.
	//More motion types such as ARC could
	//be added here if necessary.
	FORWARDS("FORWARDS"),
	BACKWARDS("BACKWARDS"),
	LEFT("LEFT"),
	RIGHT("RIGHT"),
	STOPPED("STOPPED")
	;

	private final String name;
	private MovementType(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
