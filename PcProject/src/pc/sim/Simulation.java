package pc.sim;

import java.awt.Point;

import pc.world.DirectedPoint;
import pc.world.StaticWorldState;
import pc.world.oldmodel.WorldState;

public class Simulation {

	StaticWorldState prevState = new StaticWorldState();
	
	StaticWorldState getNextState(int delta, WorldState worldState){
		StaticWorldState nextState = new StaticWorldState();
		nextState.setAttacker(new DirectedPoint(100, 100, 90));
		nextState.setBall(new Point(0, 0));

		//Test to see if stuff moves correctly in the Model.
		DirectedPoint defender = prevState.getDefender();
		if(defender != null){
			double x = defender.getX();
			double heading = defender.getDirection();
			nextState.setDefender(new DirectedPoint((int)x+1, 200, heading+(Math.PI / 180)));
		}else{
			nextState.setDefender(new DirectedPoint(200, 200, 45));
		}

		nextState.setEnemyAttacker(new DirectedPoint(300, 300, 45));
		nextState.setEnemyDefender(new DirectedPoint(400, 100, 45));

		prevState = nextState;
		return nextState;
	}
}
