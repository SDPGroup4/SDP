package pc.sim;


import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.UIManager;

import org.apache.commons.cli.Options;

import pc.strategynew.StrategyController;
import pc.vision.DistortionFix;
import pc.vision.PitchConstants;
import pc.vision.VideoStream;
import pc.vision.YAMLConfig;
import pc.vision.gui.VisionGUI;
import pc.vision.gui.tools.AlignmentTool;
import pc.vision.gui.tools.PitchModelView;
import pc.vision.gui.tools.StrategySelectorTool;
import pc.vision.recognisers.BallRecogniser;
import pc.vision.recognisers.RobotRecogniser;
import pc.world.DynamicWorldState;
import pc.world.Pitch;
import pc.world.PitchZone.PitchZoneType;

/**
 * The main class used to run the vision system. Creates the control GUI, and
 * initialises the image processing.
 */
public class RunSim{
	static Options cmdLineOptions;

	static {
		cmdLineOptions = new Options();
		cmdLineOptions.addOption("nobluetooth", false,
				"Disable Bluetooth support");
	}

	/**
	 * The main method for the class. Creates the control GUI, and initialises
	 * the image processing.
	 * 
	 * @param args
	 *            Program arguments.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
			e.printStackTrace();
		}

		final YAMLConfig yamlConfig = new YAMLConfig();
		//0 = default to main pitch
		final PitchConstants pitchConstants = new PitchConstants(0);
		final Pitch pitch = new Pitch(yamlConfig, pitchConstants);

		DynamicWorldState dynamicWorldState = new DynamicWorldState();

		// Default values for the main vision window
		int width = VideoStream.FRAME_WIDTH;
		int height = VideoStream.FRAME_HEIGHT;

		// Create a new Vision object to serve the main vision window
		final SimVision simVision = new SimVision(dynamicWorldState);


		try {
			StrategyController strategyController = new StrategyController(null, PitchZoneType.ATTACKER);
			SimVision.addWorldStateReceiver(strategyController);

			DistortionFix distortionFix = new DistortionFix(yamlConfig, pitchConstants);


			// Create the Control GUI for threshold setting/etc
			VisionGUI gui = new VisionGUI(width, height, yamlConfig);

			gui.addWindowListener(new WindowAdapter() {
				@Override
				public void windowClosed(WindowEvent e) {
					simVision.stop();
				}
			});

			PitchModelView pmvTool = new PitchModelView(gui, pitchConstants,pitch, distortionFix, dynamicWorldState);
			gui.addTool(pmvTool, "Pitch Model");
			pmvTool.addViewProvider(new BallRecogniser.ViewProvider(dynamicWorldState, pitch));
			pmvTool.addViewProvider(new RobotRecogniser.ViewProvider(dynamicWorldState, pitch));


			StrategySelectorTool stratSelect = new StrategySelectorTool(gui, strategyController);
			gui.addTool(stratSelect, "Robot and strategy control");

			AlignmentTool alignmentTool = new AlignmentTool(gui);
			gui.addTool(alignmentTool, "Alignment");

			distortionFix.addReceiver(gui);

			simVision.start();
			
			gui.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
