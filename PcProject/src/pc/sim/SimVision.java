package pc.sim;

import java.util.ArrayList;

import pc.vision.interfaces.WorldStateReceiver;
import pc.world.DynamicWorldState;
import pc.world.StaticWorldState;


public class SimVision implements Runnable{

	// Variables used in processing video
	private final DynamicWorldState dynamicWorldState;
	private static ArrayList<WorldStateReceiver> worldStateReceivers = new ArrayList<WorldStateReceiver>();
	private final Simulation sim = new Simulation();
	
	public SimVision(DynamicWorldState dynamicWorldState) {
		// Set the state fields.
		this.dynamicWorldState = dynamicWorldState;
	}

	/**
	 * Registers an object to receive the world state from the vision system
	 * 
	 * @param receiver
	 *            The object being registered
	 */
	public static void addWorldStateReceiver(WorldStateReceiver receiver) {
		worldStateReceivers.add(receiver);
	}

	public static void removeWorldStateReciver(WorldStateReceiver reciver) {
		worldStateReceivers.remove(reciver);
	}

	/**
	 * Processes an input image, extracting the ball and robot positions and
	 * robot orientations from it, and then displays the image (with some
	 * additional graphics layered on top for debugging) in the vision frame.
	 * 
	 * @param frame
	 *            The image to process and then show.
	 * @param delta
	 *            The time between frames in seconds
	 * @param counter
	 *            The index of the current frame
	 */
	public void update(int delta, long timestamp) {
		StaticWorldState staticWorldState = sim.getNextState(delta, null);
		dynamicWorldState.pushState(staticWorldState, timestamp);
	}
	
	/**
	 * Tests if a floating point value is within bounds, or outside bounds if
	 * the range is inverted
	 * 
	 * @param value
	 *            The value to check
	 * @param lower
	 *            The lower bound
	 * @param upper
	 *            The upper bound
	 * @param inverted
	 *            true if the range is inverted, false otherwise
	 * @return true if the value is within bounds, false otherwise
	 */
	public static boolean checkBounds(float value, float lower, float upper,
			boolean inverted) {
		if (!inverted)
			return (lower <= value && value <= upper);
		else
			return (upper <= value || value <= lower);
	}



	private Thread simThread = null;

	private boolean keepRunning = true;
	@Override
	public void run() {

		long timeNow = System.currentTimeMillis();
		long prevTime = timeNow;
		keepRunning = true;

		while(keepRunning){
			try{
				int delta = (int) (timeNow - prevTime);
				update(delta, timeNow);
				Thread.sleep(32);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	public void start(){
		if(simThread != null){
			stop();
		}
		keepRunning = true;
		simThread = new Thread(this);
		simThread.start();
	}

	public void stop(){
		keepRunning = false;
		while(simThread != null){
			try{
				simThread.join();
				simThread = null;
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
}
