package pc.strategynew;

import pc.strategynew.plan.Plan;
import pc.world.oldmodel.WorldState;

/**
 * Represents the highest level of the strategy code.
 * 
 * The StrategySelector will select which one of these
 * is relevant to use every time a major change in world
 * state happens (e.g. the ball changes pitch zones).
 * 
 * Strategies extending this abstract class must
 * specify a StrategyType from the below enum, and
 * return this using getType(). They must also be
 * able to return the best Plan to follow given
 * the current world and strategy state.
 * 
 * 
 * The robot's decision making hierarchy is:
 * 
 * Strategy    (e.g. Attacking, Marking, Passing)
 * Plan		   (e.g. CatchBall, ReturnToOrigin, ScoreGoal)
 * Operation   (e.g. Forwards, TurnLeft, Stop, ConfuseKickLeft)
 * Command	   (the Arduino must be programmed for these e.g. KICK, FORWARDS, STOP).
 */
public abstract class Strategy {
	
	public enum StrategyType {
		//All possible types of Strategy.
		DO_NOTHING("DO_NOTHING_STRATEGY"), 
		ATTACKING("ATTACKING_STRATEGY"),
		INTERCEPTOR("INTERCEPTOR STRATEGY"),
		MILESTONEORDINARYPASSING("PASSING ORDINARY STRATEGY"),
		MILESTONEOBSTACLEPASSING("PASSING OBSTACLE STRATEGY"),
		FINDSPACE("FIND SPACE STRATEGY"),
		TRACKBALL("TRACK THE BALL"),
		RECEIVER_STATIONARY("ALLIGN WITH ALLY STATIONARY"),
		RECEIVER_MOVING("ALLIGN WITH ALLY MOVING"),
		PENALTY_ATTACKING("PENALTY SHOT"),
		
		/* Potential others
		PASSING, 
		DEFENDING, 
		MARKING
		*/
		;
		
		private final String name;
		private StrategyType(String name) {
			this.name = name;
		}
		@Override
		public String toString() {
			return name;
		}
	}
	

	/**
	 * Get the next plan the robot should follow (which
	 * may be the same one as last time).
	 * 
	 * May also update the StrategyState given if anything
	 * useful has changed.
	 * 
	 * Must not return null (should use a new DoNothingPlan() instead).
	 * 
	 * @param worldState The current state of the world as seen by the vision system. Do not change this.
	 * @param currentState The current state of the robot's decision making. You can update this if necessary for the desired Plans.
	 * @return A Plan that the robot should follow. Must not be null.
	 */
	abstract Plan getNextPlan(WorldState worldState, StrategyState currentState);
	
	/**
	 * @return A type value from the above enum describing what this strategy does.
	 */
	abstract StrategyType getType();
	
	@Override
	public String toString() {
		return getType().toString();
	}
}
