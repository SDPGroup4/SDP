package pc.strategynew;

import pc.world.PitchZone.PitchZoneType;
import pc.world.oldmodel.WorldState;


/**
 * Decide on which strategy to use for the robot based
 * on the world state, and the current strategy state.
 * 
 * At the moment, it always makes sure that an attacker strategy
 * is selected, as this is the only one half-written so far.
 * 
 * This will need changed to adapt to when the ball changes pitch zones
 * (see commented out skeleton code below for a rough outline).
 * 
 * The way StrategySelectors work may need rewritten to allow strategies to
 * be selected from the GUI manually.
 */
public class StrategySelector {

	//At the moment, this always returns an AttackerStrategy().
	//Will need adapted.
	public Strategy getStrategy(WorldState worldState, StrategyState currentState){
		// Check where the ball is, and make a decision on which strategies to
		// run based upon that.

		PitchZoneType ballLocation = StrategyUtils.getPitchZone(worldState.getBall(), worldState);

		Strategy currentStrategy = null;

		switch(ballLocation){
		case ENEMY_ATTACKER: currentStrategy = new ReceiverStrategyStationary(); break;
		case DEFENDER:	
		case ATTACKER:       currentStrategy = new AttackerStrategy();     break;
		case ENEMY_DEFENDER: currentStrategy = new InterceptorStrategy();  break;
		}

		return currentStrategy != null ? currentStrategy : new DoNothingStrategy();
	}
}