package pc.strategynew;

import pc.movement.ActionSpeed;
import pc.strategynew.plan.CatchBallPlan;
import pc.strategynew.plan.DoNothingPlan;
import pc.strategynew.plan.KickToAllyPlan;
import pc.strategynew.plan.MoveToGetRunupPlan;
import pc.strategynew.plan.Plan;
import pc.strategynew.plan.ReturnToOriginPlan;
import pc.world.oldmodel.WorldState;

/**
 * Class to pass the ball to the ally defender robot for the milestone, without enemy robot
 * 
 * Idea:
 * Catch the ball and kick to the ally robot from the origin point of the area
 */
public class PassOrdinaryStrategy extends Strategy{
	
	private static final int TIME_TO_WAIT_BETWEEN_KICKS = 1000;
	
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState){
		Plan currentPlan = new DoNothingPlan(); // out of scope of the pass strategy, do nothing

		if(strategyState.hasCaughtBall || strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball)){ // ball in our zone
			if(strategyState.hasCaughtBall){ // we have the ball to pass, we need move and kick
				if (StrategyUtils.hasEnoughOfARunUp(strategyState, strategyState.allyRobotZone)){
					currentPlan = new KickToAllyPlan(strategyState, ActionSpeed.FULL);
				} else {
					// Move to a small distance behind the origin x-axis which allows us to move and kick with a run-up.
					currentPlan = new MoveToGetRunupPlan(strategyState, strategyState.allyRobotZone);
				}
			} else {
				long currentTime = System.currentTimeMillis();
				long timeSinceLastkick = currentTime - strategyState.lastKickedBallTime;
				if(timeSinceLastkick >= TIME_TO_WAIT_BETWEEN_KICKS){
					currentPlan = new CatchBallPlan(strategyState);
				}
//				else{
//					System.out.println("Waiting for kickTimer "+timeSinceLastkick+" to get above "+TIME_TO_WAIT_BETWEEN_KICKS);;
//				}
			}
		}else{
			//System.out.println("Has caught ball = "+strategyState.hasCaughtBall+" ball in our zone = "+strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball));
			currentPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);
		}
		return currentPlan;
	}
	
	@Override
	StrategyType getType() {
		return StrategyType.MILESTONEORDINARYPASSING;
	}
}
