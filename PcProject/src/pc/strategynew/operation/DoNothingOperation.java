package pc.strategynew.operation;

import pc.control.RobotController;
import pc.strategynew.StrategyState;

/**
 * Do absolutely nothing (don't even send a command).
 */
public class DoNothingOperation extends Operation{

	@Override
	public OperationType getType() {
		return OperationType.DO_NOTHING;
	}

	@Override
	public boolean execute(RobotController<?, ?> robotController, StrategyState currentState) {
		// DO NOTHING
		return true;
	}
	
	//These operations have no other parameters, so any command of the same type is equal.
	@Override
	public boolean equals(Object obj) {
		return obj != null && obj instanceof DoNothingOperation;
	}
}
