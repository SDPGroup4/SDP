package pc.strategynew.operation;

import pc.command.KickerCommand;
import pc.command.RobotCommand;
import pc.control.RobotController;
import pc.movement.ActionSpeed;
import pc.movement.ActionType;
import pc.movement.RobotAction;
import pc.strategynew.StrategyState;

public class KickerOperation extends Operation {
	
	// How we act
	public final ActionSpeed actionSpeed;
	public final ActionType actionType;
	
	public KickerOperation(ActionType actionType, ActionSpeed actionSpeed) {
		this.actionSpeed = actionSpeed;
		this.actionType = actionType;
	}
	
	// Act at default 
	public KickerOperation(ActionType actionType) {
		this(actionType, ActionSpeed.FULL);
	}

	@Override
	public String toString() {
		return super.toString()+" "+actionType+" "+actionSpeed;
	}

	//Check if the movement type and speed are the same.
	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof KickerOperation){
			KickerOperation mov = (KickerOperation) obj;
			return mov.getType() == getType() && mov.actionSpeed == actionSpeed;
		}
		return false;
	}
	
	@Override
	public OperationType getType() {
		switch(actionType){
		case KICK: return OperationType.KICK;
		case CATCH:  return OperationType.CATCH;
		case RESET_CATCHER: return OperationType.RESET_CATCHER;
		case NONE:		return OperationType.NONE;
		}
		return OperationType.DO_NOTHING;
	}
	
	//Send the right command to the robot controller to perform the appropriate motion.
	//Does duplicate detection to avoid telling the robot to do the same movement twice.
	@SuppressWarnings("incomplete-switch")
	@Override
	public boolean execute(RobotController<?, ?> robotController, StrategyState currentState) {
		RobotAction act = new RobotAction(actionType, actionSpeed);
		//if(currentState.robotMovement.equals(mov)){
			RobotCommand command = null;
			switch(actionType){
			case KICK: command = new KickerCommand.Kick(actionSpeed.getPower()); break;
			case CATCH:  command = new KickerCommand.Catch(); break;
			case RESET_CATCHER: command = new KickerCommand.ResetCatcher(); break;
			}
			if(command != null){
				boolean received = robotController.executeSync(command);
				if(received){
					currentState.robotAction = act;
					switch(actionType){
					case KICK:
						currentState.catchAttempted = false;
						currentState.catcherReady = false;
						currentState.lastKickedBallTime = System.currentTimeMillis();
						break;
					case CATCH:
						currentState.catchAttempted = true;
						currentState.catcherReady = false;
						break;
					case RESET_CATCHER:
						currentState.catchAttempted = false;
						currentState.catcherReady = true;
						break;
					}
				}
				return received;
			}else{
				System.out.println("Null command.");
			}
		//}
		return false;
	}
	
	
}
