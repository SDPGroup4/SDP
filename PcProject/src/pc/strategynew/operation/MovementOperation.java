package pc.strategynew.operation;

import pc.command.MovementCommand;
import pc.command.RobotCommand;
import pc.control.RobotController;
import pc.movement.MovementType;
import pc.movement.RobotMovement;
import pc.strategynew.StrategyState;


/**
 * An operation for every type of movement.
 * Can send commands of the correct movement type at the correct speeds.
 */
public class MovementOperation extends Operation{

	//How to move.
	public final int speed;
	public final MovementType movementType;
	
	//Move at the specified speed (or ZERO if stopped).
	public MovementOperation(MovementType movementType, int speed){
		this.movementType = movementType;
		this.speed = movementType == MovementType.STOPPED ? 0 : speed;
	}

	@Override
	public String toString() {
		return super.toString()+" "+movementType+" "+speed;
	}

	//Check if the movement type and speed are the same.
	@Override
	public boolean equals(Object obj) {
		if(obj != null && obj instanceof MovementOperation){
			MovementOperation mov = (MovementOperation) obj;
			return mov.getType() == getType() && mov.speed == speed;
		}
		return false;
	}
	
	@Override
	public OperationType getType() {
		switch(movementType){
		case BACKWARDS: return OperationType.TRAVEL;
		case FORWARDS:  return OperationType.TRAVEL;
		case LEFT:		return OperationType.ROTATE;
		case RIGHT:		return OperationType.ROTATE;
		case STOPPED:	return OperationType.STOP;
		}
		return OperationType.DO_NOTHING;
	}

	//Send the right command to the robot controller to perform the appropriate motion.
	//Does duplicate detection to avoid telling the robot to do the same movement twice.
	@Override
	public boolean execute(RobotController<?, ?> robotController, StrategyState currentState) {
		RobotMovement mov = new RobotMovement(movementType, speed);
		//if(currentState.robotMovement.equals(mov)){
			RobotCommand command = null;
			switch(movementType){
			case BACKWARDS: command = new MovementCommand.Backwards(speed); break;
			case FORWARDS:  command = new MovementCommand.Forwards(speed); break;
			case LEFT:		command = new MovementCommand.RotateLeft(speed); break;
			case RIGHT:		command = new MovementCommand.RotateRight(speed); break;
			case STOPPED:	command = new MovementCommand.Stop(); break;
			}
			if(command != null){
				boolean received = robotController.executeSync(command);
				if(received){
					currentState.robotMovement = mov;
				}
				return received;
			}else{
				System.out.println("Null command.");
			}
		//}
		return false;
	}
}
