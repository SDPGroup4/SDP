package pc.strategynew.operation;

import pc.control.RobotController;
import pc.strategynew.StrategyState;

/**
 * The second lowest level of the robot's decision making heirarchy.
 * These act as wrappers around robot Commands, or lists
 * of robot Commands, allowing for more complex sequences of movement
 * (e.g. the ConfuseKick last years' robot did). These will
 * deal with the translation of the Strategy system's intentions
 * to the robot's Command primitives.
 * 
 * Each operation must have a corresponding OperationType from the below
 * enum, and return it in the getType() method.
 * 
 * Every operation must also have an execute(RobotController, StrategyState)
 * method that will execute its series of Commands on the RobotController
 * and update the StrategyState if necessary so that other levels in the
 * decision making hierarchy can keep track of what is going on.
 * 
 * 
 * The full heirarchy is:
 * 
 * Strategy    (e.g. Attacking, Marking, Passing)
 * Plan		   (e.g. CatchBall, ReturnToOrigin, ScoreGoal)
 * Operation   (e.g. Forwards, TurnLeft, Stop, ConfuseKickLeft)
 * Command	   (the Arduino must be programmed for these e.g. KICK, FORWARDS, STOP).
 */
public abstract class Operation {

	public enum OperationType{
		//All possible types of operation.
		//Add more to this list once more have been implemented.
		DO_NOTHING("DO_NOTHING_OP"), 
		TRAVEL("TRAVEL_OP"), 
		ROTATE("ROTATE_OP"),
		STOP("STOP_OP"),
		KICK("KICK"),
		CATCH("CATCH"),
		RESET_CATCHER("RESET_CATCHER"),
		MOVE_AND_KICK("MOVE_AND_KICK"),
		NONE("NONE")
		/* Potential other ones (based on what last year used).
		RESET_CATCHER("RESET_CATCHER"),
 
		ARC_LEFT("ARC_LEFT"), 
		ARC_RIGHT("ARC_RIGHT"),
		
 		MOVEKICK("MOVEKICK"), 
		ROTATENMOVE("ROTATENMOVE"), 
		MOVENROTATE("MOVENROTATE"), 

		CONFUSEKICKRIGHT("CONFUSEKICKRIGHT"), 
		CONFUSEKICKLEFT("CONFUSEKICKLEFT")
		*/
		;
		
		private final String name;
		private OperationType(String name){
			this.name = name;
		}
		@Override
		public String toString() {
			return name;
		}
	}


	
	/**
	 * @return The enum value describing what this operation does.
	 */
	public abstract OperationType getType();
	
	@Override
	public String toString() {
		return getType().toString();
	}
	
	/**
	 * Execute the necessary Command or series of Commands on the 
	 * RobotController, and update the StrategyState with any relevant
	 * information to allow the higher levels of the decision making
	 * hierarchy to know what is going on.
	 * 
	 * Returns true if the robot received the command, and false if it did not.
	 * 
	 * @param robotController The controller to execute Commands on.
	 * @param currentState  The current state of the strategy systems. 
	 * 					    Can be updated here to inform the rest of the strategy system,
	 * 						and may also influence how this Operation is executed.
	 */
	public abstract boolean execute(RobotController<?, ?> robotController, StrategyState currentState);

	
}
