package pc.strategynew.operation;

import pc.command.KickerCommand;
import pc.command.MoveAndKickCommand;
import pc.command.MovementCommand;
import pc.command.RobotCommand;
import pc.control.RobotController;
import pc.movement.ActionSpeed;
import pc.movement.ActionType;
import pc.movement.MovementType;
import pc.movement.RobotAction;
import pc.movement.RobotMovement;
import pc.strategynew.StrategyState;




public class MoveAndKickOperation extends Operation{

	private final MovementOperation movOp;
	private final KickerOperation kickOp;
	private final int millisecondsToWait;

	public MoveAndKickOperation(MovementOperation movOp, KickerOperation kickOp, int millisecondsToWait){
		this.movOp = movOp;
		this.kickOp = kickOp;
		this.millisecondsToWait = millisecondsToWait;
	}

	@Override
	public OperationType getType() {
		return OperationType.MOVE_AND_KICK;
	}

	@SuppressWarnings("incomplete-switch")
	@Override
	public boolean execute(RobotController<?, ?> robotController, StrategyState currentState) {		

		int power = movOp.speed;
		MovementType movementType = movOp.movementType;

		RobotMovement mov = new RobotMovement(movementType, power);

		MovementCommand movCommand = null;
		switch(movementType){
		case BACKWARDS: movCommand = new MovementCommand.Backwards(power); break;
		case FORWARDS:  movCommand = new MovementCommand.Forwards(power); break;
		case LEFT:		movCommand = new MovementCommand.RotateLeft(power); break;
		case RIGHT:		movCommand = new MovementCommand.RotateRight(power); break;
		case STOPPED:	movCommand = new MovementCommand.Stop(); break;
		}

		ActionType actionType = kickOp.actionType;
		ActionSpeed actionSpeed = kickOp.actionSpeed;
		
		RobotAction act = new RobotAction(actionType, actionSpeed);
		//if(currentState.robotMovement.equals(mov)){
		KickerCommand kickCommand = null;
		switch(actionType){
		case KICK: kickCommand = new KickerCommand.Kick(actionSpeed.getPower()); break;
		case CATCH:  kickCommand = new KickerCommand.Catch(); break;
		case RESET_CATCHER: kickCommand = new KickerCommand.ResetCatcher(); break;
		}

		RobotCommand hybridCommand = new MoveAndKickCommand(movCommand, kickCommand, millisecondsToWait);
		boolean received = robotController.executeSync(hybridCommand);

		if(received){
			currentState.robotMovement = mov;
			
			currentState.robotAction = act;
			switch(actionType){
			case KICK:
				currentState.catchAttempted = false;
				currentState.catcherReady = false;
				currentState.lastKickedBallTime = System.currentTimeMillis();
				
				//Sleep after kicking for a while to stop the robot chasing after the ball.
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				break;
			case CATCH:
				currentState.catchAttempted = true;
				currentState.catcherReady = false;
				break;
			case RESET_CATCHER:
				currentState.catchAttempted = false;
				currentState.catcherReady = true;
				break;
			}
			
		}
		
		return received;
	}




}
