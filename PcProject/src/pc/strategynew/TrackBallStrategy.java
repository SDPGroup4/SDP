package pc.strategynew;

import pc.strategynew.plan.Plan;
import pc.strategynew.plan.TrackBallPlan;
import pc.world.oldmodel.WorldState;

/**
 * When the ball is in the opposition attacking zone, our robot should stay in line with the 
 * y-coordinate of the ball. Used for getting ready to receive a pass from team defender
 */

public class TrackBallStrategy extends Strategy{
	
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState){
		Plan currentPlan = new TrackBallPlan(strategyState);
		return currentPlan;
	}	
	
	
	@Override
	StrategyType getType() {
		return StrategyType.TRACKBALL;
	}
	
	
}
