package pc.strategynew;

import pc.strategynew.plan.GetIntoSpacePlan;
import pc.strategynew.plan.Plan;
import pc.world.oldmodel.WorldState;

/**
 * Our robot must move to find a clear line of sight between our robot and team defender.
 * This is because an enemy attacker may try to get in between our defender and attacker
 * and so we must find space.
 * 
 */


public class FindSpaceStrategy extends Strategy {

		
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState){
		Plan currentPlan = new GetIntoSpacePlan(worldState, strategyState.ourRobotZone);
		return currentPlan;
	}	


	@Override
	StrategyType getType() {
		return StrategyType.FINDSPACE;
	}


}
