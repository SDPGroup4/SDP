package pc.strategynew;


import pc.strategynew.plan.InBetweenEnemiesPlan;
import pc.strategynew.plan.Plan;
import pc.world.oldmodel.WorldState;

/**
 * Class to try and get the attacker to intercept the ball between the enemy robots when the
 * enemy defender has the ball and is trying pass to attacker, similar to the 'MarkingStrategy'
 * found in the previous system. 
 * 
 * Still quite simple, only aims for the mid point between the robots and travels there.
 */


public class InterceptorStrategy extends Strategy {

	
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState){
		Plan currentPlan = new InBetweenEnemiesPlan(strategyState, worldState);
		return currentPlan;
	}	


	@Override
	StrategyType getType() {
		return StrategyType.INTERCEPTOR;
	}


}
	

