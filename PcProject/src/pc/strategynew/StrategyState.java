package pc.strategynew;

import pc.movement.ActionSpeed;
import pc.movement.ActionType;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.movement.MovementType;
import pc.movement.RobotAction;
import pc.movement.RobotMovement;
import pc.strategynew.operation.DoNothingOperation;
import pc.strategynew.operation.Operation;
import pc.strategynew.plan.DoNothingPlan;
import pc.strategynew.plan.Plan;
import pc.strategynew.plan.PlanUtils;
import pc.world.PitchZone;
import pc.world.PitchZone.PitchZoneType;
import pc.world.oldmodel.MovingObject;
import pc.world.oldmodel.WorldState;

/**
 * Holds all the values necessary for every stage of the decision
 * making hierarchy to choose what to do next.
 * 
 * This will need to be extended to hold useful information that
 * needs to be passed between layers of the system. This is an attempt
 * to be able to pass relevant data between layers without having to use huge numbers
 * of local variables (see pc.strategy.GeneralStrategy to seem what this attempts to avoid).
 * 
 * There may be a better way of passing this information between layers, 
 * but this method should work in the mean time.
 *  
 * 
 * The StrategyState can be altered from any level of the decision hierarchy as
 * a way of passing message (both up or down the hierarchy).
 */
public class StrategyState {
	
	//Which robot is this state for?
	
	// Our Robot
	public PitchZone ourRobotZone = null;
	public MovingObject ourRobot = null;
	public int angleToBall = 0;
	public boolean ourRobotWithinZone = true;
	public boolean travelForwardsToReturn = true;
	public MovingObject predictedPointInBallPath = null;
	
	// Ally Robot
	public PitchZone allyRobotZone = null;
	public MovingObject allyRobot = null;
	
	// Enemy Attacker
	public PitchZone opponentAttackerRobotZone = null;
	public MovingObject opponentAttackerRobot = null;
	
	// Enemy Defender
	public PitchZone opponentDefenderRobotZone = null;
	public MovingObject opponentDefenderRobot = null;

	
	//Where is the ball?
	public MovingObject ball = null;
	public MovingObject predictedBallLocation = null;
	public PitchZoneType ballZone = PitchZoneType.DEFENDER;
	public boolean ballNearEdgeOfPitch = false;
	public boolean ballOnPitch = false;
	public boolean ballSeenNearby = false;
	
	public long lastKickedBallTime = 0;
	public long lastTimeBallWasInEnemyDefenderZone = 0;
	public long lastMetThresholdTime = 0;
	
	public static long scoreGoalTimer = 0;
	
	// Set goals
	public float[] enemyGoal;
	public float[] ourGoal;
	
	private Strategy currentStrategy = new DoNothingStrategy();
	private Plan currentPlan = new DoNothingPlan();
	private Operation currentOperation = new DoNothingOperation();
	
	//Do we have the ball?
	public boolean hasCaughtBall = false;
	public boolean catcherReady = false;
	public boolean catchAttempted = false;
	public boolean isInSpace = false;
	
	public long shouldStopAfterMs = 0;

	//How are we moving?
	public RobotMovement robotMovement = new RobotMovement(MovementType.STOPPED, MovementSpeed.ZERO);
	
	//How are we acting?
	public RobotAction robotAction = new RobotAction(ActionType.NONE, ActionSpeed.ZERO);
	
	// Robot angle to the goal
	public int angleToGoalMid;
	
	// Robot angle to upper side of the goal
	public int angleToGoalUpper;
	
	// Robot angel to bottom side of the goal
	public int angleToGoalBottom;
	
	// the border side x axis we want to shot
	float goalX;

	/**
	 * Update current state for the strategy system based on its existing value and
	 * the new state of the world.
	 * 
	 * @param worldState The new world state to adapt to.
	 * @param ourRobotZoneType Which robot this strategy state is for.
	 */
	public synchronized void update(WorldState worldState, PitchZoneType ourRobotZoneType){
		ourRobotZone = new PitchZone(ourRobotZoneType, worldState);
		ourRobot = StrategyUtils.getRobot(ourRobotZoneType, worldState);

		PitchZoneType allyZoneType = StrategyUtils.getAllyFor(ourRobotZoneType);
		allyRobotZone = new PitchZone(allyZoneType, worldState);
		allyRobot = StrategyUtils.getRobot(allyZoneType, worldState);
		
		opponentAttackerRobotZone = new PitchZone(PitchZoneType.ENEMY_ATTACKER, worldState);
		opponentAttackerRobot = StrategyUtils.getRobot(PitchZoneType.ENEMY_ATTACKER, worldState);
		
		opponentDefenderRobotZone = new PitchZone(PitchZoneType.ENEMY_DEFENDER, worldState);
		opponentDefenderRobot = StrategyUtils.getRobot(PitchZoneType.ENEMY_DEFENDER, worldState);
		
		ball = StrategyUtils.getBall(worldState);
		predictedBallLocation = StrategyUtils.getPredictedLocation(worldState);
		//System.out.println("X: " + predictedBallLocation.x + " Y: " + predictedBallLocation.y);
		predictedPointInBallPath = StrategyUtils.closePointToLine(ball, predictedBallLocation, ourRobot);
		ballZone = StrategyUtils.getPitchZone(worldState.getBall(), worldState);
		ballNearEdgeOfPitch = StrategyUtils.nearSidesOfPitch(worldState.getBall());
		ballOnPitch = StrategyUtils.isballOnPitch(worldState);
		
		if(ballZone == PitchZoneType.ENEMY_DEFENDER){
			lastTimeBallWasInEnemyDefenderZone = System.currentTimeMillis();
		}
		
		angleToBall = StrategyUtils.calculateAbsoluteAngleDeg(ourRobot.x, ourRobot.y, ball.x, ball.y);
		
		ourRobotWithinZone = StrategyUtils.robotIsWithinZone(ourRobot, worldState);
		
		if (worldState.weAreShootingRight) {
			ourGoal = worldState.leftGoal;
			enemyGoal = worldState.rightGoal;
			goalX = opponentDefenderRobotZone.getRightEdge();
		} else {
			ourGoal = worldState.rightGoal;
			enemyGoal = worldState.leftGoal;
			goalX = opponentDefenderRobotZone.getLeftEdge();
		}
		// Using enemyGoal[2] for mid shots, attacker strategy decide to chose which to chose
		angleToGoalMid = StrategyUtils.calculateAbsoluteAngleDeg(ourRobot.x, ourRobot.y, goalX, enemyGoal[2]);
		
		// Using enemyGoal[0] for left side (up) shots, attacker strategy decide to chose which to chose
		angleToGoalUpper = StrategyUtils.calculateAbsoluteAngleDeg(ourRobot.x, ourRobot.y, goalX, enemyGoal[1]);
		
		// Using enemyGoal[2] for right side (bottom) shots, attacker strategy decide to chose which to chose
		angleToGoalBottom = StrategyUtils.calculateAbsoluteAngleDeg(ourRobot.x, ourRobot.y, goalX, enemyGoal[0]);
		

		// If ball is in defender, enemy defender, or enemy attacker zone,
		// it is not nearby
		if (StrategyUtils.isballOutsideOurZone(worldState)){
			ballSeenNearby = false;
		} 
		// Otherwise, it is either in our zone, or hidden from view by us
		else {
			ballSeenNearby = DistanceThreshold.ROUGH.isCloseEnough(ourRobot, ball);
		}
		
		// Only when the ball is in our zone and the ball is near the robot (currently 35mm) and we
		// attempted to catch the ball do we assume we have caught the ball
		// OR if ball was recently in our pitch, is out of view now, and we have attempted to catch
		hasCaughtBall = (!StrategyUtils.isballOutsideOurZone(worldState) 
				&& StrategyUtils.isBallNearRobot(ourRobot, ball)
				&& catchAttempted)
				|| (ballSeenNearby 
				&& !StrategyUtils.isballOnPitch(worldState)
				&& catchAttempted);
		
		isInSpace = PlanUtils.isInSpace(worldState, this);
		
		// Use the robot's last orientation and distance from zone dividers to determine if
		// it needs to travel forwards or backwards to return to zone after escaping.
		
		// If facing right:
		if (((ourRobot.orientationDegrees < 90)||(ourRobot.orientationDegrees > 270))&&(ourRobot.orientationDegrees!=0)){
			// and closer to right divider:
			if ((Math.abs(ourRobot.x - StrategyUtils.getOurZoneDividers(worldState)[1])
					< Math.abs(ourRobot.x - StrategyUtils.getOurZoneDividers(worldState)[0]))
					&& (ourRobot.x != 0)){
				// travel backwards to return.
				travelForwardsToReturn = false;			
			}
			// and closer to left divider:
			else{
				// travel forwards to return. 
				travelForwardsToReturn = true;
			}
		}
		// If facing left:
		else{
			// and closer to right divider:
			if ((Math.abs(ourRobot.x - StrategyUtils.getOurZoneDividers(worldState)[1])
					< Math.abs(ourRobot.x - StrategyUtils.getOurZoneDividers(worldState)[0]))
					&& (ourRobot.x != 0)){
				// travel backwards to return.
				travelForwardsToReturn = true;			
			}
			// and closer to left divider:
			else{
				// travel forwards to return. 
				travelForwardsToReturn = false;
			}	
		}
	
	}
	
	//Synchronized getters and setters for all levels of the hierarchy.
	public synchronized Strategy getCurrentStrategy() {
		return currentStrategy;
	}
	public synchronized void setCurrentStrategy(Strategy currentStrategy) {
		this.currentStrategy = currentStrategy;
	}
	public synchronized Plan getCurrentPlan() {
		return currentPlan;
	}
	public synchronized void setCurrentPlan(Plan currentPlan) {
		this.currentPlan = currentPlan;
	}
	public synchronized Operation getCurrentOperation() {
		return currentOperation;
	}
	public synchronized void setCurrentOperation(Operation currentOperation) {
		this.currentOperation = currentOperation;
	}
}
