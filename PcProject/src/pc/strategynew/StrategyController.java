package pc.strategynew;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

import pc.control.RobotController;
import pc.strategynew.operation.DoNothingOperation;
import pc.strategynew.operation.Operation;
import pc.strategynew.plan.DoNothingPlan;
import pc.strategynew.plan.Plan;
import pc.vision.interfaces.WorldStateReceiver;
import pc.world.PitchZone.PitchZoneType;
import pc.world.oldmodel.WorldState;

/**
 * The heart of the robot's decision making process.
 * 
 * Takes charge of a single robot via a RobotController, and
 * responds in changes to the world state by telling the robot
 * what to do.
 *
 * Each time the world state changes, the strategy controller:
 * 
 * - Updates the current StrategyState based on the world state.
 * - Selects the most appropriate Strategy using a StrategySelector
 * - Selects the most appropriate Plan using this Strategy.
 * - Selects the most appropriate Operation using this Plan
 * - Stores the current Strategy, Plan, and Operation in the StrategyState
 * 
 * In separate thread which runs once every STRATEGY_TICK milliseconds (which can
 * be set via the GUI), the strategy controller
 * - Retrieves the current Operation from the StrategyState
 * - Executes this Operation on the RobotController (which may send a Command or a series of Commands).
 * - Allows the Operation to update the StrategyState with any relevant information (e.g. when it last kicked).

 * 
 *  * The robot's decision making hierarchy is:
 * 
 * Strategy    (e.g. Attacking, Marking, Passing)
 * Plan		   (e.g. CatchBall, ReturnToOrigin, ScoreGoal)
 * Operation   (e.g. Forwards, TurnLeft, Stop, ConfuseKickLeft)
 * Command	   (the Arduino must be programmed for these e.g. KICK, FORWARDS, STOP).
 */
public class StrategyController implements WorldStateReceiver, Runnable{

	//How long (in milliseconds to wait between executing operations.
	//The process of deciding which Strategy, Plan, and Operation to 
	//use is run every frame of vision, but the actual execution of 
	//commands is run in a separate Thread that repeats with this delay:
	public static int STRATEGY_TICK = 85;

	//Where on the pitch the robot this controller drives is.
	public PitchZoneType robotZone;

	//Controller for telling the robot what to do once a decision is made.
	public RobotController<?,?> robotController;


	//Allows the controller to be paused and resumed. It will send no
	//commands at all when it is paused.
	private boolean pauseStrategyController = true;
	private boolean enableAutomaticStrategySelection = true;

	//How this controller selects its strategies. This
	//may need tweaked later to allow manual selection of 
	//strategies through the GUI.
	private StrategySelector strategySelector = new StrategySelector();

	//Used mainly for updating GUI elements whenever the strategy changes.
	private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);


	public StrategyController(RobotController<?,?> robotController, PitchZoneType robotZone) {
		this.robotController = robotController;
		this.robotZone = robotZone;
	}

	public boolean isPaused() {
		return pauseStrategyController;
	}
	
	public boolean isAutomatic(){
		return enableAutomaticStrategySelection;
	}

	//Whenever the strategy is paused or resumed, make sure
	//any relevant GUI elements are updated.
	public void setPaused(boolean paused) {
		boolean oldValue = pauseStrategyController;
		pauseStrategyController = paused;

		if(paused){
			stopOperationExecutionThread();
		}else{
			startOperationExecutionThread();
		}

		pcs.firePropertyChange("paused", oldValue, paused);
	}

	private Thread operationExecutionThread = null;
	private boolean operationExecutionThreadShouldRun = false;

	private void startOperationExecutionThread(){
		if(operationExecutionThread != null){
			stopOperationExecutionThread();
		}
		operationExecutionThreadShouldRun = true;
		operationExecutionThread = new Thread(this);
		operationExecutionThread.start();
	}

	private void stopOperationExecutionThread(){
		operationExecutionThreadShouldRun = false;
		if(operationExecutionThread != null){
			try {
				operationExecutionThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		operationExecutionThread = null;
	}

	//Allowing other pieces of code to see when the strategy code changes its mind.
	//Mainly used by GUI elements.
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		pcs.addPropertyChangeListener(listener);
	}
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		pcs.removePropertyChangeListener(listener);
	}

	public void setAutomaticStrategySelection(boolean isOn){
		enableAutomaticStrategySelection = isOn;
		pcs.firePropertyChange("auto", !isOn, isOn);
	}
	
	public void setStrategy(Strategy strategy){
		currentStrategyState.setCurrentStrategy(strategy);
		pcs.firePropertyChange("currentStrategy", 1, 2); //Used to trigger GUI updates. Values mean nothing.
	}

	//The Strategy state from the last time it was updated.
	StrategyState currentStrategyState = new StrategyState();

	/**
	 *  This method is the top of the decision making hierarchy
	 *  
	 *  Each time the world state changes, the strategy controller:
	 * 
	 * - Updates the current StrategyState based on the world state.
	 * - Selects the most appropriate Strategy using a StrategySelector
	 * - Selects the most appropriate Plan using this Strategy.
	 * - Selects the most appropriate Operation using this Plan
	 * - Executes this Operation on the RobotController (which may send a Command or a series of Commands).
	 */
	@Override
	public void sendWorldState(WorldState worldState) {
		//Do not decide or execute anything if paused.
		if (pauseStrategyController){
			return;
		}

		currentStrategyState.update(worldState, robotZone);

		Strategy currentStrategy;
		if(enableAutomaticStrategySelection){
			currentStrategy = strategySelector.getStrategy(worldState, currentStrategyState);
			currentStrategy = currentStrategy != null ? currentStrategy : new DoNothingStrategy();
			currentStrategyState.setCurrentStrategy(currentStrategy);
		}else{
			currentStrategy = currentStrategyState.getCurrentStrategy();
		}
		
		Plan currentPlan = currentStrategy.getNextPlan(worldState, currentStrategyState);
		currentPlan = currentPlan != null ? currentPlan : new DoNothingPlan();
		currentStrategyState.setCurrentPlan(currentPlan);

		Operation op = currentPlan.getNextOperation(worldState, currentStrategyState);
		op = op != null ? op : new DoNothingOperation();
		currentStrategyState.setCurrentOperation(op);

		pcs.firePropertyChange("currentStrategy", 1, 2); //Used to trigger GUI updates. Values mean nothing.
	}

	public Strategy getCurrentStrategy() {
		return currentStrategyState.getCurrentStrategy();
	}

	public Plan getCurrentPlan(){
		return currentStrategyState.getCurrentPlan();
	}

	public Operation getCurrentOperation(){
		return currentStrategyState.getCurrentOperation();
	}

	//Execute the most recent Operation from the StrategyState
	//Runs once every STRATEGY_TICK milliseconds (which can be tweaked from the GUI).
	@Override
	public void run() {
		while(operationExecutionThreadShouldRun){
			long startTime = System.currentTimeMillis();
			if(robotController != null){
				//synchronized (currentStrategyState) { //Don't allow anything to change the strategy state until the op has been executed.
				Operation op = getCurrentOperation();
				if(op != null){
					System.out.println("Executing op from run thread: "+op);
					op.execute(robotController, currentStrategyState);
				}
				//}
			}
			long duration = startTime - System.currentTimeMillis();
			if(duration < STRATEGY_TICK){
				try{
					Thread.sleep(STRATEGY_TICK - duration);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		}
	}
}
