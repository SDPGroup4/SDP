package pc.strategynew;


import pc.movement.ActionSpeed;
import pc.strategynew.plan.CatchBallPlan;
import pc.strategynew.plan.DoNothingPlan;
import pc.strategynew.plan.GetIntoSpacePlan;
import pc.strategynew.plan.KickToAllyPlan;
import pc.strategynew.plan.Plan;
import pc.strategynew.plan.ReturnToOriginPlan;
import pc.world.oldmodel.WorldState;


/**
 * Class to pass the ball to the ally defender robot for the milestone 3, with enemy stationary robot in line
 * 
 * Idea:
 * Move to the correct area and kick to the ally robot, keep the facing perpendicular to Y axis
 */

public class PassObstacleStrategy extends Strategy {
	
	private static final int TIME_TO_WAIT_BETWEEN_KICKS = 1000;
	
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState){
		Plan currentPlan = new DoNothingPlan(); // out of scope of the pass strategy, do nothing
		
		if(strategyState.hasCaughtBall || strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball)){ // ball in our zone
			if(strategyState.hasCaughtBall){ // we have the ball to pass, we need move and kick
				if (strategyState.isInSpace){ // kick Plan		
					currentPlan = new KickToAllyPlan(strategyState, ActionSpeed.FULL); // not powerful kicker
				} else { // move to avoid enemy robot
					System.out.println("HERE");
					currentPlan = new GetIntoSpacePlan(worldState, strategyState.ourRobotZone);
					
					// now we are okay to kick to ally
				}
			} else {
				long currentTime = System.currentTimeMillis();
				long timeSinceLastkick = currentTime - strategyState.lastKickedBallTime;
				if(timeSinceLastkick >= TIME_TO_WAIT_BETWEEN_KICKS){
				currentPlan = new CatchBallPlan(strategyState);
				}else{
					System.out.println("Waiting for kickTimer "+timeSinceLastkick+" to get above "+TIME_TO_WAIT_BETWEEN_KICKS);;
				}
			}
		}else{
			//System.out.println("Has caught ball = "+strategyState.hasCaughtBall+" ball in our zone = "+strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball));
			currentPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);
		}
		return currentPlan;
	}
		
		
	
	@Override
	StrategyType getType() {
		return StrategyType.MILESTONEOBSTACLEPASSING;
	}
	
}

