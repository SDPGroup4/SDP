package pc.strategynew;

import pc.strategynew.plan.CatchBallPlan;
import pc.strategynew.plan.DoNothingPlan;
import pc.strategynew.plan.MoveToGetRunupPlan;
import pc.strategynew.plan.Plan;
import pc.strategynew.plan.ReturnToOriginPlan;
import pc.strategynew.plan.ScoreGoalPlan;
import pc.world.PitchZone;
import pc.world.PitchZone.PitchZoneType;
import pc.world.oldmodel.WorldState;

/**
 * The default strategy used for an attacking robot when the
 * ball is in its court.
 * 
 * At the moment, no other strategies do anything, so this is the default.
 * It has not been written yet, so will constantly attempt to return to the
 * origin. This will need filled in (potentially by looking at pc.strategy.AttackerStrategy.
 */
public class AttackerStrategy extends Strategy{
	
	private static final int TIME_TO_WAIT_BETWEEN_KICKS = 1000;
	
	//Return to the origin point for now. No other behaviours have been written.
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState) {
		
		
		Plan nextPlan = null;
		
		long currentTime = System.currentTimeMillis();
		long timeSinceLastkick = currentTime - strategyState.lastKickedBallTime;
		if(timeSinceLastkick < TIME_TO_WAIT_BETWEEN_KICKS){
			new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);
		}else if (!strategyState.ballOnPitch && !strategyState.hasCaughtBall) {
			nextPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);	
		} else {
			// Ball is in our defender's zone
			if (StrategyUtils.getPitchZone(strategyState.ball, worldState) == PitchZoneType.DEFENDER) {
				// Track our defender's angle to prepare for a bounce pass
				//nextPlan = new TrackAllyAnglePlan(strategyState);
				nextPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);	
			}
			// Ball is outside our zone
			else if (StrategyUtils.isballOutsideOurZone(worldState)) {
				nextPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);	
			} else {
				// Ball is in our zone
				if (!strategyState.hasCaughtBall) {
					nextPlan = new CatchBallPlan(strategyState);
				} else{ // has caught the ball
						
					// If timer is running, and has been for <2 seconds, return to origin
					if (((System.currentTimeMillis() - strategyState.lastMetThresholdTime) < 2000)&&(strategyState.lastMetThresholdTime != 0)){
						nextPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);
					}
					else {
						// If edge threshold timer has run for >=2 seconds, turn it off and prepare to score
						strategyState.lastMetThresholdTime = 0;
						//System.out.println("\n\nNONONo\n\n");
						PitchZone enemyGoalZone = new PitchZone(PitchZoneType.ENEMY_DEFENDER, worldState);			
						if(StrategyUtils.hasEnoughOfARunUp(strategyState, enemyGoalZone)){
							nextPlan = new ScoreGoalPlan(strategyState, worldState);				
						}else{
							nextPlan = new MoveToGetRunupPlan(strategyState, enemyGoalZone);
						}
					}
					// If near edge, start edge threshold timer (unless timer already running)
					if((StrategyUtils.robotNearEdges(strategyState))&&(strategyState.lastMetThresholdTime==0)){
						strategyState.lastMetThresholdTime = System.currentTimeMillis();
					}
				}
			}
		}
		return nextPlan != null ? nextPlan : new DoNothingPlan();
	}

	@Override
	StrategyType getType() {
		return StrategyType.ATTACKING;
	}
}
