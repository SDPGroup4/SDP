package pc.strategynew.plan;

import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.world.PitchZone;

public class MoveToGetRunupPlan extends TravelToPlan {
	public MoveToGetRunupPlan(StrategyState strategyState, PitchZone zoneToAimTowards){
		super(strategyState.ourRobotZone.getRunUpXCoordinateForKickingTowards(zoneToAimTowards),
				strategyState.ourRobot.y,
				MovementSpeed.SLOW,
				DistanceThreshold.ROUGH,
				AngleThreshold.ROUGH,
				true);
	}
}
