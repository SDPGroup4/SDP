
package pc.strategynew.plan;

import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;
import pc.world.PitchZone;

/**
 * Go to the central point of the robot's pitch zone.
 */
public class ReturnNearOriginPlan extends TravelToPlan{
	
	
	public ReturnNearOriginPlan(PitchZone zone, StrategyState strategyState){
		super(zone.getOriginPoint(),
				StrategyUtils.findClosest90Angle(strategyState.ourRobot),
				//StrategyUtils.calculateAbsoluteAngleDeg(state.ourRobot, state.allyRobot),
				MovementSpeed.SLOW, 
				DistanceThreshold.VERYROUGH, 
				AngleThreshold.ROUGH, 
				true);
	}
	
	@Override
	public String toString() {
		return "RETURN_NEAR_ORIGIN "+super.toString();
	}
}