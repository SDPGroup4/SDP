package pc.strategynew.plan;

import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.strategynew.operation.Operation;
import pc.world.PitchZone;
import pc.world.oldmodel.WorldState;



/**
 * Move the robot to a space where there should be a clear line of sight between our robot
 * and team enemy robot. Splits our zone in 3 equal areas (may have more areas to increase
 * granularity and accuracy). Our robot should always strive be in the centre of one of
 * these areas so that the defender can find us.
 * 
 */
public class GetIntoSpacePlan extends TravelToPlan{

	public GetIntoSpacePlan(WorldState worldState, PitchZone zone){
		super(PlanUtils.FindSpace(worldState, zone), 
				MovementSpeed.SLOW, 
				DistanceThreshold.MEDIUM, 
				AngleThreshold.ROUGH, 
				true);
	}
	
	@Override
	public Operation getNextOperation(WorldState worldState, StrategyState strategyState) {
		/*
		if(isAtDestination(strategyState) && strategyState.robotMovement.type == MovementType.STOPPED){
			strategyState.isInSpace = true;
		}else{
			strategyState.isInSpace = false;
		}
		*/
		return super.getNextOperation(worldState, strategyState);
	}
	
	@Override
	public String toString() {
		return "GET_INTO_SPACE "+super.toString();
	}
	
	
}
