package pc.strategynew.plan;


import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;


public class TrackAllyAnglePlan extends TravelToPlan {
	public TrackAllyAnglePlan(StrategyState strategyState){ 
		super(strategyState.ourRobotZone.getOriginPoint(),
			  StrategyUtils.clampToDegreeRange((strategyState.allyRobot.orientationDegrees*-1 + 180)),
			  MovementSpeed.VERYSLOW,
			  DistanceThreshold.ROUGH, 
			  AngleThreshold.ROUGH,
			  true);
	}

	
	@Override
	public String toString() {
		return "TRACKING_ALLY_ROBOT_ANGLE "+super.toString();
	}
}

