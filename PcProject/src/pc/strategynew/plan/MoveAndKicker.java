package pc.strategynew.plan;

import java.awt.Point;

import pc.movement.ActionSpeed;
import pc.movement.ActionType;
import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.movement.MovementType;
import pc.strategynew.StrategyState;
import pc.strategynew.operation.DoNothingOperation;
import pc.strategynew.operation.KickerOperation;
import pc.strategynew.operation.MoveAndKickOperation;
import pc.strategynew.operation.MovementOperation;
import pc.strategynew.operation.Operation;
import pc.world.oldmodel.MovingObject;
import pc.world.oldmodel.WorldState;

public class MoveAndKicker extends TravelToPlan {

	private final ActionSpeed kickPower;

	String message = "";
	
	protected static int framesToHoldFor = 0;
	protected static int framesHeld = 0;


	@Override
	public PlanType getType() {
		return PlanType.MOVE_AND_ACT;
	}

	//Mirror all the TravelToPlan constructors, but add in an extra kickPower argument.
	
	public MoveAndKicker(
			float x, float y, int targetDegrees,
			MovementSpeed speed, 
			DistanceThreshold distanceThreshold, 
			AngleThreshold angleThreshold, 
			boolean backwardsIsAllowed, 
			ActionSpeed kickPower) {
		
		super(x, y, targetDegrees, speed, distanceThreshold, angleThreshold, backwardsIsAllowed);
		this.kickPower = kickPower;

	}

	public MoveAndKicker(float x, float y, MovementSpeed speed,
			DistanceThreshold distanceThreshold, AngleThreshold angleThreshold,
			boolean backwardsIsAllowed, ActionSpeed kickPower) {
		super(x, y, speed, distanceThreshold, angleThreshold, backwardsIsAllowed);
		this.kickPower = kickPower;
		framesToHoldFor = 0;
	}


	public MoveAndKicker(int x, int y, int targetDegrees, MovementSpeed speed,
			DistanceThreshold distanceThreshold, AngleThreshold angleThreshold,
			boolean hasTargetAngle, boolean backwardsIsAllowed, ActionSpeed kickPower) {
		super(x, y, targetDegrees, speed, distanceThreshold, angleThreshold,
				hasTargetAngle, backwardsIsAllowed);
		this.kickPower = kickPower;
		framesToHoldFor = 0;
	}


	public MoveAndKicker(int x, int y, MovementSpeed speed,
			DistanceThreshold distanceThreshold, AngleThreshold angleThreshold,
			boolean backwardsIsAllowed, ActionSpeed kickPower) {
		super(x, y, speed, distanceThreshold, angleThreshold, backwardsIsAllowed);
		this.kickPower = kickPower;
		framesToHoldFor = 0;
	}


	public MoveAndKicker(MovingObject obj, int targetAngleDegrees,
			MovementSpeed speed, DistanceThreshold distanceThresh,
			AngleThreshold angleThresh, boolean backwardsIsAllowed, ActionSpeed kickPower) {
		super(obj, targetAngleDegrees, speed, distanceThresh, angleThresh,
				backwardsIsAllowed);
		this.kickPower = kickPower;
		framesToHoldFor = 0;
	}


	public MoveAndKicker(MovingObject obj, MovementSpeed speed,
			DistanceThreshold distanceThresh, AngleThreshold angleThresh,
			boolean backwardsIsAllowed, ActionSpeed kickPower) {
		super(obj, speed, distanceThresh, angleThresh, backwardsIsAllowed);
		this.kickPower = kickPower;
		framesToHoldFor = 0;
	}


	public MoveAndKicker(Point destinationPoint, int targetAngle,
			MovementSpeed speed, DistanceThreshold distanceThreshold,
			AngleThreshold angleThreshold, boolean backwardsIsAllowed, ActionSpeed kickPower) {
		super(destinationPoint, targetAngle, speed, distanceThreshold, angleThreshold,
				backwardsIsAllowed);
		this.kickPower = kickPower;
		framesToHoldFor = 0;
	}


	public MoveAndKicker(Point destinationPoint, MovementSpeed speed,
			DistanceThreshold distanceThreshold, AngleThreshold angleThreshold,
			boolean backwardsIsAllowed, ActionSpeed kickPower) {
		super(destinationPoint, speed, distanceThreshold, angleThreshold,
				backwardsIsAllowed);
		this.kickPower = kickPower;
		framesToHoldFor = 0;
	}


	//Work out which operations to perform to score a goal.
	@Override
	public Operation getNextOperation(WorldState worldState, StrategyState strategyState) {
		Operation nextOperation = null;
		
		// We have the ball
		if(isAtDestination(strategyState) && strategyState.robotMovement.type == MovementType.STOPPED){
			framesHeld++;
			if(framesHeld >= framesToHoldFor){
			
			message+="KickingBall";
			
			nextOperation = new MoveAndKickOperation(
					    new MovementOperation(MovementType.FORWARDS, MovementSpeed.FULL.getPower()), 
						new KickerOperation(ActionType.KICK, kickPower),
						300);
			}else{
				nextOperation = new MovementOperation(MovementType.STOPPED, 0);
			}
			
		}else{
			framesHeld = 0;
			message+="MovingToTarget&Angle";
			nextOperation = super.getNextOperation(worldState, strategyState);
		}

		//If nothing has been decided, do nothing (to avoid NullPointerExceptions).
		return nextOperation != null ? nextOperation : new DoNothingOperation();
	}

	@Override
	public String toString() {
		return " MOVE_AND_ACT "+message+super.toString();
	}

}
