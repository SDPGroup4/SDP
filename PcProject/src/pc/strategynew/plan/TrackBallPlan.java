package pc.strategynew.plan;


import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;


/**
 * Moves robot in line with the ball's y-coordinate but on the same x-axis (origin of our zone
 * x-coordinate). Used to get robot in right position for receiving a pass, but is very simple
 * and may be too rudimentary, especially if team 14 plan to use bounce passes. we will see
 * with testing but I assume that some of the prediction system will definitely have to be 
 * considered.
 * 
 */

public class TrackBallPlan extends TravelToPlan{
	
	public TrackBallPlan(StrategyState state){
		super(PlanUtils.findBall(state), MovementSpeed.FAST, DistanceThreshold.PRECISE, AngleThreshold.ROUGH, true);
	}
	
	
	@Override
	public String toString() {
		return "TRACK THE BALL "+super.toString();
	}
	
	
}
