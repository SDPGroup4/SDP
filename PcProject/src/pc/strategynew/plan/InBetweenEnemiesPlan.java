package pc.strategynew.plan;

import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;
import pc.world.oldmodel.WorldState;


/**
 * Moves Between the Two Enemy robots (For Attacker)
 */

public class InBetweenEnemiesPlan extends TravelToPlan{
	
	
	public InBetweenEnemiesPlan(StrategyState strategyState, WorldState worldState){
		super(PlanUtils.getMidOfEnemies(strategyState, worldState),
				StrategyUtils.findClosest90Angle(strategyState.ourRobot),
				MovementSpeed.FAST,
				DistanceThreshold.INTERCEPT,
				AngleThreshold.ROUGH,
				true);
	}
	
	
	@Override
	public String toString() {
		return "MOVE_BETWEEN_ENEMIES "+super.toString();
	}
	
}
	

