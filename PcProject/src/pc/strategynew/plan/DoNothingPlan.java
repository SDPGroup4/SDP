package pc.strategynew.plan;

import pc.strategynew.StrategyState;
import pc.strategynew.operation.DoNothingOperation;
import pc.strategynew.operation.Operation;
import pc.world.oldmodel.WorldState;

/**
 * A Plan that always returns a DoNothingOperation
 * 
 * Used to simply wait and do nothing. Used to pause control
 * or to make the robot wait for something else to change
 * in the world (e.g. if the ball is removed from the pitch
 * or is in the wrong zone).
 */
public class DoNothingPlan extends Plan{
	
	@Override
	public PlanType getType() {
		return PlanType.DO_NOTHING;
	}

	@Override
	public Operation getNextOperation(WorldState worldState, StrategyState currentState) {
		return new DoNothingOperation();
	}
}
