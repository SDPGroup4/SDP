package pc.strategynew.plan;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;

import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.movement.MovementType;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;
import pc.strategynew.operation.DoNothingOperation;
import pc.strategynew.operation.MovementOperation;
import pc.strategynew.operation.Operation;
import pc.vision.PixelInfo;
import pc.vision.interfaces.ObjectRecogniser;
import pc.world.StaticWorldState;
import pc.world.oldmodel.MovingObject;
import pc.world.oldmodel.WorldState;

/**
 * A plan for travelling to a specific point.
 */
public class TravelToPlan extends Plan {

	private static TravelToPlan plan = null;
	//The destination point
	private int destX;
	private int destY;

	//What angle to face when the point is reached (if any).
	private final int targetDegrees;
	private final boolean hasRotationTarget;

	//How fast to move towards this point.
	private final MovementSpeed speed;

	//How precise to be in determining whether the robot has reached this point.
	private final DistanceThreshold distThreshold;
	private final AngleThreshold angleThresh;

	private final boolean backwardsIsAllowed;

	//What distance/angle to slow down at
//	private final int slowDownDistance = 200;
//	private final int slowDownAngle = 60;

	String message = "";

	/**
	 * Construct a plan for reaching the given point.
	 * @param x The x coordinate of the target point.
	 * @param y The y coordinate of the target point.
	 * @param targetDegrees The angle (in degrees from -180 to +180) that the robot should turn to when it reaches this point.
	 * @param speed The to travel to this point at.
	 * @param distanceThreshold How exact do we need to be when determining whether the robot is at this point.
	 * @param angleThreshold How exact do angles need to be when determining which direction the robot is facing.
	 * @param hasTargetAngle Whether the robot requires to be facing a specific angle once it reaches the point.
	 */
	public TravelToPlan(int x, int y, int targetDegrees, MovementSpeed speed, DistanceThreshold distanceThreshold, AngleThreshold angleThreshold, boolean hasTargetAngle, boolean backwardsIsAllowed) {
		this.destX = x;
		this.destY = y;
		this.targetDegrees = targetDegrees;
		this.speed = speed;
		this.distThreshold = distanceThreshold;
		this.angleThresh = angleThreshold;
		hasRotationTarget = hasTargetAngle;
		this.backwardsIsAllowed = backwardsIsAllowed;
		plan = this;
	}

	//Utility constructors with different arguments (e.g. without an angle, or taking
	//a Point rather than x/y coordinates.

	public TravelToPlan(int x, int y, MovementSpeed speed, DistanceThreshold distanceThreshold, AngleThreshold angleThreshold, boolean backwardsIsAllowed) {
		this(x, y, 0, speed, distanceThreshold, angleThreshold, false, backwardsIsAllowed);
	}

	public TravelToPlan(float x, float y, int targetDegrees, MovementSpeed speed, DistanceThreshold distanceThreshold, AngleThreshold angleThreshold, boolean backwardsIsAllowed) {
		this((int) x, (int) y, targetDegrees, speed, distanceThreshold, angleThreshold, true, backwardsIsAllowed);
	}

	public TravelToPlan(float x, float y, MovementSpeed speed, DistanceThreshold distanceThreshold, AngleThreshold angleThreshold, boolean backwardsIsAllowed) {
		this((int) x, (int) y, 0, speed, distanceThreshold, angleThreshold, false, backwardsIsAllowed);
	}

	public TravelToPlan(Point destinationPoint, MovementSpeed speed, DistanceThreshold distanceThreshold, AngleThreshold angleThreshold, boolean backwardsIsAllowed) {
		this(destinationPoint.x, destinationPoint.y, 0, speed, distanceThreshold, angleThreshold, false, backwardsIsAllowed);
	}

	public TravelToPlan(Point destinationPoint, int targetAngle, MovementSpeed speed, DistanceThreshold distanceThreshold, AngleThreshold angleThreshold, boolean backwardsIsAllowed) {
		this(destinationPoint.x, destinationPoint.y, targetAngle, speed, distanceThreshold, angleThreshold, true, backwardsIsAllowed);
	}

	public TravelToPlan(MovingObject obj, MovementSpeed speed, DistanceThreshold distanceThresh, AngleThreshold angleThresh, boolean backwardsIsAllowed) {
		this((int) obj.x, (int) obj.y, speed, distanceThresh, angleThresh, backwardsIsAllowed);
	}

	public TravelToPlan(MovingObject obj, int targetAngleDegrees, MovementSpeed speed, DistanceThreshold distanceThresh, AngleThreshold angleThresh, boolean backwardsIsAllowed) {
		this((int) obj.x, (int) obj.y, targetAngleDegrees, speed, distanceThresh, angleThresh, true, backwardsIsAllowed);
	}

	private int clamp(int min, int x, int max){
		return x < min ? min : x > max ? max : x;
	}

	private void clampDestinationWithinZone(StrategyState state){
		int edgeThreshold = 30;
		destX = clamp(state.ourRobotZone.leftEdge + edgeThreshold, destX, state.ourRobotZone.rightEdge - edgeThreshold);
		destY = clamp(state.ourRobotZone.topEdge + edgeThreshold, destY, state.ourRobotZone.bottomEdge - edgeThreshold);
	}

	@Override
	public PlanType getType() {
		return PlanType.TRAVEL_TO;
	}
	private int ourDeg = 0;

	//Work out which operations to perform to arrive at the target point.
	@Override
	public Operation getNextOperation(WorldState worldState, StrategyState strategyState) {
		state = strategyState;
		
		ourDeg = strategyState.ourRobot.orientationDegrees;
		Operation nextOperation = null;

		MovingObject robot = strategyState.ourRobot;
		ourRobotX = (int) robot.x;
		ourRobotY = (int) robot.y;

		// Clamp down the target coordinates if they are close to the pitch boundaries; 
		clampDestinationWithinZone(strategyState);

		boolean hasArrived = distThreshold.isCloseEnough(robot, destX, destY);
		boolean isFacingDesiredAngle = !hasRotationTarget || angleThresh.isCloseEnoughDegrees(robot.orientationDegrees, targetDegrees);

		message = "arrived = "+hasArrived+" facingDesiredAngle = "+isFacingDesiredAngle;

		if (strategyState.ourRobotWithinZone) {
			if(hasArrived && isFacingDesiredAngle){ 
				//Stop the robot when it has arrived and is facing the right way.
				nextOperation = new MovementOperation(MovementType.STOPPED, 0);
			}else if(hasArrived && !isFacingDesiredAngle){
				//If the robot has arrived, but is facing the wrong way, turn it around.
				nextOperation =	getRotationOperation(robot.orientationDegrees, targetDegrees, speed);
				message+="RotatingToFinalAngle";
			}else{
				//Has not arrived yet, so turn to face the target, and then move towards it.
				//Will need updated to allow it to go backwards too.

				int degreesToTarget = StrategyUtils.calculateRelativeAngleDeg(robot.x, robot.y, robot.orientationDegrees, destX, destY);
				boolean isFacingTarget = angleThresh.isCloseEnoughDegrees(degreesToTarget);


				int distanceToTarget = DistanceThreshold.getDistance((int)robot.x, (int)robot.y, destX, destY);

				message+="dist = "+distanceToTarget+" deg to target "+degreesToTarget+" isFacing "+isFacingTarget;

				if(isFacingTarget){
					nextOperation = getTravelOperation(distanceToTarget, MovementType.FORWARDS);
				}else{
					if(backwardsIsAllowed){
						int degreesToTargetBackwards = StrategyUtils.calculateRelativeAngleBackwardsDeg(robot.x, robot.y, robot.orientationDegrees, destX, destY);
						boolean isFacingTargetBackwards = angleThresh.isCloseEnoughDegrees(degreesToTargetBackwards);

						if(isFacingTargetBackwards){

							//Move very slowly if close to target.
							nextOperation = getTravelOperation(distanceToTarget, MovementType.BACKWARDS);
						}else{

							boolean isQuickerToGoBackwards = Math.abs(degreesToTargetBackwards) < Math.abs(degreesToTarget);

							if(isQuickerToGoBackwards){
								//Turn to face the target.
								nextOperation = getRotationOperation(degreesToTargetBackwards, speed);
							}else{
								//Turn to face the target.
								nextOperation = getRotationOperation(degreesToTarget, speed);
							}
						}
					}else{
						nextOperation = getRotationOperation(degreesToTarget, speed);
					}
				}
			}
		} else {
			// Our robot has left our zone; reverse or move forwards based on last observations
			// of orientation & distance from pitch dividers.
			if (strategyState.travelForwardsToReturn){
				nextOperation = new MovementOperation(MovementType.FORWARDS, speed.getPower());
			}else {
				nextOperation = new MovementOperation(MovementType.BACKWARDS, speed.getPower());
			}
		}

		//If nothing has been decided, do nothing (to avoid NullPointerExceptions).
		return nextOperation != null ? nextOperation : new DoNothingOperation();
	}


	/**
	 * Get an operation to make the robot face the target angle
	 * by turning in the best direction.
	 * 
	 * @param currentDegrees The angle the robot is currently facing (degrees from -180 to +180).
	 * @param targetDegrees  The angle the robot should be facing (degrees from -180 to +180)
	 * @param speed How fast to turn.
	 * @return The rotation operation required to reach this angle most efficiently.
	 */
	private Operation getRotationOperation(int currentDegrees, int targetDegrees, MovementSpeed speed){
		int degreesToTurn = StrategyUtils.getShortestTurnDegrees(currentDegrees, targetDegrees);
		return getRotationOperation(degreesToTurn, speed);
	}

	/**
	 * Get the type of rotation operation required to turn the given
	 * number of degrees (positive or negative from -180 to +180).
	 * Negative angles turn left, positive angles turn right.
	 * 
	 * @param degreesToTurn The number of degrees to turn (positive or negative from -180 to +180).
	 * @param speed How fast to turn.
	 * @return An operation determining whether to turn left or right.
	 */
	private Operation getRotationOperation(int degreesToTurn, MovementSpeed speed){
		MovementType movType = degreesToTurn < 0 ? MovementType.RIGHT : MovementType.LEFT;
		float w = getWeight(35, 180, Math.abs(degreesToTurn));
		int power = lerp(40, 60, w);
		return new MovementOperation(movType, power);
	}

	private Operation getTravelOperation(int distanceToMove, MovementType movType){
		float w = getWeight(60, 180, Math.abs(distanceToMove));
		int power = lerp(40, 100, w);
		return new MovementOperation(movType, power);
	}
	
	private static float getWeight(int start, int end, int value){
		float range = end - start;
		float v = value - start;
		float w = v / range;
		
		return Math.max(0, Math.min(1, w));
	}
	
	private static int lerp(int start, int end, float weight){
		float range = end - start;
		float v = range*weight;
		v += start;
		return (int) Math.max(start, Math.min(end, v));
	}

	@Override
	public String toString() {
		return super.toString()+" "+destX+" "+destY+" targetDeg = "+targetDegrees+" currentDeg = "+ourDeg+" "+speed+" "+distThreshold+" "+angleThresh+message;
	}

	protected boolean isAtDestination(StrategyState state){
		clampDestinationWithinZone(state);
		boolean hasArrived = distThreshold.isCloseEnough(state.ourRobot, destX, destY);
		boolean isFacingDesiredAngle = !hasRotationTarget || angleThresh.isCloseEnoughDegrees(state.ourRobot.orientationDegrees, targetDegrees);
		return hasArrived && isFacingDesiredAngle;
	}












	private static int ourRobotX = 200;
	private static int ourRobotY = 200;
	private static StrategyState state = null;
	public static class TravelVisualizer implements ObjectRecogniser{
		@Override
		public void processFrame(PixelInfo[][] pixels, BufferedImage frame, Graphics2D debugGraphics, BufferedImage debugOverlay, StaticWorldState result) {
			if(plan != null){
				if(plan.hasRotationTarget){
					int r = 400;
					int angle = plan.targetDegrees;
					AngleThreshold angleThresh = plan.angleThresh;
					drawLine(plan.destX, plan.destY, angle, r, debugGraphics);
					drawLine(plan.destX, plan.destY, angle+angleThresh.getDegreeThreshold(), r, debugGraphics);
					drawLine(plan.destX, plan.destY, angle-angleThresh.getDegreeThreshold(), r, debugGraphics);
				}
				debugGraphics.setColor(Color.RED);
				drawCircle(plan.destX, plan.destY, 5, debugGraphics);
				drawCircle(plan.destX, plan.destY, plan.distThreshold.getThreshold(), debugGraphics);
				debugGraphics.setColor(Color.RED);
				drawCircle(ourRobotX, ourRobotY, 5, debugGraphics);
				
				if(state != null){
					if(StrategyUtils.hasShotOnGoal(state)){
						debugGraphics.setColor(Color.GREEN);
					}else{
						debugGraphics.setColor(Color.RED);
					}
					drawLine(ourRobotX, ourRobotY, state.ourRobot.orientationDegrees, 100, debugGraphics);
					drawCircle((int)state.opponentDefenderRobot.x, (int)state.opponentDefenderRobot.y, StrategyUtils.DEFENDER_RADIUS, debugGraphics);
				}
				debugGraphics.setColor(Color.WHITE);
				if(ScoreGoalPlan.hasTimedOut){
					debugGraphics.drawString("TIMED OUT!", ourRobotX, ourRobotY);
				}else{
					debugGraphics.drawString("...", ourRobotX, ourRobotY);
				}
			}
		}
	}

	private static void drawCircle(int centreX, int centreY, int radius, Graphics2D debugGraphics){
		float halfRad = radius / 2;
		debugGraphics.drawOval((int) (centreX - halfRad), (int) (centreY - halfRad), radius, radius);
	}

	private static void drawLine(int startX, int startY, float angleDeg, int r, Graphics2D debugGraphics){
		int endX = startX + (int) (r * Math.cos(Math.toRadians(angleDeg)));
		int endY = startY + (int) (r * Math.sin(Math.toRadians(angleDeg)));
		debugGraphics.drawLine(startX,startY,  endX, endY);
	}
}
