package pc.strategynew.plan;

import pc.strategynew.StrategyState;
import pc.strategynew.operation.Operation;
import pc.world.oldmodel.WorldState;

/**
 * The second level in the decision making hierarchy, and probably the one
 * that does the most work. These represent reasonably complex
 * re-usable chunks of behaviour that strategies can use e.g. CatchBall,
 * ScoreGoal, ReturnToOrigin. Plans come up with an Operation that
 * must be performed next in order to achieve its goal.
 * 
 * Filling in all these classes is where the bulk of the strategy
 * work will take place. It may help to adapt some of the elements from
 * pc.strategy.GeneralStrategy for examples on how these behaviours could be
 * carried out. Any intermediary variables required will be stored in
 * a StrategyState passed in every time a plan is created.
 * 
 * The robot's decision making hierarchy is:
 * 
 * Strategy    (e.g. Attacking, Marking, Passing)
 * Plan		   (e.g. CatchBall, ReturnToOrigin, ScoreGoal)
 * Operation   (e.g. Forwards, TurnLeft, Stop, ConfuseKickLeft)
 * Command	   (the Arduino must be programmed for these e.g. KICK, FORWARDS, STOP).
 */
public abstract class Plan {
	//All types of Plan that exist.
	//Add more to extend the robots capabilities.
	public enum PlanType{
		DO_NOTHING("DO_NOTHING_PLAN"),
		TRAVEL_TO("TRAVEL_TO_PLAN"),
		SCORE_GOAL("SCORE_GOAL"),
		CATCH_BALL("CATCH_BALL"),
		MOVE_AND_ACT("MOVE_AND_ACT");
		/* Potential other ones
		SCORE_GOAL("SCORE_GOAL"),
		PASS_BALL("PASS_BALL")
		*/
		;
		
		private final String name;
		PlanType(String name){
			this.name = name;
		}
		@Override
		public String toString() {
			return name;
		}
	}
	
	
	/**
	 * Get the corresponding type from the above enum that
	 * describes what this plan attempts to accomplish.
	 */
	public abstract PlanType getType();
	
	/**
	 * Get the next Operation that should be executed to advance this
	 * plan's progression.
	 * 
	 * @param worldState The current state of the world as seen by the vision system.
	 * @param currentState Can be updated or read from to pass data to other levels of the hierarchy.
	 * @return The next Operation to perform to advance this strategy.
	 */
	public abstract Operation getNextOperation(WorldState worldState, StrategyState currentState);

	@Override
	public String toString() {
		return getType().toString();
	}
}
