package pc.strategynew.plan;

import pc.movement.ActionSpeed;
import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;


/**
 * 
 * The Plan to kick ,assume our robot are at right position
 *
 * Idea: kick at certain power so that it could move to the ally's pitch zone
 *       facing will adjust to ally robot
 */

public class KickToAllyPlan extends MoveAndKicker {
	public KickToAllyPlan(StrategyState strategyState, ActionSpeed kickPower) {
		//super((int)strategyState.ourRobot.x, (int)strategyState.ourRobot.y, 0, DistanceThreshold.PRECISE, AngleThreshold.PRECISE, false, kickPower);
		super(strategyState.ourRobot, 
		      StrategyUtils.calculateAbsoluteAngleDeg(strategyState.ourRobot, strategyState.allyRobot),
		      MovementSpeed.SLOW,
		      DistanceThreshold.ROUGH,
		      AngleThreshold.PRECISE, 
		      false, kickPower);
	}

	@Override
	public String toString() {
		return "KICK_TO_ALLY_ROBOT "+super.toString();
	}
	
	
}
