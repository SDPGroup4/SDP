package pc.strategynew.plan;

import java.awt.Point;

import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;

public class AdjustRobotOrientation extends TravelToPlan {

	/**
	 * Constructor to create a operation to turn robot to a particular angle
	 * @param destinationPoint The point where the robot should be on the pitch for the turn.
	 * @param targetAngle The angle to which the robot should turn to.
	 */
	public AdjustRobotOrientation(Point destinationPoint, int targetAngle) {
		super(destinationPoint, targetAngle, MovementSpeed.MEDIUM, DistanceThreshold.PRECISE, AngleThreshold.PRECISE, true);
		
	}
}
