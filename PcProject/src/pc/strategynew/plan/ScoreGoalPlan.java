package pc.strategynew.plan;

import pc.movement.ActionSpeed;
import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;
import pc.strategynew.operation.Operation;
import pc.world.oldmodel.WorldState;


public class ScoreGoalPlan extends MoveAndKicker {
	
	public static int GOAL_TIMEOUT = 9000;

	public ScoreGoalPlan(StrategyState strategyState, WorldState worldState) {
		super(strategyState.ourRobot,
			 angleToShootAt(strategyState, worldState), 
			 MovementSpeed.SLOW,
			 DistanceThreshold.ROUGH, 
			 AngleThreshold.GOAL,
			 true, ActionSpeed.FULL);
		framesToHoldFor = 3;
	}
	
	@Override
	public Operation getNextOperation(WorldState worldState, StrategyState strategyState) {
		if(framesHeld > 0){
			turnLeft = !turnLeft;
		}
		return super.getNextOperation(worldState, strategyState);
	}
	
	public static boolean hasTimedOut = false;
	private static boolean turnLeft;
	private static int angleToShootAt(StrategyState strategyState, WorldState worldState){
		
		int robotAngle = strategyState.ourRobot.orientationDegrees;

		boolean hasTimedOut = (System.currentTimeMillis() - StrategyState.scoreGoalTimer > GOAL_TIMEOUT);
		
		if(StrategyUtils.hasShotOnGoal(strategyState)){
			ScoreGoalPlan.hasTimedOut = false;
			return robotAngle;
		}else if((hasTimedOut && StrategyUtils.facingGoal(worldState, strategyState))){
			ScoreGoalPlan.hasTimedOut = true;

			return robotAngle;
		}else if(worldState.weAreShootingRight){
			if(robotAngle < -90 || robotAngle > 90){
				return 0;
			}else if(robotAngle > 45){
				turnLeft = true;
			}else if(robotAngle < -45){
				turnLeft = false;
			}
		}else{
			if(robotAngle > -90 && robotAngle < 90){
				return 180;
			}else if(robotAngle > 135){
				turnLeft = false;
			}else if(robotAngle < -135){
				turnLeft = true;
			}
		}
		ScoreGoalPlan.hasTimedOut = false;
		int angleToChange = AngleThreshold.ROUGH.getDegreeThreshold() + 2;
		return turnLeft ? robotAngle - angleToChange : robotAngle + angleToChange;
	}
	
	@Override
	public PlanType getType() {
		return PlanType.SCORE_GOAL;
	}
	
	@Override
	public String toString() {
		return "SCORE_GOAL "+super.toString();
	}
}
