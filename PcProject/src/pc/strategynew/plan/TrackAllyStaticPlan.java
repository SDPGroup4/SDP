package pc.strategynew.plan;


import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;


public class TrackAllyStaticPlan extends TravelToPlan {
	public TrackAllyStaticPlan(StrategyState strategyState){
		super(strategyState.ourRobot, 
			  StrategyUtils.calculateAbsoluteAngleDeg(strategyState.ourRobot, strategyState.allyRobot), 
			  MovementSpeed.SLOW,
			  DistanceThreshold.ROUGH, 
			  AngleThreshold.ROUGH,
			  true);
		
		//Fraser's version (tracks motion of ally robot)
		//super(strategyState.ourRobotZone.getOriginPoint().x,  strategyState.allyRobot.y,  ((strategyState.allyRobot.orientationDegrees + 180) % 360), MovementSpeed.SLOW, DistanceThreshold.ROUGH, AngleThreshold.ROUGH, true, true);
		//System.out.println("DesiredAngle" + ((strategyState.allyRobot.orientationDegrees + 180) % 360));
	}

	/*
	@Override
	public Operation getNextOperation(WorldState worldState, StrategyState strategyState) {
		if(isAtDestination(strategyState) && strategyState.robotMovement.type == MovementType.STOPPED){
			Operation nextOperation = new super(getRotationOperation(strategyState.ourRobot.orientationDegrees,  StrategyUtils.calculateAngleDeg(strategyState.ourRobot, strategyState.allyRobot), MovementSpeed.MEDIUM));
		}
		return super.getNextOperation(worldState, strategyState);
	}
	*/
	
	@Override
	public String toString() {
		return "TRACKING_ALLY_ROBOT_STATIONARY "+super.toString();
	}
}

