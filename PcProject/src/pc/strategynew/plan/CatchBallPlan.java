package pc.strategynew.plan;

import pc.movement.ActionType;
import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.movement.MovementType;
import pc.strategynew.StrategyState;
import pc.strategynew.StrategyUtils;
import pc.strategynew.operation.DoNothingOperation;
import pc.strategynew.operation.KickerOperation;
import pc.strategynew.operation.MoveAndKickOperation;
import pc.strategynew.operation.MovementOperation;
import pc.strategynew.operation.Operation;
import pc.world.oldmodel.WorldState;

public class CatchBallPlan extends TravelToPlan{
	
	String message = "";

	public CatchBallPlan(StrategyState state){
		super(state.ball, 
			  state.angleToBall, 
			  MovementSpeed.SLOW, 
			  DistanceThreshold.ROUGH, 
			  AngleThreshold.ROUGH, 
			  StrategyUtils.ballWasRecentlyInEnemyDefenderZone(state));
		StrategyState.scoreGoalTimer = System.currentTimeMillis();
	}
	
	@Override
	public PlanType getType() {
		return PlanType.CATCH_BALL;
	}
	
	//Work out which operations to perform to score a goal.
		@Override
		public Operation getNextOperation(WorldState worldState, StrategyState strategyState) {
			Operation nextOperation = null;
			if (!strategyState.hasCaughtBall) {
                message+=" (not caught ball) ";
				if (strategyState.catcherReady) {
					 message+=" (catcher ready) ";
					if(isAtDestination(strategyState) 
							//&& strategyState.robotMovement.type == MovementType.STOPPED
							){
						
						StrategyState.scoreGoalTimer = System.currentTimeMillis();
						nextOperation =  new MoveAndKickOperation(
								new MovementOperation(MovementType.FORWARDS, MovementSpeed.SLOW.getPower()),
								new KickerOperation(ActionType.CATCH),
								350);
						
						//nextOperation = new KickerOperation(ActionType.CATCH);
					}
					else if (!strategyState.ballOnPitch && strategyState.robotMovement.type == MovementType.STOPPED){
						strategyState.scoreGoalTimer = System.currentTimeMillis();
						nextOperation = new KickerOperation(ActionType.CATCH);
					}
					else{
						nextOperation = super.getNextOperation(worldState, strategyState);
					}
				} else {
					// Catcher not ready
					nextOperation = new KickerOperation(ActionType.RESET_CATCHER);
				}
			}else{
				nextOperation = new MovementOperation(MovementType.STOPPED, 0);
			}
			//If nothing has been decided, do nothing (to avoid NullPointerExceptions).
			return nextOperation != null ? nextOperation : new DoNothingOperation();
		}

		@Override
		public String toString() {
			return " CATCH_BALL "+message+super.toString();
		}
}
