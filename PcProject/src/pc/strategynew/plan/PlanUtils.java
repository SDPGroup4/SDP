package pc.strategynew.plan;

import java.awt.Point;

import pc.movement.DistanceThreshold;
import pc.strategynew.StrategyState;
import pc.vision.PitchConstants;
import pc.world.PitchZone;
import pc.world.oldmodel.WorldState;


/**
 * A big bag of useful functions required at various levels of the strategy
 * system. Most common types of calculation should be written once here
 * rather than scattered throughout the code. This should hopefully make the code
 * a bit more readable e.g. instead of:
 *  
 * Any function that is used repeatedly or does not fit in well with the class 
 * it is used in should be moved here (or a similar utility class) to keep the
 * decision making code readable and provide only a single debuggable place for
 * the calculations.
 */

public class PlanUtils {
	
	public static boolean isInSpace(WorldState worldState, StrategyState strategyState){
		Point space = FindSpace(worldState, strategyState.ourRobotZone);
		boolean isInSpace = DistanceThreshold.ROUGH.isCloseEnough(strategyState.ourRobot, space);
		return isInSpace;
	}

	//Returns the coordinate of the centre of the appropriate area of our zone (where there
	//is a clear line of sight)
	public static Point FindSpace(WorldState worldState, PitchZone zone){
		//Define the coordinates used for calculation
		float enemyAttX = worldState.getEnemyAttackerRobot().x;
		float enemyAttY = worldState.getEnemyAttackerRobot().y;
		float teamDefX = worldState.getDefenderRobot().x;
		float teamDefY = worldState.getDefenderRobot().y;
		int originX = zone.getOriginPoint().x;
		
		//System.out.println("enemyAttX" + enemyAttX);
		//System.out.println("enemyAttY" + enemyAttY);
		
		
		
		//Find equation of line between defender and enemy attacker and find point (blockedPointY)
		//on line that corresponds to the area in our zone that is blocked by enemy attacker
		float gradient = (enemyAttY - teamDefY) /  (enemyAttX - teamDefX);
		int blockedPointY = (int)(gradient*(originX - enemyAttX) + enemyAttY);
		
		
		int[] lines = getLines(worldState);				//Splits our zone by finding are boundaries
		
		//System.out.println("line0" + lines[0]);
		//System.out.println("line1" + lines[1]);
		//System.out.println("line2" + lines[2]);
		
		int blocked = noGoArea(lines, blockedPointY);   //Returns area blocked by enemy (i.e. DONT GO THERE)
		int notBlocked = goArea(lines, blocked, (int)teamDefY); //Select area to move into (not Blocked)
		//System.out.println("Blocked" + blocked);
		//System.out.println("NotBlocked" + notBlocked);
		Point space = null;
		//Selects the centre of the area that we want to move into (does't move to centre at the moment)
		switch(notBlocked){
			case 0:
				space = new Point( (int)originX, (int)(lines[0]-(lines[2]/4)) );
				break;
			case 1:
				space = zone.getOriginPoint();
				break;
			case 2:
				space = new Point( (int)originX, (int)(lines[1]+(lines[2]/4)) );
				break;
		}
		
		return space;
	
	}
	
	
	//Method finds the area that is blocked
	
	public static int noGoArea(int[] lines, int blockedPointY){
		
		
		if (blockedPointY < lines[0]){
			return 0;					//Top third of our zone 
		}else if(blockedPointY > lines[0] && blockedPointY < lines[1]){
			return 1;					//Middle third of our zone 
		}else if(blockedPointY > lines[1]){
			return 2;					//Bottom third of our zone 
		}else{
			return 1;					//If in doubt don't go to middle
		}	
		
	}
	
	
	//Method to choose which area to go into
	public static int goArea(int [] lines , int blocked, int teamDefY){
		
		//If top third is blocked go to bottom third
		if (blocked == 0){
			return 2;
		}
		//If bottom third is blocked go to top third
		if (blocked == 2){
			return 0;
		}
		
		//If middle is blocked, go to area that is closest to team defender
		int teamDefZoneMid = (lines[0] + lines[1]) / 2;
		if(teamDefY > teamDefZoneMid){
			return 2;
		}
		return 0;
		
		
	}
	
	
	//Splitting our zone into 3 areas (top, middle, bottom) by finding the separating boundary
	//between areas
	public static int[] getLines(WorldState worldState){
		int topEdge = PitchConstants.getPitchOutlineTop();	
		int bottomEdge = PitchConstants.getPitchOutlineBottom();
		
		//System.out.println("Top " + topEdge);
		//System.out.println("Bottom " + bottomEdge);
	
		int length = bottomEdge - topEdge;  //how long is area
		int line0 = topEdge + (length / 3);
		int line1 = topEdge + (2 * length/3);
		int[] lines = {line0, line1, length/3};		//we also include the height of 1 area for future (length/3)
		
		return lines;
	}
	
	public static Point findBall(StrategyState state){
		
		int targetX = state.ourRobotZone.getOriginPoint().x;		
		int targetY = (int)state.ball.y;
		
		return new Point(targetX, targetY);
		
	}
	
	
	//Returns a point on the line between the enemies such that the Attacker suitable sits
	//in it's own zone
	public static Point getMidOfEnemies(StrategyState strategyState, WorldState worldState){
		//Define the coordinates used for calculation
		float enemyAttX = worldState.getEnemyAttackerRobot().x;
		float enemyAttY = worldState.getEnemyAttackerRobot().y;
		float enemyDefX = worldState.getEnemyDefenderRobot().x;
		float enemyDefY = worldState.getEnemyDefenderRobot().y;
		float originX = strategyState.ourRobotZone.getOriginPoint().x;
		
		if (!strategyState.opponentAttackerRobotZone.isInZone(strategyState.opponentAttackerRobot)){
			enemyAttX = strategyState.opponentAttackerRobotZone.getOriginPoint().x;
			enemyAttY = strategyState.opponentAttackerRobotZone.getOriginPoint().y;
		}
		
		if (!strategyState.opponentDefenderRobotZone.isInZone(strategyState.opponentDefenderRobot)){
			enemyDefX = strategyState.opponentDefenderRobotZone.getOriginPoint().x;
			enemyDefY = strategyState.opponentDefenderRobotZone.getOriginPoint().y;
		}
		
		
		//Find equation of line between enemies and find point on line that lies directly
		//between zone parameters
		float gradient = (enemyAttY - enemyDefY) /  (enemyAttX - enemyDefX);
		int targetY = (int)(gradient*(originX - enemyAttX) + enemyAttY);
		int targetX = (int)originX;
		
		Point mid = new Point(targetX, targetY);
		return mid;
	
	}
	

}
