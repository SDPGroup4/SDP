package pc.strategynew;

import pc.movement.DistanceThreshold;
import pc.vision.PitchConstants;
import pc.world.PitchZone;
import pc.world.PitchZone.PitchZoneType;
import pc.world.oldmodel.MovingObject;
import pc.world.oldmodel.WorldState;

/**
 * A big bag of useful functions required at various levels of the strategy
 * system. Most common types of calculation should be written once here
 * rather than scattered throughout the code. This should hopefully make the code
 * a bit more readable e.g. instead of:
 * 
 * Math.abs(ballY - PitchConstants.getPitchOutlineTop()) < 20 || Math.abs(ballY - PitchConstants.getPitchOutlineBottom()) < 20 )
 * 
 * we can say
 * 
 * StrategyUtils.nearSidesOfPitch(worldState.getBall());
 * 
 * which explains what the intention of the code is without having to work out
 * what the maths is doing.
 * 
 * 
 * Any function that is used repeatedly or does not fit in well with the class 
 * it is used in should be moved here (or a similar utility class) to keep the
 * decision making code readable and provide only a single debuggable place for
 * the calculations.
 */
public final class StrategyUtils {
	private StrategyUtils(){} //Stop new StrategyUtils() from working elsewhere.
	
	public static final int DEFENDER_RADIUS = 40;
	
	/**
	 * Check whether the given object is near the sides of the pitch.
	 * @param object The object whose position needs checked.
	 * @return True if the object's centre within 20 pixels of the top or bottom of the pitch.
	 */
	public static boolean nearSidesOfPitch(MovingObject object){
		if(object != null){
			return nearSidesOfPitch(object.y);
		}
		return false;
	}

	/**
	 * Check whether the given coordinate is near the sides of the pitch.
	 * @param y The y coordinate to check.
	 * @return True if the y value is within 20 pixels of the top or bottom of the pitch.
	 */
	public static boolean nearSidesOfPitch(float y){
		return Math.abs(y - PitchConstants.getPitchOutlineTop()) < 20 
				|| Math.abs(y - PitchConstants.getPitchOutlineBottom()) < 20;
	}


	/**
	 * Determine which zone on the pitch the object is in.
	 * Handles checks for shooting direction as well.
	 * @param object The object to check.
	 * @param worldState The current world the object inhabits. Allows for dynamic resizing of pitch.
	 * @return The type of zone the object is now in.
	 */
	public static PitchZoneType getPitchZone(MovingObject object, WorldState worldState){
		if(object != null){
			return getPitchZone(object.x, worldState);
		}
		return PitchZoneType.DEFENDER; //Default value.
	}

	/**
	 * Determine which zone the given x coordinate is in.
	 * Handles checks for shooting direction, and resizing
	 * of zone boundaries too.
	 * 
	 * @param x The x coordinate to check.
	 * @param worldState The world. Allows for dynamic resizing of pitch.
	 * @return The type of zone the coordinate is in.
	 */
	public static PitchZoneType getPitchZone(float x, WorldState worldState){

		//This is adapted from the old strategy code, so may be a bit of a mess.
		//It has been hidden here to make the rest of the code less messy, but
		//might still be rewritten for clarity later.

		int defenderCheck = (worldState.weAreShootingRight) ? worldState.dividers[0] : worldState.dividers[2];
		int leftCheck =  (worldState.weAreShootingRight) ? worldState.dividers[1] : worldState.dividers[0];
		int rightCheck = (worldState.weAreShootingRight) ? worldState.dividers[2] : worldState.dividers[1];

		if (    (worldState.weAreShootingRight && x < defenderCheck)
				|| (!worldState.weAreShootingRight && x > defenderCheck)) {

			return PitchZoneType.DEFENDER;

		} else if (x > leftCheck && x < rightCheck) {

			return PitchZoneType.ATTACKER;

		} else if ( worldState.weAreShootingRight && x > defenderCheck && x < leftCheck 
				|| !worldState.weAreShootingRight && x < defenderCheck && x > rightCheck) {

			return PitchZoneType.ENEMY_ATTACKER;

		} else if (!worldState.weAreShootingRight && (x < leftCheck)
				|| worldState.weAreShootingRight && (x > rightCheck)) {

			return PitchZoneType.ENEMY_DEFENDER;

		}else{
			return PitchZoneType.DEFENDER;
		}
	}

	public static boolean facingGoal(WorldState worldState, StrategyState strategyState){
		
		if (worldState.weAreShootingRight){
			return (strategyState.ourRobot.orientationDegrees < 45)&&(strategyState.ourRobot.orientationDegrees > -45);
		}
		else{
			return (strategyState.ourRobot.orientationDegrees >135)||(strategyState.ourRobot.orientationDegrees < -135);
			
		}

	}


	/**
	 * Get the robot from the given zone.
	 * @param robotZoneType The pitch zone the required robot inhabits.
	 * @param worldState The current state of the world.
	 * @return The MovingObject representing the robot for that zone.
	 */
	public static MovingObject getRobot(PitchZoneType robotZoneType, WorldState worldState) {
		switch(robotZoneType){
		case ATTACKER: 			return worldState.getAttackerRobot();
		case DEFENDER: 			return worldState.getDefenderRobot();
		case ENEMY_ATTACKER:	return worldState.getEnemyAttackerRobot();
		case ENEMY_DEFENDER:	return worldState.getEnemyDefenderRobot();
		}
		return null;
	}

	/**
	 * Get the ball object
	 * @param worldState Current state of the world.
	 * @return MovingObject that represents the ball.
	 */
	public static MovingObject getBall(WorldState worldState) {
		return worldState.getBall();
	}

	/**
	 * Calculate the angle (in degrees) between two points.
	 * @param robotX The robot's current x coordinate.
	 * @param robotY The robot's current y coordinate.
	 * @param robotDegrees The robot's current orientation (in degrees from -180 to +180)
	 * @param targetX The x coordinate of the target point.
	 * @param targetY The y coordinate of the target point.
	 * @return The angle (in degrees between -180 and +180) that the target point is away from the robot's current orientation. 
	 */
	public static int calculateRelativeAngleDeg(float robotX, float robotY, float robotDegrees, float targetX, float targetY) {
		double robotRadians = Math.toRadians(robotDegrees);
		double angleRadians = calculateRelativeAngleRad(robotX, robotY, robotRadians, targetX, targetY);
		return (int) Math.toDegrees(angleRadians);
	}

	public static int calculateAbsoluteAngleDeg(float robotX, float robotY, float targetX, float targetY) {
		double angleRadians = calculateAbsoluteAngleRad(robotX, robotY, targetX, targetY);
		return (int) Math.toDegrees(angleRadians);
	}

	public static int calculateRelativeAngleDeg(MovingObject ourRobot, MovingObject otherObj) {
		return calculateRelativeAngleDeg(ourRobot.x, ourRobot.y, ourRobot.orientationDegrees, otherObj.x, otherObj.y);
	}

	public static int calculateAbsoluteAngleDeg(MovingObject ourRobot, MovingObject otherObj) {
		return calculateAbsoluteAngleDeg(ourRobot.x, ourRobot.y, otherObj.x, otherObj.y);
	}

	public static int calculateRelativeAngleBackwardsDeg(float robotX, float robotY, float robotDegrees, float targetX, float targetY) {
		double robotRadians = Math.toRadians(robotDegrees-180);
		double angleRadians = calculateRelativeAngleRad(robotX, robotY, robotRadians, targetX, targetY);
		return (int) Math.toDegrees(angleRadians);
	}
	
	public static int findClosest90Angle(MovingObject ourRobot){
		if (ourRobot.orientationDegrees > 0) return 90;
		else return -90;
		
	}

	/**
	 * Calculate the angle (in radians) between two points.
	 * @param robotX The robot's current x coordinate.
	 * @param robotY The robot's current y coordinate.
	 * @param robotRadians The robot's current orientation (in radians from -PI to +PI)
	 * @param targetX The x coordinate of the target point.
	 * @param targetY The y coordinate of the target point.
	 * @return The angle (in radians between -PI and +PI) that the target point is away from the robot's current orientation.
	 */
	public static double calculateRelativeAngleRad(float robotX, float robotY, double robotRadians, float targetX, float targetY) {
		double targetRadians = Math.atan2(targetY - robotY, targetX - robotX);
		return getShortestTurnRadians(robotRadians, targetRadians);
	}

	public static double calculateAbsoluteAngleRad(float robotX, float robotY, float targetX, float targetY) {
		double targetRadians = Math.atan2(targetY-robotY, targetX-robotX);
		return clampToRadianRange(targetRadians);
	}

	/**
	 * Get the number of degrees to turn to reach the target angle through
	 * the shortest turn (i.e. never turning more than 180 degrees left or right).
	 * @param currentDegrees The current orientation of the robot (in degrees from -180 to +180).
	 * @param targetDegrees The desired new orientation of the robot  (in degrees from -180 to +180).
	 * @return The shorted angle (in degrees from -180 to +180) to turn to arrive at the target angle.
	 */
	public static int getShortestTurnDegrees(int currentDegrees, int targetDegrees){
		double shortestRadians = getShortestTurnRadians(Math.toRadians(currentDegrees), Math.toRadians(targetDegrees));
		return (int) Math.toDegrees(shortestRadians);
	}

	/**
	 * Get the number of radians to turn to reach the target angle through
	 * the shortest turn (i.e. never turning more than PI radians left or right).
	 * @param currentRadians The current orientation of the robot (in radians from -PI to +PI)
	 * @param targetRadians The desired new orientation of the robot (in radians from -PI to +PI).
	 * @return The shorted angle (in radians from -PI to +PI) to turn to arrive at the target angle.
	 */
	public static double getShortestTurnRadians(double currentRadians, double targetRadians){
		currentRadians = clampToRadianRange(currentRadians);
		targetRadians  = clampToRadianRange(targetRadians);
		return clampToRadianRange(currentRadians - targetRadians);
	}

	public static int clampToDegreeRange(int degrees){
		return (int) Math.toDegrees(clampToRadianRange(Math.toRadians(degrees)));
	}

	public static double clampToRadianRange(double radians){
		while (radians > Math.PI){
			radians -= 2 * Math.PI;
		}
		while (radians < -Math.PI){
			radians += 2 * Math.PI;
		}
		return radians;
	}

	/**
	 * Check if ball is in our robot's zone.
	 * @param worldState The worldState of our model.
	 * @return True if ball is outside our zone, else false.
	 */
	public static boolean isballOutsideOurZone(WorldState worldState) {
		PitchZoneType ballZone = getPitchZone(worldState.getBall().x, worldState);
		return 	(ballZone == PitchZoneType.DEFENDER
				|| ballZone == PitchZoneType.ENEMY_ATTACKER
				|| ballZone == PitchZoneType.ENEMY_DEFENDER);
	}

	public static PitchZoneType getAllyFor(PitchZoneType zone){
		switch(zone){
		case ATTACKER: return PitchZoneType.DEFENDER;
		case DEFENDER: return PitchZoneType.ATTACKER;
		case ENEMY_ATTACKER: return PitchZoneType.ENEMY_DEFENDER;
		case ENEMY_DEFENDER: return PitchZoneType.ENEMY_ATTACKER;
		}
		return PitchZoneType.DEFENDER;
	}

	/**
	 * Get the y-coordinates of pitch dividers for our zone.
	 * @param worldState the current state of the world.
	 * @return Array of the left and right pitch dividers of our robot's zone.
	 */
	public static int[] getOurZoneDividers(WorldState worldState){
		int zoneDividers[] = new int[2];

		zoneDividers[0] = (worldState.weAreShootingRight) ? worldState.dividers[1] : worldState.dividers[0];
		zoneDividers[1] = (worldState.weAreShootingRight) ? worldState.dividers[2] : worldState.dividers[1];

		return zoneDividers;
	}

	public static boolean hasEnoughOfARunUp(StrategyState strategyState, PitchZone zoneToAimAt){
		return DistanceThreshold.ROUGH.isCloseEnough(
				strategyState.ourRobot,
				strategyState.ourRobotZone.getRunUpXCoordinateForKickingTowards(zoneToAimAt),
				(int) strategyState.ourRobot.y); 
	}

	/**
	 * Check if ball is on the pitch.
	 * @param worldState The WorldState modelling the current object variables .
	 * @return True if the ball is on the pitch, else false.
	 */
	public static boolean isballOnPitch(WorldState worldState) {
		if(worldState.ballNotOnPitch) {
			return false;
		}
		return true;
	}

	/**
	 * Compares our robot's x and y values to the ball's to discern if the ball is close by
	 * @param ourRobot MovingObject for ourRobot
	 * @param ball MovingObject for the ball
	 * @return True if the ball is within 35mm of the robot else it returns false
	 */
	public static boolean isBallNearRobot(MovingObject ourRobot, MovingObject ball) {
		float X = (float) Math.pow((ourRobot.x - ball.x), 2);
		float Y = (float) Math.pow((ourRobot.y - ball.y), 2);
		float dist = (float) Math.sqrt(X + Y);
		// Distance in millimetres or pixels?
		if (dist < 35) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean hasShotOnGoal(StrategyState state){
		return hasShotOnGoal(state.ourRobot, state.opponentDefenderRobot, state.enemyGoal, state.goalX);
	}

	public static boolean hasShotOnGoal(MovingObject attacker, MovingObject defender, float[] goalYs, float goalX){
		float bottom = goalYs[0];
		float top = goalYs[1];
		
		int r = 400;
		int angleDeg = attacker.orientationDegrees;
		int endX = (int) (attacker.x + (r * Math.cos(Math.toRadians(angleDeg))));
		int endY = (int) (attacker.y + (r * Math.sin(Math.toRadians(angleDeg))));
		
		boolean isFacingGoal =  doLinesIntersect(goalX, top, goalX, bottom, attacker.x, attacker.y, endX, endY);
		boolean isBlocked = shotIsBlockedByDefender(attacker.x, attacker.y, endX, endY, defender.x, defender.y, DEFENDER_RADIUS);
		
		return isFacingGoal && !isBlocked;
	}
	
	public static boolean shotIsBlockedByDefender(float startX, float startY, float endX, float endY, float defenderX, float defenderY, float radius){
	        double baX = endX - startX;
	        double baY = endY - startY;
	        double caX = defenderX - startX;
	        double caY = defenderY - startY;

	        double a = baX * baX + baY * baY;
	        double bBy2 = baX * caX + baY * caY;
	        double c = caX * caX + caY * caY - radius * radius;

	        double pBy2 = bBy2 / a;
	        double q = c / a;

	        double disc = pBy2 * pBy2 - q;
	        if (disc < 0) {
	            return false;
	        }
	        return true;
//	        // if disc == 0 ... dealt with later
//	        double tmpSqrt = Math.sqrt(disc);
//	        double abScalingFactor1 = -pBy2 + tmpSqrt;
//	        double abScalingFactor2 = -pBy2 - tmpSqrt;
//
//	        Point p1 = new Point(pointA.x - baX * abScalingFactor1, pointA.y
//	                - baY * abScalingFactor1);
//	        if (disc == 0) { // abScalingFactor1 == abScalingFactor2
//	            return Collections.singletonList(p1);
//	        }
//	        Point p2 = new Point(pointA.x - baX * abScalingFactor2, pointA.y
//	                - baY * abScalingFactor2);
//	        return Arrays.asList(p1, p2);
//	    }
	}
	
	// Returns true if the lines intersect, otherwise false.
	public static boolean doLinesIntersect(float p0_x, float p0_y, float p1_x, float p1_y, float p2_x, float p2_y, float p3_x, float p3_y){
	    float s1_x, s1_y, s2_x, s2_y;
	    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
	    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;

	    float s, t;
	    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
	    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);

	  return s >= 0 && s <= 1 && t >= 0 && t <= 1;
	}
	
//	// Returns 1 if the lines intersect, otherwise 0. In addition, if the lines 
//	// intersect the intersection point may be stored in the floats i_x and i_y.
//	char get_line_intersection(float p0_x, float p0_y, float p1_x, float p1_y, float p2_x, float p2_y, float p3_x, float p3_y, float *i_x, float *i_y)
//	{
//	    float s1_x, s1_y, s2_x, s2_y;
//	    s1_x = p1_x - p0_x;     s1_y = p1_y - p0_y;
//	    s2_x = p3_x - p2_x;     s2_y = p3_y - p2_y;
//
//	    float s, t;
//	    s = (-s1_y * (p0_x - p2_x) + s1_x * (p0_y - p2_y)) / (-s2_x * s1_y + s1_x * s2_y);
//	    t = ( s2_x * (p0_y - p2_y) - s2_y * (p0_x - p2_x)) / (-s2_x * s1_y + s1_x * s2_y);
//
//	    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
//	    {
//	        // Collision detected
//	        if (i_x != NULL)
//	            *i_x = p0_x + (t * s1_x);
//	        if (i_y != NULL)
//	            *i_y = p0_y + (t * s1_y);
//	        return 1;
//	    }
//
//	    return 0; // No collision
//	}

	/**
	 * Check if the robot is near the origin of given pitch (which it should be in)
	 * @param ourRobot
	 * @param PitchZone
	 * @return True if the robot is in 30mm^2 from the origin
	 */
	public static boolean isRobotNearOrigin(MovingObject ourRobot, PitchZone zone){
		return DistanceThreshold.VERYROUGH.isCloseEnough(ourRobot, zone.getOriginPoint());
	}

	/**
	 * Check if our robot is in our zone (attacker)
	 * @param ourRobot MovingObject model of our robot
	 * @param worldState The current world state
	 * @return True if robot is within our zone else false
	 */
	public static boolean robotIsWithinZone(MovingObject ourRobot, WorldState worldState) {
		boolean yes = true;
		boolean no = false;

		if (ourRobot.x == 0 && ourRobot.y == 0) {
			// Robot is not recognised by vision, assume it has left our zone
			return no;
		}
		return yes;
	}

	/** 
	 * Predict where the ball object that prevPoints represents will be
	 * @param worldState The worldState for the execution
	 * @return A point where the ball object should be soon
	 */
	public static MovingObject getPredictedLocation(WorldState worldState) {
		return worldState.predictNextState(64); // 2 seconds if the FPS is 16
	}

	public static MovingObject closePointToLine(MovingObject currentPos, MovingObject predictedPos, MovingObject point) {
		// Calculate line as y = mx + c
		float gradient, constant, cX, cY, pX, pY;
		cX = currentPos.x;
		cY = currentPos.y;
		pX = predictedPos.x;
		pY = predictedPos.y;
		gradient = (pY - cY) / (pX - cX); // m = (y2-y1) / (x2-x1)
		constant = cY - (gradient*cX); 	// c = y - mx, take any coordinate based on the line to solve c

		float y = (gradient*point.x) + constant;
		if (y < 100 || y > 365) {
			// Out of bounds, value taken from vision system
			return currentPos;
		}

		MovingObject pointToTravel = new MovingObject(point.x, y);
		return pointToTravel;
	}



	public static boolean robotNearEdges(StrategyState strategyState){

		int threshold1 = 20;
		int threshold2 = 40;

		PitchZone ourZone = strategyState.ourRobotZone;
		float ourRobotX = strategyState.ourRobot.x;
		float ourRobotY = strategyState.ourRobot.y;

		float dist_to_left = Math.abs(ourZone.leftEdge - ourRobotX);
		float dist_to_right = Math.abs(ourZone.rightEdge - ourRobotX);
		float dist_to_top = Math.abs(ourZone.topEdge - ourRobotY);
		float dist_to_bottom = Math.abs(ourZone.bottomEdge - ourRobotY);

		/*
		if(dist_to_left > threshold1 && dist_to_left < threshold2){
			System.out.println("leftEdge");
			return true;
		}

		if(dist_to_right > threshold1 && dist_to_right < threshold2){
			System.out.println("rightEdge");
			return true;
		}
		 */
		if(dist_to_top > threshold1 && dist_to_top < threshold2){
			System.out.println("topEdge");
			return true; 
		}

		if(dist_to_bottom > threshold1 && dist_to_bottom < threshold2){
			System.out.println("bottomEdge");
			return true;
		}


		return false;

	}


	public static boolean ballWasRecentlyInEnemyDefenderZone(StrategyState state){
		long currentTime = System.currentTimeMillis();
		long timeDelta = currentTime - state.lastTimeBallWasInEnemyDefenderZone;
		return timeDelta < 2500;
	}


}
