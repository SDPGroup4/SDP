package pc.strategynew;

import pc.strategynew.plan.DoNothingPlan;
import pc.strategynew.plan.Plan;
import pc.world.oldmodel.WorldState;

/**
 * Always returns a DoNothingPlan().
 * 
 * Used to simply wait and do nothing. Used to pause control
 * or to make the robot wait for something else to change
 * in the world (e.g. if the ball is removed from the pitch
 * or is in the wrong zone).
 */
public class DoNothingStrategy extends Strategy{

	@Override
	StrategyType getType() {
		return StrategyType.DO_NOTHING;
	}

	@Override
	Plan getNextPlan(WorldState worldState, StrategyState currentState) {
		return new DoNothingPlan();
	}
}
