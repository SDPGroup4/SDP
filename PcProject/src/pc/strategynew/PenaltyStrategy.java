package pc.strategynew;

import java.awt.Point;

import pc.movement.DistanceThreshold;
import pc.strategynew.plan.CatchBallPlan;
import pc.strategynew.plan.DoNothingPlan;
import pc.strategynew.plan.MoveToGetRunupPlan;
import pc.strategynew.plan.Plan;
import pc.strategynew.plan.ReturnToOriginPlan;
import pc.strategynew.plan.ScoreGoalPlan;
import pc.world.PitchZone;
import pc.world.PitchZone.PitchZoneType;
import pc.world.oldmodel.WorldState;

public class PenaltyStrategy extends Strategy{
	
	/**
	 * idea:
	 * same as the attacker strategy, but when we near origin and want to shot, we do confuse kick
	 * 
	 * to be implemented:
	 * adding it to the controller, or adding it to GUI
	 * adding a timer, if time out, kick anyway
	 */
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState) {
		Plan nextPlan = null;
		
		if (!strategyState.ballOnPitch && !strategyState.hasCaughtBall) {
			nextPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);	
		} else {
			// Ball is on pitch
			if (StrategyUtils.isballOutsideOurZone(worldState)) {
				nextPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);
				
			} else {
				// Ball is in our zone
				if (!strategyState.hasCaughtBall) {
					nextPlan = new CatchBallPlan(strategyState);
				} else{ // has caught the ball
					PitchZone enemyGoalZone = new PitchZone(PitchZoneType.ENEMY_DEFENDER, worldState);

					if(StrategyUtils.hasEnoughOfARunUp(strategyState, enemyGoalZone)){
						// Robot has the ball, attempt to score a goal
						
						// if near origin, we do confuse kick plan
						Point space = strategyState.ourRobotZone.getOriginPoint();
						boolean isNearOrigin = DistanceThreshold.ROUGH.isCloseEnough(strategyState.ourRobot, space);
						if(isNearOrigin){
							// control variables
							float ememyDefenderY = strategyState.opponentDefenderRobot.y;
							float[] possiblePoints = strategyState.enemyGoal;
							int offset = 10; // adjusting
							float bottomSidePoint = possiblePoints[0] - offset;
							float upperSidePoint = possiblePoints[1] + offset;
							float midPoint = possiblePoints[2];
							
							// calculate furtherest point from enemy according to y axis
							int upperDist = (int) Math.abs(ememyDefenderY - upperSidePoint);
							int midDist = (int) Math.abs(ememyDefenderY - midPoint);
							int bottomDist = (int) Math.abs(ememyDefenderY - bottomSidePoint);
							int furtherestDist = (int) Math.max(Math.max(upperDist,midDist),bottomDist);
							
							System.out.println(furtherestDist);
							
							// decide which destination to kick
							
							nextPlan = new ScoreGoalPlan(strategyState, worldState);
						} else{ // not near origin, normal attacker strategy
							// control variables
							float ememyDefenderY = strategyState.opponentDefenderRobot.y;
							float[] possiblePoints = strategyState.enemyGoal;
							int offset = 12; // adjusting
							float bottomSidePoint = possiblePoints[0] - offset;
							float upperSidePoint = possiblePoints[1] + offset;
							float midPoint = possiblePoints[2];
							
							// calculate furtherest point from enemy according to y axis
							int upperDist = (int) Math.abs(ememyDefenderY - upperSidePoint);
							int midDist = (int) Math.abs(ememyDefenderY - midPoint);
							int bottomDist = (int) Math.abs(ememyDefenderY - bottomSidePoint);
							int furtherestDist = (int) Math.max(Math.max(upperDist,midDist),bottomDist);
							
							System.out.println(furtherestDist);
							
							// decide which destination to kick
							if(furtherestDist == upperDist){
								nextPlan = new ScoreGoalPlan(strategyState, worldState);
							} else if(furtherestDist == bottomDist){
								if(Math.abs(StrategyUtils.calculateAbsoluteAngleDeg(strategyState.ourRobot.x, strategyState.ourRobot.y, strategyState.goalX, upperSidePoint)) > 58){ // if extreme angle, we travel to origin to get more space
									nextPlan = new ReturnToOriginPlan(strategyState.ourRobotZone, strategyState);
								} else {
									nextPlan = new ScoreGoalPlan(strategyState, worldState);
								}
								
							} else {
								nextPlan = new ScoreGoalPlan(strategyState, worldState);
							}
						}
						
					}else{
						nextPlan = new MoveToGetRunupPlan(strategyState, enemyGoalZone);
					}
				}
			}
		}
		return nextPlan != null ? nextPlan : new DoNothingPlan();
	}

	@Override
	StrategyType getType() {
		return StrategyType.PENALTY_ATTACKING;
	}
	
}
