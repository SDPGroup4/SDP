package pc.strategynew;


import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.plan.Plan;
import pc.strategynew.plan.TravelToPlan;
import pc.world.oldmodel.WorldState;


public class ReceiverStrategyStationary extends Strategy{

	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState){
		//Move to the centre and face the ball.
		return new TravelToPlan(
				strategyState.ourRobotZone.getOriginPoint().x,
				strategyState.ball.y,
				StrategyUtils.findClosest90Angle(strategyState.ourRobot),
				//StrategyUtils.calculateAbsoluteAngleDeg(strategyState.ourRobot, strategyState.ball),
				MovementSpeed.SLOW,
				DistanceThreshold.ROUGH,
				AngleThreshold.ROUGH,
				true);
		/*
		Plan currentPlan;

		if(!strategyState.catcherReady){
			return new CatchBallPlan(strategyState);
		}

		if(strategyState.ballSeenNearby
				//strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball)
				){ // ball in our zone

			currentPlan = new Plan() {
				@Override
				public PlanType getType() {
					return PlanType.CATCH_BALL;
				}
				@Override
				public Operation getNextOperation(WorldState worldState, StrategyState currentState) {
					return new KickerOperation(ActionType.CATCH);
				}
			};
		}else if(strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball)){
			return new CatchBallPlan(strategyState);
		}else{
			currentPlan = new TrackAllyStaticPlan(strategyState);
		}
		return currentPlan;
		*/
	}	


	@Override
	StrategyType getType() {
		return StrategyType.RECEIVER_STATIONARY;
	}

}
