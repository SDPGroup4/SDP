package pc.strategynew;


import pc.movement.ActionType;
import pc.strategynew.operation.KickerOperation;
import pc.strategynew.operation.Operation;
import pc.strategynew.plan.CatchBallPlan;
import pc.strategynew.plan.Plan;
import pc.strategynew.plan.TrackAllyMovingPlan;
import pc.world.oldmodel.WorldState;


public class ReceiverStrategyMoving extends Strategy{
		
	@Override
	Plan getNextPlan(WorldState worldState, StrategyState strategyState){
		Plan currentPlan;
		
		if(!strategyState.catcherReady){
			return new CatchBallPlan(strategyState);
		}
		
		if(strategyState.ballSeenNearby
				//strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball)
				){ // ball in our zone
			
			currentPlan = new Plan() {
				@Override
				public PlanType getType() {
					return PlanType.CATCH_BALL;
				}
				@Override
				public Operation getNextOperation(WorldState worldState, StrategyState currentState) {
					return new KickerOperation(ActionType.CATCH);
				}
			};
		}else if(strategyState.ourRobotZone.isInZoneIgnoreHeight(strategyState.ball)){
			return new CatchBallPlan(strategyState);
		}else{
			currentPlan = new TrackAllyMovingPlan(strategyState);
		}
		return currentPlan;
	}

	@Override
	StrategyType getType() {
		return StrategyType.RECEIVER_MOVING;
	}
	
	
	

}
