package pc.logging;
import java.io.IOException;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Logger;

public class Logging {
	private Logger mainLogger;
	private FileHandler mainFileHandler;
	
	/**
	 * Initiates the logger
	 * */
	public Logging(){
		mainLogger = Logger.getLogger("");
		Date d = new Date();
		try {
			String dateString = d.toString();
			dateString = dateString.replaceAll("[ \t:]+", "_");
			mainFileHandler = new FileHandler("logs/"+dateString+".txt");
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		mainLogger.addHandler(mainFileHandler);
		
	}
	
	public void Log(String msg){
		mainLogger.info(msg);
	}
}
