package pc.vision.gui.tools;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jssc.SerialPortException;
import pc.command.KickerCommand;
import pc.command.MovementCommand;
import pc.comms.gui.AvailableDevicesPanel;
import pc.comms.gui.ConnectionDialog;
import pc.comms.gui.ReceiveFromRobotPanel;
import pc.comms.gui.SendToRobotPanel;
import pc.control.RobotController;
import pc.control.SerialController;
import pc.movement.AngleThreshold;
import pc.movement.DistanceThreshold;
import pc.movement.MovementSpeed;
import pc.strategynew.StrategyController;
import pc.vision.gui.GUITool;
import pc.vision.gui.VisionGUI;
import pc.world.PitchZone.PitchZoneType;

public class StrategySelectorTool implements GUITool, RobotController.StateChangeListener{

	private VisionGUI gui;
	private JFrame subWindow;
	private StrategyController strategyController;
	private JLabel infoLabel = new JLabel();
	

	//A box for selecting how often commands are sent to the robot.
	private JLabel strategyTickLabel = new JLabel("Strategy Tick:");
	private JComboBox<Integer> strategyTickSelecter = new JComboBox<Integer>(new Integer[]{40, 50, 60, 75, 85, 100, 120, 150, 175, 200, 300, 400, 500, 600, 700, 800, 900, 1000});
	
	private JComboBox<Integer> defaultMovementSpeedSelector = new JComboBox<Integer>(new Integer[]{-1, 20, 30, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95, 100});
	private JComboBox<Integer> defaultAngleThreshSelector = new JComboBox<Integer>(new Integer[]{-1, 5, 7, 10, 12, 15, 18, 20, 25, 30, 35, 40, 45, 50, 55, 60});
	private JComboBox<Integer> defaultDistanceThreshSelector = new JComboBox<Integer>(new Integer[]{-1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 60});
	
	//A box for selecting which zone the robot we control is in.
	private JLabel ourRobotPositionLabel = new JLabel("Our Robot Position:");
	private JComboBox<String> pitchZoneSelector = new JComboBox<String>(new String[]{"Attacker","Defender", "EnemyAttacker", "EnemyDefender"});

	
	private JCheckBox acknowledgementsEnabledBox = new JCheckBox("Use Acknowledgements",SerialController.waitForAcknowledgements);
	
	//The panel that read in input the robot sends.
	private AvailableDevicesPanel devicesPanel = new AvailableDevicesPanel();
	private SendToRobotPanel outToRobotPanel = new SendToRobotPanel();
	private ReceiveFromRobotPanel inFromRobotPanel = new ReceiveFromRobotPanel();
	
	private JLabel statusLabel = new JLabel();

	private JButton connectButton = new JButton("Connect");
	private JButton disconnectButton = new JButton("Disconnect");
	
	private JButton catchButton = new JButton("Catch");
	private JButton resetCatcherButton = new JButton("ResetCatcher");
	private JButton kickButton = new JButton("Kick");
	private JButton stopRobotButton = new JButton("Stop Robot");
	
	private final SerialController serialController;
	
	public StrategySelectorTool(VisionGUI gui, StrategyController sc) {
		this.gui = gui;
		this.strategyController = sc;
		serialController = new SerialController();
		
		serialController.addStateChangeListener(this);
		
		setUpGUI();
		addActionListeners();
	}
	
	private void setUpGUI(){
		subWindow = new JFrame("Strategy Selector");
		subWindow.setResizable(true);
		subWindow.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		infoLabel.setAlignmentX(JLabel.RIGHT_ALIGNMENT);

		Container contentPane = subWindow.getContentPane();
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
		
		contentPane.add(makeConnectionPanel());
		contentPane.add(makeCommonCommandsPanel());
		contentPane.add(Box.createRigidArea(new Dimension(0, 5)));
		contentPane.add(makeDropDownMenusPanel());
		
		contentPane.add(Box.createRigidArea(new Dimension(0, 5)));
		contentPane.add(new JScrollPane(infoLabel));

		contentPane.add(Box.createRigidArea(new Dimension(0, 5)));
		contentPane.add(outToRobotPanel);

		inFromRobotPanel.setPreferredSize(new Dimension(580, 250));
		contentPane.add(inFromRobotPanel);

		contentPane.add(new StrategySelectionPanel(strategyController));
		
		updateInfoLabel();

	}
	
	private void addActionListeners(){
		strategyController.addPropertyChangeListener(new PropertyChangeListener() {
			@Override
			public void propertyChange(PropertyChangeEvent evt) {
				updateInfoLabel();
			}
		});
		
		acknowledgementsEnabledBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SerialController.waitForAcknowledgements = acknowledgementsEnabledBox.isSelected();
				System.out.println("Wait for acknolwedgements = "+SerialController.waitForAcknowledgements);
			}
		});
		
		pitchZoneSelector.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				JComboBox<?> cb = (JComboBox<?>)event.getSource();
				int index = cb.getSelectedIndex();
				switch(index){
				case 0 : strategyController.robotZone = PitchZoneType.ATTACKER; break;
				case 1 : strategyController.robotZone = PitchZoneType.DEFENDER; break;
				case 2 : strategyController.robotZone = PitchZoneType.ENEMY_ATTACKER; break;
				case 3 : strategyController.robotZone = PitchZoneType.ENEMY_DEFENDER; break;
				default: strategyController.robotZone = PitchZoneType.ATTACKER; break;
				}
			}
		});
		
		strategyTickSelecter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Integer newTick = (Integer) strategyTickSelecter.getSelectedItem();
				StrategyController.STRATEGY_TICK = newTick;
			}
		});
		
		defaultMovementSpeedSelector.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Integer speed = (Integer) defaultMovementSpeedSelector.getSelectedItem();
				MovementSpeed.DEFAULT_MOVEMENT_SPEED = speed;
			}
		});
		
		defaultAngleThreshSelector.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Integer angleThresh = (Integer) defaultAngleThreshSelector.getSelectedItem();
				AngleThreshold.DEFAULT_ANGLE_THRESH = angleThresh;
			}
		});
		
		defaultDistanceThreshSelector.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Integer distanceThresh = (Integer) defaultDistanceThreshSelector.getSelectedItem();
				DistanceThreshold.DEFAULT_DISTANCE_THRESH = distanceThresh;
			}
		});
		
		connectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				String selectedValue = devicesPanel.getSelectedSerialPort();
				connectButton.setText("Connect to "+selectedValue);
				String devicePort = selectedValue;
				try {
					ConnectionDialog.connectWithGUI(serialController, devicePort);
					strategyController.robotController = serialController;
					inFromRobotPanel.startListeningTo(serialController.getDataInputStream());
					outToRobotPanel.setController(serialController);
				} catch (SerialPortException e) {
					System.out.println("Caught a SerialPortException connect button listener.");
					e.printStackTrace();
				}
			}
		});
		
		disconnectButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				inFromRobotPanel.stopListening();
				outToRobotPanel.setController(null);
				serialController.close();
			}
		});
		
		kickButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				serialController.execute(new KickerCommand.Kick(100));
			}
		});
		
		catchButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				serialController.execute(new KickerCommand.Catch());
			}
		});

		resetCatcherButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				serialController.execute(new KickerCommand.ResetCatcher());
			}
		});
		
		stopRobotButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				serialController.execute(new MovementCommand.Stop());
			}
		});
		
		stateChanged();
	}
	
	private void updateInfoLabel() {
		infoLabel.setText(
				"<html>"
					+"Current Strategy: <b>"+ strategyController.getCurrentStrategy() + "</b><br />"
					+"Current Plan: <b>"+ strategyController.getCurrentPlan()+ "</b><br />"
					+"Current Operation: <b>"+ strategyController.getCurrentOperation()+ "</b><br />"
					+"Strategy Sytem: <b>"+ (strategyController.isPaused() ? "paused" : "running") + "</b><br />"
					+"Strategy Control: <b>"+ (strategyController.isAutomatic() ? "auto" : "manual") + "</b>"
				+ "</html>");
		//subWindow.pack();
	}

	private static int correctWidth = -1;
	@Override
	public void activate() {
		Rectangle mainWindowBounds = gui.getBounds();
		
		if(correctWidth == -1){
			subWindow.pack();
			correctWidth = subWindow.getWidth() + 50;
			subWindow.setSize(correctWidth, subWindow.getHeight());
		}
		
		subWindow.setVisible(true);
		
		int rhsOfScreen = (int)GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().getWidth();

		subWindow.setLocation(rhsOfScreen - correctWidth, mainWindowBounds.y + 30);
		//Old = mainWindowBounds.x + mainWindowBounds.width-100, mainWindowBounds.y);
	}

	@Override
	public boolean deactivate() {
		subWindow.setVisible(false);
		return true;
	}

	@Override
	public void dispose() {
		subWindow.dispose();
	}

	@Override
	public void stateChanged() {
		boolean isConnected = serialController.isConnected();
		
		System.out.println("State changed "+isConnected);

		String selectedValue = devicesPanel.getSelectedSerialPort();
		String status = isConnected ? "connected to " + selectedValue : "not connected";
		statusLabel.setText(status);
		
		connectButton.setText(isConnected ? "Reconnect to "+selectedValue : "Connect");
		
		disconnectButton.setVisible(isConnected);
		catchButton.setVisible(isConnected);
		resetCatcherButton.setVisible(isConnected);
		kickButton.setVisible(isConnected);
		stopRobotButton.setVisible(isConnected);
		strategyController.robotController = serialController;
	}
	
	public void closeEverything(){
		inFromRobotPanel.stopListening();
		outToRobotPanel.setController(null);
		devicesPanel.stop();
		serialController.close();
		strategyController.setPaused(true);
	}
	
	private JPanel makeConnectionPanel(){
		JPanel p = new JPanel();

		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(devicesPanel);
		
		JPanel connectionPanel = new JPanel();
		connectionPanel.setLayout(new BoxLayout(connectionPanel, BoxLayout.Y_AXIS));
		connectionPanel.add(connectButton);
		connectionPanel.add(disconnectButton);
		connectionPanel.add(acknowledgementsEnabledBox);
		

		p.add(connectionPanel);
		
		return p;
	}
	
	private JPanel makeCommonCommandsPanel(){
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.X_AXIS));
		p.add(catchButton);
		p.add(resetCatcherButton);
		p.add(kickButton);
		p.add(stopRobotButton);
		return p;
	}
	
	private JPanel makeDropDownMenusPanel(){
		
		JPanel p = new JPanel();
		p.setLayout(new GridLayout(5, 2));

		p.add(strategyTickLabel);
		strategyTickSelecter.setSelectedIndex(4);
		p.add(strategyTickSelecter);

		p.add(new JLabel("Override Movement Speed: "));
		p.add(defaultMovementSpeedSelector);
		
		p.add(new JLabel("Override Distance Threshold: "));
		p.add(defaultDistanceThreshSelector);
		
		p.add(new JLabel("Override Angle Threshold: "));
		p.add(defaultAngleThreshSelector);

		p.add(ourRobotPositionLabel);
		p.add(pitchZoneSelector);
		
		return p;
	}
}
