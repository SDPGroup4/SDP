package pc.vision.gui.tools;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import pc.strategynew.AttackerStrategy;
import pc.strategynew.DoNothingStrategy;
import pc.strategynew.FindSpaceStrategy;
import pc.strategynew.PassObstacleStrategy;
import pc.strategynew.PassOrdinaryStrategy;
import pc.strategynew.PenaltyStrategy;
import pc.strategynew.ReceiverStrategyMoving;
import pc.strategynew.ReceiverStrategyStationary;
import pc.strategynew.StrategyController;
import pc.strategynew.TrackBallStrategy;

@SuppressWarnings("serial")
public class StrategySelectionPanel extends JPanel{

	private final StrategyController strategyController;

	// GUI buttons to change strategies
	private JButton pauseController = new JButton("Pause");
	private JButton startController = new JButton("Start");

	private JButton automaticButton = new JButton("Automatic");
	private JButton maunalButton = new JButton("Manual");

	private JButton attackerButton = new JButton("Attacker");
	private JButton penaltyButton = new JButton("Penalty");

	private JButton doNothingButton = new JButton("Do Nothing");
	private JButton findSpaceButton = new JButton("Find Space");
	private JButton passOrdinaryButton = new JButton("Pass Ordinary");
	private JButton passObstacleButton = new JButton("Pass Obstacle");
	private JButton receiveStationaryButton = new JButton("Receive Stationary");
	private JButton receiveMovingButton = new JButton("Receive Moving");
	private JButton trackBallButton = new JButton("Track Ball");


	public StrategySelectionPanel(StrategyController controller) {
		this.strategyController = controller;

		setLayout(new GridLayout(4, 100));

		this.add(pauseController);
		this.add(startController);

		this.add(automaticButton);
		this.add(maunalButton);

		this.add(attackerButton);
		this.add(penaltyButton);
		this.add(doNothingButton);
		this.add(findSpaceButton);
		this.add(passOrdinaryButton);
		this.add(passObstacleButton);
		this.add(receiveStationaryButton);
		this.add(receiveMovingButton);
		this.add(trackBallButton);

		pauseController.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				strategyController.setPaused(true);
			}
		});

		startController.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strategyController.setPaused(false);
			}
		});

		maunalButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strategyController.setAutomaticStrategySelection(false);
			}
		});

		automaticButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strategyController.setAutomaticStrategySelection(true);
			}
		});

		attackerButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strategyController.setStrategy(new AttackerStrategy());
			}
		});

		penaltyButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				strategyController.setStrategy(new PenaltyStrategy());
			}
		});

		doNothingButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				strategyController.setStrategy(new DoNothingStrategy());
			}
		});

		findSpaceButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				strategyController.setStrategy(new FindSpaceStrategy());
			}
		});

		passOrdinaryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				strategyController.setStrategy(new PassOrdinaryStrategy());
			}
		});

		passObstacleButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				strategyController.setStrategy(new PassObstacleStrategy());
			}
		});

		trackBallButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				strategyController.setStrategy(new TrackBallStrategy());
			}
		});

		receiveStationaryButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strategyController.setStrategy(new ReceiverStrategyStationary());
			}
		});

		receiveMovingButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				strategyController.setStrategy(new ReceiverStrategyMoving());
			}
		});
	}
}
