package pc.comms;

import java.io.IOException;
import java.io.PipedInputStream;
import java.util.ArrayList;

import jssc.SerialPortException;

/**
 * An InputStream for easy IO to the serial port.
 */
public class SerialInputStream extends PipedInputStream {
	
	//A interface for classes that listen to data when it is read in, but do
	//not consume it (allowing debug messages and acknowledgement signals
	//to be sent over the same stream.
	public static interface SerialInputListener{
		public void onDataReceived(int data);
	}
	
	//The SerialComm to read and write data to.
	private SerialComm comm;
	
	//Listeners that can deal with input when it is read in, but not consume it.
	ArrayList<SerialInputListener> listeners = new ArrayList<SerialInputListener>();

	//Methods for and removing adding listeners.
	public void addInputListener(SerialInputListener listener){
		listeners.add(listener);
	}
	
	public void removeInputListener(SerialInputListener listener){
		listeners.remove(listener);
	}

	public SerialInputStream(final SerialComm comm) {
		super();
		this.comm = comm;
	}

	@Override
	public int available() throws IOException {
		try {
			if(comm.isOpen()){
				return comm.getPort().getInputBufferBytesCount();
			}
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
		return 0;
	}

	@Override
	public int read() throws IOException {
		try {
			int inData = comm.getPort().readIntArray(1)[0];
			for(SerialInputListener listener : listeners){
				listener.onDataReceived(inData);
			}
			return inData;
		} catch (SerialPortException e) {
			e.printStackTrace();
			return -1;
		}
	}
}
