package pc.comms;

import java.io.IOException;
import java.io.OutputStream;

import jssc.SerialPortException;

/**
 * An OutputStream for easy IO to the serial port.
 * Also contains other utility read/write methods
 * for different data types.
 */
public class SerialOutputStream extends OutputStream {

private SerialComm comm;
	
	
	public SerialOutputStream(SerialComm comm) {
	super();
	this.comm = comm;
}


    public void write(byte data) throws IOException {
        try {
                comm.getPort().writeByte(data);
        } catch (SerialPortException e) {
                e.printStackTrace();
        }
}

    public void write(int data) throws IOException {
        try {
                comm.getPort().writeInt(data);
        } catch (SerialPortException e) {;
                e.printStackTrace();
        }
}

    public void write(int[] data) throws IOException {
        for (int i = 0; i < data.length; ++i) {
                try {
                        comm.getPort().writeInt(data[i]);
                } catch (SerialPortException e) {
                        e.printStackTrace();
                }
        }
}

    public void write(byte[] data) throws IOException {
            try {
                    comm.getPort().writeBytes(data);
            } catch (SerialPortException e) {
                    e.printStackTrace();
            }
    }
}
