package pc.comms.gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;

import jssc.SerialPortException;
import pc.control.SerialController;

/**
 * A basic GUI for connecting to serial ports,
 * typing in commands to send to the robot,
 * and displaying whatever the robot sends back.
 */
public class SerialControlGUI extends JFrame{

	private static final long serialVersionUID = 320108104861314103L;

	private SerialController controller = null;

	private AvailableDevicesPanel devicesPanel;
	private JButton openButton = new JButton("Open");
	private JButton closeButton = new JButton("Close");
	
	private ReceiveFromRobotPanel fromRobotPanel = new ReceiveFromRobotPanel();
	private SendToRobotPanel toRobotPanel = new SendToRobotPanel();

	public SerialControlGUI(){
		setTitle("Robot Controller");

		setMinimumSize(new Dimension(400, 200));
		setResizable(true);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);

		setUpGUIElements();

		registerActionListeners();
	}

	private void registerActionListeners() {
		//Stop all the necessary Threads and stream when the window closes.
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if(controller != null){
					controller.close();
				}
				if(devicesPanel != null){
					devicesPanel.stop();
				}
				if(fromRobotPanel != null){
					fromRobotPanel.stopListening();
				}
			}
		});


		//Connect to the selected device, and connect the sender
		//and receiver elements of the GUI to it.
		//Makes sure to close any old connections first.
		openButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Pressed open!");

				String portName = devicesPanel.getSelectedSerialPort();

				if(portName != null && portName.length() > 0){
					//Close old connections
					if(controller != null){
						fromRobotPanel.stopListening();
						toRobotPanel.setController(null);
						controller.close();
					}
					
					controller = new SerialController();
					boolean opened = false;
					
					try {
						//Allow for retries with this dialog
						ConnectionDialog.connectWithGUI(controller, portName);
						opened = true;
					} catch (SerialPortException e1) {
						e1.printStackTrace();
					}
					
					if(opened){
						System.out.println("Opened port at "+portName);
						fromRobotPanel.startListeningTo(controller.getDataInputStream());
						toRobotPanel.setController(controller);
					}else{
						fromRobotPanel.stopListening();
						toRobotPanel.setController(null);
					}
				}
			}
		});

		//Disconnect from the serial port, and stop any
		//relevant threads or streams.
		closeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.out.println("Closed pressed.");
				if(controller != null){
					fromRobotPanel.stopListening();
					controller.close();
					controller = null;
				}
			}
		});
	}

	public void setUpGUIElements(){

		setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		c.anchor=GridBagConstraints.NORTHWEST;// WEST;

		c.gridwidth = 1;
		c.gridx=0;
		c.gridy=0;
		c.gridheight = 3;
		c.insets=new Insets(10, 50, 50, 20); //tlbr
		devicesPanel = new AvailableDevicesPanel();
		add(devicesPanel, c);

		c.gridheight = 1;
		//c.insets.bottom = 10;
		c.insets.top=20;
		c.insets.left=10;
		c.insets.bottom = 0;
		c.gridx++;
		add(openButton, c);

		c.gridy++;
		c.insets.top = 0;
		add(closeButton, c);


		c.gridy = 0;
		c.gridx++;
		c.gridheight = 3;
		c.insets.top = 20;
		c.insets.bottom = 20;
		add(toRobotPanel, c);

		c.gridy = 0;
		c.gridx++;
		c.gridheight = 3;
		c.insets.top = 20;
		c.insets.bottom = 20;
		add(fromRobotPanel, c);

		pack();
	}

	//Create a visible GUI, and allow all the
	//constructors and event listeners to do all the work.
	public static void main(String[] args) throws SerialPortException {
		SerialControlGUI gui = new SerialControlGUI();
		gui.setVisible(true);
	}
}
