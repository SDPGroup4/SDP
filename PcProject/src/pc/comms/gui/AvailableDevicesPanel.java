package pc.comms.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import jssc.SerialPortException;
import pc.comms.SerialComm;

/**
 * A panel that displays an auto-refreshing
 * list of serial ports to be connected to.
 */
public class AvailableDevicesPanel extends JPanel implements Runnable, ListSelectionListener{
	private static final long serialVersionUID = -1502079015180092540L;

	//How often to poll the list of available serial ports (milliseconds).
	private static final int REFRESH_TIME_MILLIS = 1000;

	//A thread for checking available ports.
	private Thread refreshThread;
	
	//Used to terminate the thread.
	private volatile boolean keepRunning = true;

	//The selected port name.
	String selectedValue = "";
	
	
	private JLabel label = new JLabel("Available Devices:");

	//The GUI component listing all the devices.
	JList<String> devicesList = new JList<String>(new DefaultListModel<String>());
	
	private ArrayList<String> currentlyConnectedSerialPorts = new ArrayList<String>();
	
	public AvailableDevicesPanel(){
		setUpGUI();
		devicesList.addListSelectionListener(this);
		start();
	}


	private void setUpGUI() {
		setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.gridx = 0;
		c.gridy = 0;
		add(label, c);

		c.gridx = 0;
		c.gridy++;
		devicesList.setPrototypeCellValue("/dev/ttyACM100");
		devicesList.setMinimumSize(new Dimension(100, 100));
		devicesList.setBackground(Color.WHITE);
		devicesList.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		devicesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		add(devicesList, c);
	}

	
	/**
	 * Start polling for serial ports and auto-refreshing the list.
	 */
	public void start(){
		refreshThread= new Thread(this);
		refreshThread.start();
	}

	/**
	 * Stop auto-refreshing the list, and
	 * stop the background thread.
	 */
	public void stop(){
		keepRunning = false;

		try {
			refreshThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}	

	//Check to see if the list of ports has changed,
	//and update the GUI from the correct thread if necessary.
	//Also makes sure to maintain the currently selected value
	//whenever the list changes (even if it is in a different position).
	public void run(){
		while(keepRunning){

			final DefaultListModel<String> model = new DefaultListModel<String>();
			boolean shouldUpdateModel = false;

			String[] availablePorts;
			try {
				availablePorts = SerialComm.getAvailablePorts();

				shouldUpdateModel = availablePorts.length != currentlyConnectedSerialPorts.size();

				for(int i = 0; i < availablePorts.length; i++){
					String portName = availablePorts[i];

					if(!shouldUpdateModel && !portName.equals(currentlyConnectedSerialPorts.get(i))){
						shouldUpdateModel = true;
					}
					model.add(model.size(), portName);
				}
			} catch (SerialPortException e1) {
				e1.printStackTrace();
			}

			if(shouldUpdateModel){
				currentlyConnectedSerialPorts.clear();
				for(int i = 0; i < model.size(); i++){
					currentlyConnectedSerialPorts.add(model.get(i));
				}


				SwingUtilities.invokeLater(new Runnable(){
					@Override
					public void run() {
						String selected = devicesList.getSelectedValue();
						devicesList.setModel(model);

						if(selected != null){

							for(int i = 0; i < model.size(); i++){
								if(model.get(i).equals(selected)){
									devicesList.setSelectedIndex(i);
									break;
								}
							}
						}
					}
				});	
			}

			try{
				Thread.sleep(REFRESH_TIME_MILLIS);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}


	//Triggered when a new value is selected (not just the
	//same value twice).
	public void onSelectedValueChanged(String newValue){
		System.out.println("Newly selected port = "+newValue);
	}


	//Update the currently selected value.
	@Override
	public void valueChanged(ListSelectionEvent event){
		String newlySelectedValue = devicesList.getSelectedValue();

		if(	selectedValue != newlySelectedValue && 
			(selectedValue == null || !selectedValue.equals(newlySelectedValue))){
			
			selectedValue = newlySelectedValue;
			onSelectedValueChanged(selectedValue);
		}
	}
	

	/**
	 * Get the name of the selected serial port. May return null.
	 */
	public String getSelectedSerialPort(){
		return selectedValue;
	}
}
