package pc.comms.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import pc.command.RobotCommand;
import pc.command.RobotCommandFactory;
import pc.control.RobotController;


/**
 * Reads in textual input from the user, translates
 * it to a robot command, and then sends it to the robot
 * to execute.
 */
public class SendToRobotPanel extends JPanel{
	private static final long serialVersionUID = -5202561564112662754L;

	private JLabel label = new JLabel("  Input:  ");
	private JTextField inputField = new JTextField(20);
	private JButton sendButton = new JButton("Send");

	//The controller to send commands to.
	private RobotController<?, ?> controller = null;

	public SendToRobotPanel() {
		setUpGUI();
		registerActionListeners();
	}

	private void registerActionListeners() {
		//Send commands when the button is pressed, or the enter
		//key is pressed.
		
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				send();
			}
		});

		inputField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				send();
			}
		});
	}

	//Try to read a valid command from the user's text, and
	//send it to the robot controller.
	public void send(){
		if(controller != null && controller.isConnected()){
			try{
				RobotCommand command = RobotCommandFactory.stringToCommand(inputField.getText());
				if(command != null){
					System.out.println("About to send: "+command);
					controller.execute(command);
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
		}else{
			System.out.println("Controller not connected.");
		}
	}

	private void setUpGUI() {
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(label);
		inputField.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		add(inputField);
		add(sendButton);
	}

	public void setController(RobotController<?, ?> controller){
		this.controller = controller;
	}
}
