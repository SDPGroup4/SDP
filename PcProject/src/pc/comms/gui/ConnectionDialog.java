package pc.comms.gui;

import java.awt.Dialog.ModalityType;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import pc.control.RobotController;

/**
 * A dialog box for indicating connections are
 * happening, and allowing the user to retry if the connection fails.
 */
public  class ConnectionDialog<Controller extends RobotController<ConnectionType, ExceptionType> ,ConnectionType ,ExceptionType extends Exception>{

	private JDialog window;
	private ExceptionType exception;

	private ConnectionDialog(final Controller controller, final ConnectionType connection) throws ExceptionType {
		window = new JDialog(null, "Connecting", ModalityType.APPLICATION_MODAL);
		window.setSize(400, 150);
		window.setResizable(false);
		window.setLocationRelativeTo(null);
		window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

		JLabel label = new JLabel("Connecting to " + connection);
		window.add(label);
		System.out.println(label.getText());

		new Thread(new Runnable() {
			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				try {
					while (true) {
						try {
							if(controller.isConnected()){
								controller.close();
							}
							boolean success = controller.connect(connection);
							if(success){
								System.out.println("Able to open port at "+connection);
							}else{
								System.out.println("Unble to open port at "+connection);
							}
							break;
						} catch (Exception e) {

							int result = JOptionPane.showConfirmDialog(
									window,
									"Connection failed. Retry?\n\n"
											+ e.getMessage() + "\n\n"
											+ e.getCause(),
											"Connection failed",
											JOptionPane.YES_NO_OPTION,
											JOptionPane.ERROR_MESSAGE);
							if (result == JOptionPane.YES_OPTION)
								continue;

							exception = (ExceptionType) e;
							return;
						}
					}
				} finally {
					SwingUtilities.invokeLater(new Runnable() {

						@Override
						public void run() {
							window.dispose();
						}
					});
				}
			}
		}).start();
		window.setVisible(true);
		if (exception != null)
			throw exception;
	}

	/**
	 * connectWithGUI(Controller controller, ConnectionType connection)
	 * Usually called with something like connectWithGUI(serialController, portName);
	 * Opens dialog box for indicating connections are
	 * happening, and allowing the user to retry if the connection fails.
	 */
	public static <Controller extends RobotController<ConnectionType, ExceptionType> ,ConnectionType ,ExceptionType extends Exception> void connectWithGUI(Controller controller, ConnectionType connection) throws ExceptionType{
		new ConnectionDialog<Controller,ConnectionType,ExceptionType>(controller, connection);
	}
}