package pc.comms.gui;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

/**
 * A text area that reads and displays the
 * input from an inputStream. It auto-refreshes
 * via a thread any time there is something new that can be read
 * from the stream.
 */
public class ReceiveFromRobotPanel extends JPanel implements Runnable{
	private static final long serialVersionUID = 602033013538855860L;

	//Where to read from
	private InputStream in = null;
	
	//The thread that reads from the inputStream
	private Thread refreshThread = null;
	
	//Used for killing the thread.
	private volatile boolean keepRunning = true;

	//How long (milliseconds) to wait between checking for input.
	private static final int REFRESH_TIME_MILLIS = 5;
	
	//Stores the textual contents read from the stream so far.
	private StringBuilder sb = new StringBuilder();

	private JLabel label = new JLabel("Output:");
	private JButton clearButton = new JButton("Clear");
	private JTextArea textArea = new JTextArea("", 10, 40);

	public ReceiveFromRobotPanel() {
		setUpGUI();
		registerActionListeners();
	}

	private void registerActionListeners() {
		//Clear all previous text from the screen.
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				clear();
			}
		});
	}

	private void setUpGUI() {
		setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		c.gridx = 0;
		c.gridy = 0;
		add(label, c);
		
		c.gridx = 0;
		c.gridy = 1;
		add(clearButton, c);

		textArea.setAutoscrolls(true);
		textArea.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		JScrollPane scrollPane = new JScrollPane(textArea);
		
		c.gridx = 1;
		c.gridy = 0;
		c.gridheight = 2;
		add(scrollPane, c);
		

		
//		setLayout(new GridBagLayout());
//
//		GridBagConstraints c = new GridBagConstraints();
//
//		add(label, c);
//
//		c.gridy++;
//		textArea.setAutoscrolls(true);
//
//		textArea.setBorder(BorderFactory.createLineBorder(Color.BLACK));
//		JScrollPane scrollPane = new JScrollPane(textArea);
//		add(scrollPane, c);
//		
//		c.gridy++;
//		add(clearButton, c);
	}

	//Start a thread that automatically listens to the
	//given InputStream (making sure that any previous one
	//is stopped correctly.
	public void startListeningTo(InputStream in){
		if(in != null){
			if(refreshThread != null){
				stopListening();
			}
			
			keepRunning = true;
			this.in = in;
			refreshThread= new Thread(this);
			refreshThread.start();
		}
	}

	//Stop listening to the current InputStream,
	//and kill the auto-refresh thread.
	public void stopListening(){
		keepRunning = false;
		if(refreshThread != null){
			try {
				refreshThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			refreshThread = null;
		}
	}

	//Remove all previous text from the
	//screen and the stringbuilder.
	public void clear(){
		textArea.setText("");
		sb = new StringBuilder();
	}

	//Read in characters, add the to the buffer (including
	//automatic newlines where necessary, and then update
	//the GUI from the correct thread with the new text.
	public void run(){
		while(keepRunning){
			boolean shouldUpdateText = false;
			if(sb.length() > 1000){
				sb = new StringBuilder();
			}
			try {
				while(in.available() > 0){
					char nextChar = (char) in.read();
					sb.append(nextChar);
					shouldUpdateText = true;
				}
			} catch (IOException e1) {
				e1.printStackTrace();
			}

			if(shouldUpdateText){
				sb.append('\n');
				SwingUtilities.invokeLater(new Runnable(){
					@Override
					public void run() {
						textArea.setText(sb.toString());
					}
				});
			}

			try{
				Thread.sleep(REFRESH_TIME_MILLIS);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}

		if(in != null){
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			in = null;
		}
	}
}
