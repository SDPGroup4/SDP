package pc.comms;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

// JSSC methods: http://java-simple-serial-connector.googlecode.com/svn/trunk/additional_content/javadoc/0.8/jssc/package-summary.html
// example implementation: https://code.google.com/p/myrobotlab/source/browse/trunk/myrobotlab/src/org/myrobotlab/serial/jssc/SerialDeviceJSSC.java?r=1639

/**
 * Handles serial communication. Is a wrapper around
 * a JSSC SerialPort to allow for easier IO with
 * a higher level of abstraction.
 */
public class SerialComm {
	
	//The port to read from or write to.
	private SerialPort port;
	
	//Streams to read and write to for easier IO.
	SerialInputStream in = new SerialInputStream(this);
	SerialOutputStream out = new SerialOutputStream(this);

	public SerialPort getPort(){
		return port;
	}

	/**
	 * Return the number of bytes available to read
	 * from the serial port.
	 */
	public int available() throws java.io.IOException{
		try {
			return this.getPort().getInputBufferBytesCount();
		} catch (SerialPortException e) {
			e.printStackTrace();
			return -1;
		}
	}

	//Getting input and output streams to make IO easier.
	public SerialInputStream getInputStream(){
		return in;
	}

	public SerialOutputStream getOutputStream(){
		return out;
	}
	

	/**
	 * Connect to the specified port. 
	 * Usually of the form "/dev/ttyACM0" 
	 * on Linux or "COM0" on Windows.
	 */
	public boolean open(String portName) throws SerialPortException{
		port = new SerialPort(portName);
		if (isOpen()) {
			return false;
		}
		port.openPort();
		return true;
	}

	public boolean isOpen(){
		return port != null && port.isOpened();
	}

	public void write(byte[] data) throws java.io.IOException{
		try {
			getPort().writeBytes(data);
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
	}
	
	public byte[] read(byte[] data) throws java.io.IOException{
		try {
			return getPort().readBytes();
		} catch (SerialPortException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void close() throws SerialPortException{
		if(isOpen()){
			getPort().closePort();
		}
	}
	
	/**
	 * Get the list of names for all the serial ports
	 * that could be connected to on this computer.
	 * Usually of the form "/dev/ttyACM0" 
	 * on Linux or "COM0" on Windows.
	 */
	public static String[] getAvailablePorts() throws SerialPortException{
		return SerialPortList.getPortNames();
	}
}
