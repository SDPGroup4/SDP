package pc.control;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import jssc.SerialPortException;
import pc.command.RobotCommand;
import pc.command.RobotOpcode;
import pc.comms.SerialComm;
import pc.comms.SerialInputStream.SerialInputListener;
import pc.util.CharUtils;
import pc.util.SplittingDataStream;

/**
 * A RobotController that connects via a serial port given its
 * name as a String.
 */
public class SerialController extends RobotController<String, SerialPortException> implements SerialInputListener{

	public static boolean waitForAcknowledgements = true;
	
	private static final int RETRY_TIMEOUT = 180;
	private static final int NUM_RETRIES = 7;

	//The serial port this controller is connected to.
	private SerialComm comm = null;

	// Acknowledgement variable state, can be 0, 1, 2 or 3
	private int ackState = 0;

	// Access current command for onDataReceived
	private RobotCommand rc = null;

	//Try and open up a port at the given name. Usually a string like
	// "/dev/ttyACM0" on Linux or "COM0" on Windows.
	@Override
	public boolean connect(String portName) throws SerialPortException {
		comm = new SerialComm();
		try{
			if (!comm.open(portName)){
				setConnected(false);
				return false;
			}
			inputFromRobot = new DataInputStream(comm.getInputStream());
			comm.getInputStream().addInputListener(this);
			outputToRobot = new DataOutputStream(new SplittingDataStream(comm.getOutputStream(), System.out));
			setConnected(true);
			return true;
		}catch(SerialPortException e){
			System.out.println("Unable to connect to port.");
			throw e;
		}
	}

	//Close the port and its respective streams.
	@Override
	public void close() {
		try {
			if (comm != null && comm.isOpen()){
				comm.close();
			}
		} catch (SerialPortException e) {
			e.printStackTrace();
		}

		inputFromRobot = null;
		outputToRobot = null;
		setConnected(false);
	}



	/**
	 * Sends the command to the serial port, and waits for an acknowledgement.
	 * It will re-send the command until it gets an acknowledgement (waiting 20ms
	 * between attempts), but will stop after 100 retries to avoid 
	 * getting stuck in an infinite loop.
	 */
	@Override
	public boolean executeSync(RobotCommand command) {
		RobotOpcode opCode = command.getOpcode();
		this.rc = command;
		int counter = 0;
		boolean receivedAck = false;
		lastAcknowledgedOpcode = 0;
		
		long startTime = System.currentTimeMillis();
		
		do{
			System.out.println("Executing "+command);
			super.executeSync(command);
			counter++;
			
			if(waitForAcknowledgements){
				long time = System.currentTimeMillis();
				while (System.currentTimeMillis() - time < RETRY_TIMEOUT){
					if(!awaitingAcknowledgement(opCode.getOpCode())){
						receivedAck = true;
						break;
					}
				}
			}else{
				//Resend 5 times and assume the robot got it.
				receivedAck = counter >= 3;
			}
		}while((!receivedAck && counter < NUM_RETRIES));

		long finishTime = System.currentTimeMillis();
		long timeDelta = finishTime - startTime;
		
		if (!receivedAck) {
			System.out.println("\nFailed to acknowledge " + opCode+" after "+timeDelta+"ms.");
		} else {
			System.out.println("Acknowledged " + opCode+" after "+timeDelta+"ms.");
		}
		return receivedAck;
	}

	/**
	 * Checks for an acknowledgement signal on the given InputStream.
	 * Acknowledgement must contain a sequence of security bytes,
	 * and must match the given opcode.
	 */			
	public boolean awaitingAcknowledgement(int opCode) {
		if (opCode == lastAcknowledgedOpcode) {	
			lastAcknowledgedOpcode = 0;
			return false;
		}
		return true;
	}

	private int lastAcknowledgedOpcode = 0;

	/**
	 * State machine for how many bytes of the acknowledgement have been received
	 */
	@Override
	public void onDataReceived(int data) {
		System.out.println("Got "+data+" ("+CharUtils.intToCharacterString(data)+")");
		if (ackState == 0 && data == 56) {
			ackState = 1;
			//System.out.println("Ack. state = 1");
		} else if (ackState == 1 && data == 42) {
			ackState = 2;
			//System.out.println("Ack. state = 2");
		} else if (ackState == 2 && data == 91) {
			ackState = 3;
			//System.out.println("Ack. state = 3");
		} else if (ackState == 3 && rc != null && data == rc.getOpcode().getOpCode()) {
			ackState = 4;
			System.out.println("Ack. state = 4");
			lastAcknowledgedOpcode = data;
		} else {
			ackState = 0;
			//System.out.println("Not a valid acknowledgement byte");
		}
	}
}
