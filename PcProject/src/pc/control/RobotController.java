package pc.control;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import pc.command.RobotCommand;

/**
 * A generic skeleton for a class that transmits RobotCommands
 * to an OutputStream. Used as an intermediary between the
 * strategy code and the robot. 
 * 
 * Uses Generics to allow subclasses to
 * connect via different methods and throw different Exceptions.
 * 
 * This abstraction should make making a simulator easier, as RobotController
 * does not rely on being wired to a serial port, so the executeSync() method
 * may handle commands completely virtually without using ports.
 */
public abstract class RobotController<ConnectionType, ExceptionType extends Exception>{

	//Streams to and from the robot.
	protected DataInputStream inputFromRobot;
	protected DataOutputStream outputToRobot;

	private boolean connected;



	//Listeners that can respond to this port connecting or disconnecting.
	private List<StateChangeListener> stateChangeListeners;

	//Allows commands to be sent on a different thread.
	private ExecutorService executor;
	private final static ThreadFactory EXECUTOR_FACTORY = new ThreadFactory() {
		@Override
		public Thread newThread(Runnable runnable) {
			Thread t = new Thread(runnable, "BrickCommServer executor");
			t.setDaemon(true);
			return t;
		}
	};

	public RobotController(){
		stateChangeListeners = new ArrayList<RobotController.StateChangeListener>();
		connected = false;
		executor = Executors.newSingleThreadExecutor(EXECUTOR_FACTORY);
	}

	/**
	 * Establish a connection to wherever the commands are being sent.
	 * Should set up the input and output streams used later.
	 * Should also make use of the setConnected() method to automatically inform
	 * state listeners.
	 */
	public abstract boolean connect(ConnectionType connection) throws ExceptionType;

	/**
	 * Close the input and output streams, and perform any other necessary teardown.
	 */
	public abstract void close();


	/**
	 * Whether the controller is ready to send and receive data.
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * Update the connection state, and
	 * inform all listeners if it has changed.
	 */
	protected void setConnected(boolean connected) {
		if (this.connected != connected) {
			this.connected = connected;

			for (StateChangeListener listener : stateChangeListeners) {
				listener.stateChanged();
			}
		}
	}

	public void addStateChangeListener(StateChangeListener listener) {
		stateChangeListeners.add(listener);
	}

	public void removeStateChangeListener(StateChangeListener listener) {
		stateChangeListeners.remove(listener);
	}

	/**
	 * Executes a command asynchronously. Returns immediately and is safe to
	 * call from any thread.
	 */
	public void execute(final RobotCommand command) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				executeSync(command);
			}
		});
	}

	/**
	 * Executes a command synchronously. Never call this method from GUI or
	 * frame grabber thread!
	 * 
	 * This is called from execute() too, so to override any sending behaviour,
	 * this is the place to do it.
	 */
	public boolean executeSync(RobotCommand command) {
		if (outputToRobot != null){
			try {
				command.send(outputToRobot, true);
				outputToRobot.flush();
				return true;
			} catch (IOException e) {
				e.printStackTrace();
				close();
			}
		}
		return false;
	}

	/**
	 * An interface for responding in some way to the connection state changing.
	 */
	public interface StateChangeListener {
		void stateChanged();
	}

	/**
	 * Get the input stream to read data from the robot.
	 */
	public DataInputStream getDataInputStream(){
		return inputFromRobot;
	}
}
