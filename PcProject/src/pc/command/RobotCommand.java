package pc.command;

import java.io.DataOutputStream;
import java.io.IOException;

import pc.util.ByteArrayUtils;

public abstract class RobotCommand{
	public abstract RobotOpcode getOpcode();

	public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
		System.out.println("Sending "+this);
		if(writeSecurityBytes){
			out.writeByte(56);
			out.writeByte(42);
			out.writeByte(91);
		}
		out.writeByte(getOpcode().getOpCode());
	}


	public String toString(){
		return getOpcode().toString();
	}


	//Used by subclasses to make writing to the stream more consistent,
	//and make sure the bytes are in the order the Arduino expects.

	protected void writeBoolean(DataOutputStream out, boolean value) throws IOException{
		out.writeBoolean(value);
	}

	protected void writeInt(DataOutputStream out, int value) throws IOException{
		out.writeByte(value >> 8);
		out.writeByte(value);
	}

	protected void writeFloat(DataOutputStream out, float value) throws IOException{
		byte[] floatBytes = ByteArrayUtils.floatToByteArray(value);
		out.write(floatBytes);
	}
}
