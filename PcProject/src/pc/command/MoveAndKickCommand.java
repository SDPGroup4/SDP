package pc.command;

import java.io.DataOutputStream;
import java.io.IOException;

public class MoveAndKickCommand extends RobotCommand{

	private final MovementCommand moveCommand;
	private final KickerCommand kickCommand;
	private final int timeToMoveForwardsMs;

	public MoveAndKickCommand(MovementCommand moveCommand, KickerCommand kickCommand, int timeToMoveForwardsMs) {
		this.moveCommand = moveCommand;
		this.kickCommand = kickCommand;
		this.timeToMoveForwardsMs = timeToMoveForwardsMs;
	}

	@Override
	public RobotOpcode getOpcode() {
		return RobotOpcode.MOVE_AND_KICKER;
	}

	@Override
	public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
		super.send(out, writeSecurityBytes);
		moveCommand.send(out, false);
		kickCommand.send(out, false);
		writeInt(out, timeToMoveForwardsMs);
	}
}
