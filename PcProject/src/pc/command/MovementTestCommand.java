package pc.command;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Commands for calibrating motors and timings.
 * These take in more arguments than the MovementCommands,
 * allowing you to set seperate powers for each wheel, and
 * have the movement automatically stop after a specified delay.
 */
public abstract class MovementTestCommand extends RobotCommand{
	
	//Motor percentages for each wheel
	private final int leftPower;
	private final int rightPower;
	
	//Time in millisecconds to run the motors for
	private final int delay;
	
	
	public MovementTestCommand(int leftPower, int rightPower, int delay) {
		this.leftPower = Math.min(leftPower, 100);
		this.rightPower = Math.min(rightPower, 100);
		this.delay = delay;
	}

	//Write this command's opcode, the power for each wheel, and the
	//time to run the motors for in milliseconds.
	@Override
	public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
		super.send(out, writeSecurityBytes);
		writeInt(out, leftPower);
		writeInt(out, rightPower);
		writeInt(out, delay);
	}

	@Override
	public String toString() {
		return super.toString()+" "+leftPower+" "+rightPower+" "+delay;
	}
	

	//Specific classes for each type of motion to test:
	
	public static class BackwardsTest extends MovementTestCommand{
		public BackwardsTest(int leftPower, int rightPower, int delay) {
			super(leftPower, rightPower, delay);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.BACKWARDS_TEST;
		}
	}

	public static class RotateLeftTest extends MovementTestCommand{
		public RotateLeftTest(int leftPower, int rightPower, int delay) {
			super(leftPower, rightPower,  delay);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.ROTATE_LEFT_TEST;
		}
	}

	public static class RotateRightTest extends MovementTestCommand{
		public RotateRightTest(int leftPower, int rightPower, int delay) {
			super(leftPower, rightPower,  delay);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.ROTATE_RIGHT_TEST;
		}
	}

	public static class ForwardsTest extends MovementTestCommand{
		public ForwardsTest(int leftPower, int rightPower, int delay) {
			super(leftPower, rightPower,  delay);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.FORWARDS_TEST;
		}
	}
}

