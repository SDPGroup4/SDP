package pc.command;

/**
 * The types of all the commands the
 * robot should be able to respond to,
 * and their corresponding opcodes.
 */
public enum RobotOpcode {	
	//Moving
	STOP(1, "STOP"),
	FORWARDS(2, "FORWARDS"),
	BACKWARDS(3, "BACKWARDS"),
	ROTATE_LEFT(4, "ROTATE_LEFT"),
	ROTATE_RIGHT(5, "ROTATE_RIGHT"),
	
	//Kicking and Catching
	KICK(6, "KICK"),
	CATCH(7, "CATCH"),
	RESET_CATCHER(8, "RESET_CATCHER"),
	
	//Testing motion
	FORWARDS_TEST(9, "FORWARDS_TEST"),
	BACKWARDS_TEST(10, "BACKWARDS_TEST"),
	ROTATE_LEFT_TEST(11, "ROTATE_LEFT"),
	ROTATE_RIGHT_TEST(12, "ROTATE_RIGHT"),
	TEST_KICK(13, "TEST_KICK"),
	
	//Tests for communications:
	TEST(14, "TEST"),
	TEST_INT(15, "TEST_INT"),
	TEST_FLOAT(16, "TEST_FLOAT"),
	TEST_INT_AND_FLOAT(17, "TEST_INT_AND_FLOAT"),
	TEST_BOOL(18, "TEST_BOOL"),
	TEST_BYTE(19, "TEST_BYTE"),
	
	MOVE_AND_KICKER(20, "HYBRID_MOVE_AND_KICKER")
	;
	
	private final byte opCode;
	private final String label;
	
	private RobotOpcode(int opCode, String label){
		this.opCode = (byte) opCode;
		this.label = label;
	}
	public String toString(){
		return label;
	}
	public byte getOpCode(){
		return opCode;
	}
}
