package pc.command;

import java.io.DataOutputStream;
import java.io.IOException;

public abstract class KickerCommand extends RobotCommand{
	
	public static class Kick extends KickerCommand{
		private final int power;
		public Kick(int power) {
			this.power = power;
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.KICK;
		}
		//Transmit the kick opcode and the power to kick with.
		@Override
		public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
			super.send(out, writeSecurityBytes);
			writeInt(out, power);
		}

		@Override
		public String toString() {
			return super.toString()+" "+power;
		}
	}

	public static class Catch extends KickerCommand{
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.CATCH;
		}
	}

	public static class ResetCatcher extends KickerCommand {
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.RESET_CATCHER;
		}
	}
}
