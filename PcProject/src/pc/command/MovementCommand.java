package pc.command;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Commands for moving the robot forwards, backwards, left, and right.
 * Also includes stopping, kicking and catching.
 */
public abstract class MovementCommand extends RobotCommand{

	//Motor percentage to move with
	private final int power;

	public MovementCommand(int power) {
		this.power = power;
	}

	//Transmit the movement opcode and the power to move with.
	@Override
	public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
		super.send(out, writeSecurityBytes);
		if(power != 0){
			writeInt(out, power);
		}
	}

	@Override
	public String toString() {
		return super.toString()+" "+power;
	}



	//Forwards, backwards, left, and right:

	public static class Forwards extends MovementCommand{
		public Forwards(int power) {
			super(power);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.FORWARDS;
		}
	}

	public static class Backwards extends MovementCommand{
		public Backwards(int power) {
			super(power);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.BACKWARDS;
		}
	}

	public static class RotateLeft extends MovementCommand{
		public RotateLeft(int power) {
			super(power);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.ROTATE_LEFT;
		}
	}

	public static class RotateRight extends MovementCommand{
		public RotateRight(int power) {
			super(power);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.ROTATE_RIGHT;
		}
	}

	//Stopping

	public static class Stop extends MovementCommand{
		public Stop() {
			super(0);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.STOP;
		}
	}
}
