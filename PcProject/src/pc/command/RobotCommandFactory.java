package pc.command;

import java.util.StringTokenizer;

import pc.command.KickerCommand.Catch;
import pc.command.KickerCommand.Kick;
import pc.command.KickerCommand.ResetCatcher;
import pc.command.MovementCommand.Backwards;
import pc.command.MovementCommand.Forwards;
import pc.command.MovementCommand.RotateLeft;
import pc.command.MovementCommand.RotateRight;
import pc.command.MovementCommand.Stop;
import pc.command.MovementTestCommand.BackwardsTest;
import pc.command.MovementTestCommand.ForwardsTest;
import pc.command.MovementTestCommand.RotateLeftTest;
import pc.command.MovementTestCommand.RotateRightTest;
import pc.command.TestCommands.TestBoolean;
import pc.command.TestCommands.TestByte;
import pc.command.TestCommands.TestCommand;
import pc.command.TestCommands.TestFloat;
import pc.command.TestCommands.TestInt;
import pc.command.TestCommands.TestKick;


/**
 * Used for translating textual input strings into
 * commands that can be sent to the robot.
 */
public final class  RobotCommandFactory {

	/**
	 * Try to translate the given string into
	 * a command and its parameters. Allows for
	 * multiple aliases for the same commands.
	 * 
	 * May throw exceptions when trying to parse numbers.
	 */
	public static RobotCommand stringToCommand(String text) throws Exception{
		if(text != null && text.length() > 0){
			StringTokenizer tok = new StringTokenizer(text);
			int numTokens = tok.countTokens();
			System.out.println("\nNum tokens = "+numTokens);

			String firstTok = tok.nextToken();

			if(stringMatches(firstTok, "KICK", "K", "6", "13")){
				return readKickCommand(tok, numTokens);
			}else if(stringMatches(firstTok, "STOP", "S", "1")){
				return readNoArgsCommand(RobotOpcode.STOP, tok, numTokens);
			}else if(stringMatches(firstTok, "FORWARDS", "F", "FORWARD", "2", "9")){
				return readMovementCommand(RobotOpcode.FORWARDS, RobotOpcode.FORWARDS_TEST, tok, numTokens);
			}else if(stringMatches(firstTok, "BACKWARDS", "B", "BACKWARD", "3", "10")){
				return readMovementCommand(RobotOpcode.BACKWARDS, RobotOpcode.BACKWARDS_TEST, tok, numTokens);
			}else if(stringMatches(firstTok, "ROTATE_LEFT", "L", "RL", "ROT_LEFT", "ROTATELEFT", "ROTLEFT", "LEFT", "4", "11")){
				return readMovementCommand(RobotOpcode.ROTATE_LEFT, RobotOpcode.ROTATE_LEFT_TEST, tok, numTokens);
			}else if(stringMatches(firstTok, "ROTATE_RIGHT", "R", "RR", "ROT_RIGHT", "ROTATERIGHT", "ROTRIGHT", "RIGHT", "5", "12")){
				return readMovementCommand(RobotOpcode.ROTATE_RIGHT, RobotOpcode.ROTATE_RIGHT_TEST, tok, numTokens);
			}else if(stringMatches(firstTok, "CATCH", "c", "7")){
				return readNoArgsCommand(RobotOpcode.CATCH, tok, numTokens);
			}else if(stringMatches(firstTok, "RESET_CATCHER", "RESET", "RES", "rc", "8")){
				return readNoArgsCommand(RobotOpcode.RESET_CATCHER, tok, numTokens);
			}else if(stringMatches(firstTok, "TEST", "t", "14")){
				return readNoArgsCommand(RobotOpcode.TEST, tok, numTokens);
			}else if(stringMatches(firstTok, "TESTINT", "15", "TEST_INT", "ti")){
				return readSingleIntegerCommand(RobotOpcode.TEST_INT, tok, numTokens);
			}else if(stringMatches(firstTok, "tf", "TESTFLOAT", "TEST_FLOAT", "TESTDOUBLE","TEST_DOUBLE", "td", "16")){
				return readSingleFloatCommand(RobotOpcode.TEST_FLOAT, tok, numTokens);
			}else if(stringMatches(firstTok, "TEST_BOOL", "TESTBOOL", "BOOL", "18")){
				return readTestBoolCommand(tok, numTokens);
			}else if(stringMatches(firstTok, "TEST_BYTE", "BOOL", "19")){
				return readTestByteCommand(tok, numTokens);
			}else if(stringMatches(firstTok, "h1", "fk", "hybrid", "both", "20")){
				return new MoveAndKickCommand(new Forwards(100), new Kick(100), 250);
			}else if(stringMatches(firstTok, "h2", "fc")){
				return new MoveAndKickCommand(new Forwards(40), new Catch(), 250);
			}
		}
		System.out.println("Invalid Command.");
		return null;
	}

	/**
	 * Parse either a MovementCommand or a MovementTestCommand
	 * depending on the number of arguments supplied.
	 */
	private static RobotCommand readMovementCommand(RobotOpcode singleArgOpcode, RobotOpcode threeArgOpcode, StringTokenizer tok, int numTokens) throws Exception{
		if(numTokens == 2){
			int power = intFromString(tok.nextToken());
			
			switch(singleArgOpcode){
			case FORWARDS : 	return new Forwards(power);
			case BACKWARDS: 	return new Backwards(power);
			case ROTATE_LEFT: 	return new RotateLeft(power);
			case ROTATE_RIGHT: 	return new RotateRight(power);
			default : 			return null;
			}
		}else if(numTokens == 4){
			int leftPower = intFromString(tok.nextToken());
			int rightPower = intFromString(tok.nextToken());
			int delay = intFromString(tok.nextToken());

			switch(threeArgOpcode){
			case FORWARDS_TEST : 		return new ForwardsTest(leftPower, rightPower, delay);
			case BACKWARDS_TEST: 		return new BackwardsTest(leftPower, rightPower, delay);
			case ROTATE_LEFT_TEST: 		return new RotateLeftTest(leftPower, rightPower, delay);
			case ROTATE_RIGHT_TEST: 	return new RotateRightTest(leftPower, rightPower, delay);
			default : 			return null;
			}
		}else{
			System.out.println("Wrong number of arguments for "+singleArgOpcode+". Should be either: "+singleArgOpcode+" <power> or "+threeArgOpcode+" <leftPower> <rightPower> <delay>");
		}
		return null;
	}
	
	
	/**
	 * Read a command that doesn't need any arguments.
	 */
	private static RobotCommand readNoArgsCommand(RobotOpcode opcode, StringTokenizer tok, int numTokens) throws Exception{
		if(numTokens == 1){
			switch(opcode){
			case STOP:				return new Stop();
			case TEST:				return new TestCommand();
			case CATCH:				return new Catch();
			case RESET_CATCHER: 	return new ResetCatcher();
			default:				return null;
			}
		}
		System.out.println(opcode+" does not take extra arguments.");
		return null;
	}

	/**
	 * Read in either a KICK or TEST_KICK command depending on
	 * the number of arguments.
	 */
	private static RobotCommand readKickCommand(StringTokenizer tok, int numTokens) throws Exception{
		if(numTokens == 5){
			int upPower = intFromString(tok.nextToken());
			int downPower = intFromString(tok.nextToken());
			int upDelay = intFromString(tok.nextToken());
			int downDelay = intFromString(tok.nextToken());
			return new TestKick(upPower, downPower, upDelay, downDelay);
		}else if(numTokens == 2){
			return readSingleIntegerCommand(RobotOpcode.KICK, tok, numTokens);
		}else{
			System.out.println("Wrong number of arguments for kick. Should be either: KICK <speed> or KICK <upPower> <downPower> <upDelay> <downDelay>");
		}
		return null;
	}

	private static RobotCommand readSingleIntegerCommand(RobotOpcode opcode, StringTokenizer tok, int numTokens) throws Exception{
		if(numTokens == 2){
			int value= intFromString(tok.nextToken());

			switch(opcode){
			case KICK: 			return new Kick(value);
			case TEST_INT: 		return new TestInt(value);
			default: 			return null;
			}
		}
		System.out.println("Wrong number of arguments for "+opcode+". Should be : "+opcode+" <integer> ");
		return null;
	}
	
	private static RobotCommand readSingleFloatCommand(RobotOpcode opcode, StringTokenizer tok, int numTokens) throws Exception{
		if(numTokens == 2){
			float value= floatFromString(tok.nextToken());

			switch(opcode){
			case TEST_FLOAT: 	return new TestFloat(value);
			default: 			return null;
			}
		}
		System.out.println("Wrong number of arguments for "+opcode+". Should be : "+opcode+" <integer> ");
		return null;
	}

	private static RobotCommand readTestByteCommand(StringTokenizer tok, int numTokens) throws Exception{
		if(numTokens == 2){
			byte value = (byte) intFromString(tok.nextToken());
			return new TestByte(value);
		}
		System.out.println("Wrong number of arguments for TEST_BYTE. Should be TEST_BYTE <byte>");
		return null;
	}

	private static RobotCommand readTestBoolCommand(StringTokenizer tok, int numTokens) {
		if(numTokens == 2){
			boolean value = boolFromString(tok.nextToken());
			return new TestBoolean(value);
		}
		System.out.println("Wrong number of arguments for TEST_BOOL. Should be TEST_BOOL <bool>");
		return null;
	}
	
	public static int intFromString(String s) throws Exception{
		return Integer.parseInt(s);
	}

	public static float floatFromString(String str){
		return Float.parseFloat(str);
	}

	public static boolean boolFromString(String str){
		return str.equals("true") || str.equals("1");
	}
	
	
	/**
	 * Check f the given test string matches any of
	 * the given possible aliases (case insensitive).
	 */
	public static boolean stringMatches(String testString, String ... strings){
		for(String s : strings){
			if(testString.equalsIgnoreCase(s)) return true;
		}
		return false;
	}
}