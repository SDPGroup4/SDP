package pc.command;

import java.io.DataOutputStream;
import java.io.IOException;

/**
 * Commands for testing the communications setup
 * and seeing whether different data types are
 * transmitted and received correctly.
 */
public class TestCommands {

	public static class TestCommand extends RobotCommand {
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.TEST;
		}
	}

	public static class TestFloat extends RobotCommand{
		private final float testValue;
		public TestFloat(float test){
			this.testValue = test;
		}
		@Override
		public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
			super.send(out, writeSecurityBytes);
			writeFloat(out, testValue);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.TEST_FLOAT;
		}

		@Override
		public String toString() {
			return super.toString()+" "+testValue;
		}
	}

	public static class TestInt extends RobotCommand{
		private final int testValue;
		public TestInt(int test){
			this.testValue = test;
		}
		@Override
		public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
			super.send(out, writeSecurityBytes);

			writeInt(out, testValue);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.TEST_INT;
		}
	}

	public static class TestByte extends RobotCommand{
		private final byte testValue;
		public TestByte(byte test){
			this.testValue = test;
		}
		@Override
		public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
			super.send(out, writeSecurityBytes);
			out.write(testValue);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.TEST_BYTE;
		}
	}

	public static class TestBoolean extends RobotCommand{
		private final boolean testValue;
		public TestBoolean(boolean test){
			this.testValue = test;
		}
		@Override
		public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
			super.send(out, writeSecurityBytes);
			writeBoolean(out, testValue);
		}
		@Override
		public RobotOpcode getOpcode() {
			return RobotOpcode.TEST_BOOL;
		}
	}
	
	
	//Test the kicker for consecutive upward then downward swings.
	//Has sepeaate parameters for the motor percentage and time (in milliseconds)
	//to run the motor for.
	public static class TestKick extends RobotCommand{
		private final int upPower;
		private final int downPower;
		private final int upDelay;
		private final int downDelay;
		public TestKick(int upPower, int downPower, int upDelay, int downDelay) {
			super();
			this.upPower = Math.min(upPower, 100);
			this.upDelay = upDelay;
			this.downPower = Math.min(downPower, 100);
			this.downDelay = downDelay;
		}
		@Override
		public void send(DataOutputStream out, boolean writeSecurityBytes) throws IOException {
			super.send(out, writeSecurityBytes);
			writeInt(out, upPower);
			writeInt(out, downPower);
			writeInt(out, upDelay);
			writeInt(out, downDelay);
		}

		@Override
		public String toString() {
			return super.toString()+" "+upPower+" "+downPower+" "+upDelay+" "+downDelay;
		}

		@Override
		public RobotOpcode getOpcode(){
			return RobotOpcode.TEST_KICK;
		}
	}
}
