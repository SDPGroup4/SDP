//A cut back version of the Arduino code from
//milestone 2. Most of the computation is done on the PC-side
//Vision system now, so most of the old commands have been removed.

#include "SDPArduino.h"
#include "WSWire.h"
#include <Servo.h>

/////////////////////////////////////////////////
//         Constants and Variables             //
/////////////////////////////////////////////////

// Motor ports
const int LEFT_MOTOR = 2;
const int RIGHT_MOTOR = 3;
const int KICKER_MOTOR = 0;
Servo servo;

//Timers
long prevTime = 0;
long leftTimer = 0;
long rightTimer = 0;
long kickTimer = 0;

// Byte sizes for variable types
const int DOUBLE_BYTES = 4;
const int INT_BYTES = 2;
const int BOOLEAN_BYTES = 1;

//How long to wait for bytes (milliseconds)
int TIMEOUT = 500;

//How long (milliseconds) to wait before stopping a motor automatically
//Stops the robot going on a rampage if the comms experience high latency.
int AUTO_STOP_DELAY_MS = 300;



/////////////////////////////////////////////////
//             Action Opcodes                  //
/////////////////////////////////////////////////

//Movement. 
//All movement commands take in a single motor percentage (except STOP, which has no args).
const int STOP = 1;
const int FORWARDS = 2;
const int BACKWARDS = 3;
const int ROTATE_LEFT = 4;
const int ROTATE_RIGHT = 5;

//Kicking and Catching.
//CATCH and RESET catcher have no args. KICK takes in a kicker motor percentage.
const int KICK = 6;
const int CATCH = 7;
const int RESET_CATCHER = 8;

//Used for testing and calibrating motors.
//Should not be called from strategy code.
//All take in two motor percentages (seperate left and right percentages)
//and a delay in milliseconds (how long to run the motors for).
const int FORWARDS_TEST = 9;
const int BACKWARDS_TEST = 10;
const int ROTATE_LEFT_TEST = 11;
const int ROTATE_RIGHT_TEST = 12;
const int TEST_KICK = 13;

//A hybrid move indicating the use of both a movement and a kicker opcode, 
//along with an integer for the movement delay.
const int MOVE_AND_KICKER = 20;

//Reactive incrementation of motor powers:
//If opcode and power of a movement command remains the same after a timer has expired,
//increase the motor power for the operation.
int opcodeCache = 0;
int opcodeCurrent = 0;
int sentPowerCache = 0;
int actualPowerCache = 0;
long powerTimer = -1;
boolean leftMotorCache = true;
boolean rightMotorCache = true;
int powerIncreaseCount = 1;

/////////////////////////////////////////////////
//     Initialization and Main Loop            //
/////////////////////////////////////////////////


//Initialize the wireless pins and motors using Gary's test code
//in SDPArduino.cpp.
void setup() {
  SDPsetup();
  helloWorld();
  delay(2000);
  Serial.flush();
  prevTime = millis();
  servo.attach(6, 600, 2400);
}

//Every step, check whether any commands have been received, and respond to them.
//Has passcode before every valid command to act as a spam-filter.
void loop() {
  //Update non-blocking timer mechanism to avoid using delay().
  doTimers();
  
  //Read through the buffer until a valid passcode has been received in full.
  if (Serial.available() > 0) {
    
    byte sec1 = readByte();
    //Serial.print("BYTE NUMBER1: ");
    //Serial.println(sec1);
    if(sec1 == 56){
      byte sec2 = readByte();
      //Serial.print("BYTE NUMBER2: ");
      //Serial.println(sec2);
      if(sec2 == 42){
        byte sec3 = readByte();
       // Serial.print("BYTE NUMBER3: ");
        //Serial.println(sec3);
        if(sec3 == 91){
          
           //This is now the start of a valid command, so read in the opcode,
           //acknowledge it, and work out how to respond to it.
           byte nextOpCode = readByte();

           acknowledge(nextOpCode);
           
           //Serial.print("OPCODE: ");
           //Serial.println(nextOpCode);
           
           handleOpCode(nextOpCode);
           
           switchOffAfter(LEFT_MOTOR, AUTO_STOP_DELAY_MS);
           switchOffAfter(RIGHT_MOTOR, AUTO_STOP_DELAY_MS);
        }
      }
    }
  }
}

//How to handle each of the possible actions.
void handleOpCode(int opCode) {

  
  //Cache the current opcode
  opcodeCache = opcodeCurrent;
  opcodeCurrent = opCode;
  
  
  if (opcodeCache != opcodeCurrent){
   powerTimer = -1; 
  }
  
  switch (opCode) {
    //Movement
    case STOP:              handleStop();                                       break;
    case FORWARDS:          handleMovementCommandPowerOnly(true, true);         break;
    case BACKWARDS:         handleMovementCommandPowerOnly(false, false);       break;
    case ROTATE_LEFT:       handleMovementCommandPowerOnly(false, true);        break;
    case ROTATE_RIGHT:      handleMovementCommandPowerOnly(true, false);        break;
    
    //Kicking and catching
    case KICK:              handleKick();                                       break;
    case CATCH:             handleCatch();                                      break;
    case RESET_CATCHER:     handleResetCatcher();                               break;
    
    //Testing movement
    case FORWARDS_TEST:     handleMovementCommandPowersAndTime(true, true);     break;
    case BACKWARDS_TEST:    handleMovementCommandPowersAndTime(false, false);   break;
    case ROTATE_LEFT_TEST:  handleMovementCommandPowersAndTime(false, true);    break;
    case ROTATE_RIGHT_TEST: handleMovementCommandPowersAndTime(true, false);    break;
    case TEST_KICK:         handleTestKick();                                   break;
    
    case MOVE_AND_KICKER:   handleMoveAndKick();                                break;
   }
}

/////////////////////////////////////////////////
//         Handling Movement Commands          //
/////////////////////////////////////////////////

//Stop the wheels (but not the kicker).
//No arguments.
void handleStop() {
  motorStop(LEFT_MOTOR);
  motorStop(RIGHT_MOTOR);
}

//Set the left and right wheels to the motor percentage provided.
//Using booleans to indicate which directions to run the wheels in
//allows a single movement command to be used for forwards, backwards, left, and right.
//Takes in a single integer argument, which is the power level (percentage) for both motors.
void handleMovementCommandPowerOnly(boolean leftForwards, boolean rightForwards){
  int motorPercentage = readInt();
  
    
  //Check previous opcode against current one;
  //if the opcode is the same, and the power increment timer is off, start the timer.
  //For turning operations (opcode 4 and 5), set timer to 500 milliseconds;
  //for movement operations, set timer to 3 seconds.
  boolean isNewRotation = false;
  if ((opcodeCache == opcodeCurrent)
      &&(motorPercentage == sentPowerCache)){
    if (powerTimer < 0){
       if ((opcodeCurrent == ROTATE_LEFT)||(opcodeCurrent == ROTATE_RIGHT)){
         powerTimer = min(1250, powerIncreaseCount * 250);
         powerIncreaseCount += 1;
       }else {
         powerTimer = powerIncreaseCount * 2500;
         powerIncreaseCount += 1;
       }
    }
  } else {
    sentPowerCache = motorPercentage;
    actualPowerCache = motorPercentage;

    powerTimer = -1;
    powerIncreaseCount = 1;
    
    //Artificially boost right hand turns to cope with different motor powers.
    if((opcodeCurrent == ROTATE_RIGHT)){
      actualPowerCache += 5;
    }
    
    if((opcodeCurrent == ROTATE_LEFT)||(opcodeCurrent == ROTATE_RIGHT)){
      isNewRotation = true;
    }
  }   
 

  //Cache current motor power and directions
  leftMotorCache = leftForwards;
  rightMotorCache = rightForwards;
  
  //Serial.print("Power");
  //Serial.println(motorPercentage);
  

  if(isNewRotation){
    setLeftPower(leftForwards, 80);
    setRightPower(rightForwards, 80);
    //actualPowerCache -= 5;
    powerTimer = 100;
  }else{
    setLeftPower(leftForwards, actualPowerCache);
    setRightPower(rightForwards, actualPowerCache);
  }
  Serial.print("\nMove ");
  Serial.print(leftForwards);
  Serial.print(rightForwards);
  Serial.print(" power ");
  Serial.print(actualPowerCache);
}

//Used for testing and calibrating motors.
//Set the left and right wheels to the motor percentages provided,
//and start timers to switch them off after the given delay (non-blocking).
//Using booleans to indicate which directions to run the wheels in
//allows a single movement command to be used for forwards, backwards, left, and right.
//Takes in a three arguments: a left percentage, right percentage, and delay in milliseconds,
//which determines how long to run the motors for.
void handleMovementCommandPowersAndTime(boolean leftForwards, boolean rightForwards){
  //Read in the arguments
  int leftPercentage = readInt();
  int rightPercentage = readInt();
  int delayTimeMillis = readInt();
  
  //Move the wheels forwards or backwards
  setLeftPower(leftForwards, leftPercentage);
  setRightPower(rightForwards, rightPercentage);

  //Leave the motors running for delayTime, and then stop them.
  switchOffAfter(LEFT_MOTOR, delayTimeMillis);
  switchOffAfter(RIGHT_MOTOR, delayTimeMillis);
}

//Set the left motor to the given percentage.
//Allows a single point to tweak percentages for differing
//motor powers, or to reverse the direction if it is wired in backwards.
void setLeftPower(boolean forwards, int power){
  if(!forwards){
    motorForward(LEFT_MOTOR, power);
  }else{
    motorBackward(LEFT_MOTOR, power);
  }
}

//Set the right motor to the given percentage.
//Allows a single point to tweak percentages for differing
//motor powers, or to reverse the direction if it is wired in backwards.
void setRightPower(boolean forwards, int power){
  if(!forwards){
    motorForward(RIGHT_MOTOR, power);
  }else{
    motorBackward(RIGHT_MOTOR, power);
  }
}



/////////////////////////////////////////////////
//      Handling Kicking and Catching          //
/////////////////////////////////////////////////

//Kick with the given speed (integer motor percentage). Allows adjustment for
//different kick strengths.
void handleKick(){
  //int kickSpeed =  readInt();  
  //motorForward(KICKER_MOTOR, kickSpeed);
  //switchOffAfter(KICKER_MOTOR, 300);
  servo.write(53);
}

//Bring the catcher down over the ball.
//Assumes it was already raised. 
//No arguments.
void handleCatch() {
  //motorBackward(KICKER_MOTOR, 50);
  //switchOffAfter(KICKER_MOTOR, 500); // initial was 300
  servo.write(-20);
}

// Move kicker to up position, ready to catch the ball.
//Assumes it was down when called.
//No arguments.
void handleResetCatcher() {
  //motorForward(KICKER_MOTOR, 50);
  //switchOffAfter(KICKER_MOTOR, 1000); // initial is 400
  servo.write(45);
}

//Used for testing the kicker motors.
//Has 4 arguments - upPower, downPower, upTime, downTime
//This allows the kicker to be raised and lowered in a
//customizable manor. May need rewrtitten for new kicker mechanism.
//Uses blocking timers, so should not be used outside of testing.
void handleTestKick() {  
  int kickUpPower = readInt();    // Power for kicker to move to up position
  int kickDownPower = readInt();  // ... power for down position
  int delayUpTime = readInt();    // Time to switch the motor on for on the upward swing.
  int delayDownTime = readInt();  // ... and the downward swing.

  if (kickUpPower > 100) { // Range is 0-100%
    kickUpPower = 100;
  }
  if (kickDownPower > 100) {
    kickDownPower = 100;
  }

  //motorForward(KICKER_MOTOR, kickUpPower);
  //delay(delayUpTime);
  //motorBackward(KICKER_MOTOR, kickDownPower);
  //delay(delayDownTime);
  //motorStop(KICKER_MOTOR);
  servo.write(53);
  delay(2000);
  servo.write(0);
}

void handleMoveAndKick(){
  byte movOpcode = readByte();
  Serial.print("\nMovOp = ");
  Serial.print(movOpcode);
  handleOpCode(movOpcode);
  
  byte kickerOpcode = readByte();
  Serial.print("\nKickOp = ");
  Serial.print(kickerOpcode);
  handleOpCode(kickerOpcode);
  
  int delayTime = readInt();
  Serial.print("\nDelay = ");
  Serial.print(delayTime);
  switchOffAfter(LEFT_MOTOR, delayTime);
  switchOffAfter(RIGHT_MOTOR, delayTime);
}


/////////////////////////////////////////////////
//     Reading in Data from the Serial Port    //
/////////////////////////////////////////////////

//Wait for a given number of bytes to appear in the serial buffer,
//but time out if it is taking too long (to avoid getting stuck in an infinite wait loop).
boolean waitForBytes(int numBytes){
  int timer = 0;
  while(Serial.available() < numBytes && timer < TIMEOUT){
    delay(1);
    timer++;
  }
  boolean finished = Serial.available() >= numBytes;
  return finished;
}

//Read in a single byte (waiting for time-out if necessary).
byte readByte() { 
  if(waitForBytes(1)){
    return Serial.read();
  }
  return 0;
}

//Read a single 2 byte integer from the serial port (waiting for time-out if necessary).
int readInt() { 
  if(waitForBytes(INT_BYTES)){
    int lhs = Serial.read();
    int rhs = Serial.read();
    int result = (lhs << 8) | rhs ;
    return result;
  }
  return 0;
}


/////////////////////////////////////////////////
//        Non-Blocking Timers                  //
/////////////////////////////////////////////////

//Update all the timers each frame. If any of them reach zero, then
//stop the motor it corresponds to. This allows a very simple
//non-blocking timer mechanism to avoid calling delay().
//This must be called every loop().
void doTimers(){
  unsigned long currentTime = millis();
  long timeDelta = currentTime - prevTime;
  
  prevTime = currentTime;
  
  //Update the left timer
  if(leftTimer > 0){
  if(leftTimer > timeDelta){
    leftTimer -= timeDelta;
  }else{
    leftTimer = -1;
    powerTimer = -1;
    motorStop(LEFT_MOTOR);
  }
  }
  
  //Update the right timer
  if(rightTimer > 0){
  if(rightTimer > timeDelta){
    rightTimer -= timeDelta;
  }else{
    rightTimer = -1;
    powerTimer = -1;
    motorStop(RIGHT_MOTOR);
  }
  }
  
  //Update the kicker timer
  if(kickTimer > timeDelta){
  if(kickTimer > timeDelta){
    kickTimer -= timeDelta;
  }else{
    kickTimer = -1;
    motorStop(KICKER_MOTOR);
  }
  }
  
  //Update motor power timer
  // and trigger the increment of motor power
  if(powerTimer > 0){
  if(powerTimer > timeDelta){
    powerTimer -= timeDelta;
  }else{
    powerTimer = -1;
    increaseMotorPower(leftMotorCache,rightMotorCache);
    Serial.print("Power incremented");
    Serial.println(actualPowerCache);
  }
  }
}

//Start a non-blocking timer that will switch off the
//specified motor after the given number of milliseconds.
void switchOffAfter(int motorNumber, int timeMs){
  switch(motorNumber){
  case LEFT_MOTOR : leftTimer = timeMs; break;
  case RIGHT_MOTOR : rightTimer = timeMs; break;
  case KICKER_MOTOR : kickTimer = timeMs; break;
  }
} 

//Start a non-blocking timer to increase the motor power by 5
//of movement commands after the given number of milliseconds.
void increaseMotorPower(boolean leftForwards, boolean rightForwards){
  //Do not actually increment the power yet.
  //actualPowerCache = actualPowerCache + 5;
  setLeftPower(leftForwards, actualPowerCache);
  setRightPower(rightForwards, actualPowerCache);
}

/////////////////////////////////////////////////
//    Acknowledging Receipt of Commands        //
/////////////////////////////////////////////////

// Send acknowledgement of the given opcode with security code 56,42,91
void acknowledge(byte opcodeToAcknowledge) {
  byte reply[4];
  reply[0] = 56;
  reply[1] = 42;
  reply[2] = 91;
  reply[3] = opcodeToAcknowledge;
  
  //Not sure what this is meant to do. I don't think it's needed, but uncomment it if it is...
  //while (Serial.available() % 4 != 0);
  
  Serial.write(reply, 4);
}
