//The simplest possible echo client.

#include "SDPArduino.h"
#include "WSWire.h"

// TODO Allow variable power
// TODO Get ratio for distance to move x degrees
// TODO Allow robot speed to be set, find a way to read in a number

int nextOpCode = 0;
int power = 50;
boolean kickerIsDown = true;
const int leftMotor = 3, rightMotor = 2, kickerMotor = 0;


//From Group 9's file RobotOpcode in the SharedLib folder.
//Implementations of methods are found in the BrickCommClient.java file in the BrickProjet folder.
const int STOP = 1;
const int FORWARDS = 2;
const int BACKWARDS = 3;
const int ROTATE_LEFT = 4;
const int ROTATE_RIGHT = 5;
const int ROTATE_BY = 6;
const int TRAVEL = 7;
const int ARC_FORWARDS = 8;
const int KICK = 9;
const int QUIT = 10;
const int CATCH = 11;
const int RESET_CATCHER = 12;
const int TEST = 14;
const int TESTINT = 15;
const int TESTDOUBLE = 16;
const int TESTINTANDDOUBLE = 17;
boolean movingForwards = false;
boolean movingBackwards = false;
int turnRadius = 0;

void setup() {
  /*pinMode(8, OUTPUT);       // initialize pin 8 to control the radio
  digitalWrite(8, HIGH);    // select the radio
  Serial.begin(115200);     // start the serial port at 115200 baud (correct for XinoRF and RFu, if using XRF + Arduino you might need 9600)
  Serial.print("STARTED");  // transmit started packet?*/
  SDPsetup();
  helloWorld();
}

void loop() { // Repeatedly called
  if (Serial.available() > 0) {
    nextOpCode = Serial.read();
    handleOpCode(nextOpCode);
  }
}

void handleOpCode(int opCode) {
  switch (opCode) {
    case STOP:
      handleStop();
      break;
    case FORWARDS:
      handleForwards();
      break;
    case BACKWARDS:
      handleBackwards();
      break;
    case KICK:
      handleKick();
      break;
    case ROTATE_LEFT:
      handleRotate(false);
      break;
    case ROTATE_RIGHT:
      handleRotate(true);
      break;
    case ARC_FORWARDS:
      handleArcForwards();
      break;
    case ROTATE_BY:
      handleRotateBy();
      break;
    case TRAVEL:
      handleTravel();
      break;
    case CATCH:
      handleCatch();
      break;
    case RESET_CATCHER:
      handleResetCatcher();
      break;
    case TEST:
      handleTest();
      break;
    case TESTINT:
      handleTestINT();
      break;
    case TESTDOUBLE:  handleForwards();
      handleTestDOUBLE();
      break;
    case TESTINTANDDOUBLE:
      handleTestINTANDDOUBLE();
      break;
    case QUIT:
      return;
    default:
      Serial.print("Unknown opcode: ");
      Serial.print(opCode);
  }
}

double readDouble() { // readDouble from input
  return (double) Serial.read();

}

double readInt() { // readInt from input
  return (int) Serial.read();

}

double readBoolean() { // readBoolean from input
  return (boolean) Serial.read();

}

void handleStop() {
  motorAllStop();
  movingForwards = false;
  movingBackwards = false;
  Serial.print("STOP");

}

void handleForwards() {
  while(Serial.available() == 0) {}  // wait until parameter is recognised
  int forwardRightPower = Serial.read();
  while(Serial.available() == 0) {}
  int forwardLeftPower = Serial.read();
  while(Serial.available() == 0) {}
  int delayTimeLHS = Serial.read();  // time for kicker to wait in up position
  while(Serial.available() == 0) {}
  int delayTimeRHS = Serial.read();  // time for kicker to wait in up position
  int delayTime = (delayTimeLHS << 8) + delayTimeRHS;
  Serial.print("Success! ForwardPower set!");
  motorForward(leftMotor, forwardLeftPower); // Motors forward, forwardPower% Power
  motorForward(rightMotor, forwardRightPower);
  delay(delayTime);
  motorStop(leftMotor);
  motorStop(rightMotor);
  movingForwards = true;
  Serial.print("FORWARDS");

}

void handleBackwards() {
  while(Serial.available() == 0) {}
  int backwardRightPower = Serial.read();
  while(Serial.available() == 0) {}
  int backwardLeftPower = Serial.read();
  while(Serial.available() == 0) {}
  int delayTimeLHS = Serial.read();  // time for kicker to wait in up position
  while(Serial.available() == 0) {}
  int delayTimeRHS = Serial.read();  // time for kicker to wait in up position
  int delayTime = (delayTimeLHS << 8) + delayTimeRHS;
  Serial.print("Success! BackwardPower set!");
  motorBackward(leftMotor, backwardRightPower); // Motors backwards, backwardPower% Power
  motorBackward(rightMotor, backwardLeftPower);
  delay(delayTime);
  motorStop(leftMotor);
  motorStop(rightMotor);
  movingBackwards = true;
  Serial.print("BACKWARDS");
}

void handleRotate(boolean clockwise) {
  if (clockwise) {
    motorForward(leftMotor, 100);
    motorBackward(rightMotor, 100);
    Serial.print("ROTATE_RIGHT");
  } else {
    motorForward(rightMotor, 100);
    motorBackward(leftMotor, 100);
    Serial.print("ROTATE_LEFT");
  }
}

void handleRotateBy() {
  int turnRadius = readInt();
  double rotatespeed = readDouble(); // power affect speed
  // rotate(angle, immediateReturn); we need to implement this as last year uses NXTRegulatedMotor we do not
  // maybe use a var as a timer to tell the rotation ends
  Serial.print("ROTATE_BY");

}

void handleTravel() {
  // What should this method do?
  // probaboly let motor travel a certain speed to a certain distance
  // if so
  int speedToTravel = readInt();
  int distance = readInt();
  double time = (double) distance / speedToTravel;
  // need loop to count down motorForward(rightMotor, 50);
  Serial.print("TRAVEL");
}

void handleArcForwards() {
  double radius = 0;
  int distance = 0;
  int power = 0;
  motorForward(leftMotor, power);
  Serial.print("ARC_FORWARDS");
}

void handleKick() {
  if (!kickerIsDown) {
    return;	// kicker in wrong position, abort kick call
  }

  if (power > 100) { // Max speed is 100
    power = 100;
  }

  /*motorForward(kickerMotor, power);
  delay(500);        // Change to timer instead of delay
  motorStop(kickerMotor);*/
  while(Serial.available() == 0) {}
  int kickUpPower = Serial.read();   // power for kicker to move to up position
  while(Serial.available() == 0) {}
  int kickDownPower = Serial.read();  // ... power for down position
  while(Serial.available() == 0) {}
  int delayUpTimeLHS = Serial.read();  // time for kicker to wait in up position
  while(Serial.available() == 0) {}
  int delayUpTimeRHS = Serial.read();  // time for kicker to wait in up position
  int delayUpTime = (delayUpTimeLHS << 8) + delayUpTimeRHS;

  while(Serial.available() == 0) {}
  int delayDownTimeLHS = Serial.read();  // time for kicker to wait in up position
  while(Serial.available() == 0) {}
  int delayDownTimeRHS = Serial.read();  // time for kicker to wait in up position
  int delayDownTime = (delayDownTimeLHS << 8) + delayDownTimeRHS;
  
  // time for kicker to wait in down position
  Serial.print("Success! KickPower set!");
  if (kickUpPower > 100) {
    kickUpPower = 100;
  }
  if (kickDownPower > 100) {
    kickDownPower = 100;
  }

  motorForward(kickerMotor, kickUpPower);
  delay(delayUpTime);
  motorBackward(kickerMotor, kickDownPower);
  delay(delayDownTime);
  motorStop(kickerMotor);

  //kickerIsDown = false;
  Serial.print("KICK");
}

void handleQuit() {
  // Shut off arduino, close connection
  digitalWrite(8, LOW);
  Serial.print("QUIT");
  exit(0);
}

void handleCatch() {
  // Move kicker to down position, catching the ball
  if (kickerIsDown) {
    return; // kicker in wrong position
  }
  motorBackward(kickerMotor, 25);
  kickerIsDown = true;

  Serial.print("CATCH");
}

void handleResetCatcher() {
  // Move kicker to up position
  if (!kickerIsDown) {
    return;  // kicker in wrong position
  }
  motorForward(kickerMotor, 25);
  kickerIsDown = false;

  Serial.print("RESET_CATCHER");
}

void handleTest() {
  Serial.print("TEST");
}

void handleTestINT() {
  Serial.print("TESTINT");
}

void handleTestDOUBLE() {
  Serial.print("TESTDOUBLE");
}

void handleTestINTANDDOUBLE() {
  Serial.print("TESTINTANDDOUBLE");
}


