//The simplest possible echo client.

char nextChar = 'n';

void setup(){
    pinMode(8, OUTPUT);       // initialize pin 8 to control the radio
    digitalWrite(8, HIGH);    // select the radio
    Serial.begin(115200);     // start the serial port at 115200 baud (correct for XinoRF and RFu, if using XRF + Arduino you might need 9600)
    Serial.print("STARTED");  // transmit started packet
}

void loop(){ // Repeatedly called
	if(Serial.available() > 0){
        nextChar = (char) Serial.read();
        Serial.print(nextChar);
    }
}
