//The simplest possible echo client.

#include "SDPArduino.h"
#include "WSWire.h"

// TODO handleRotate needs filled in, check if a global var should be used for boolean clockwise


// Motor ports
const int LEFT_MOTOR = 2;
const int RIGHT_MOTOR = 3;
const int KICKER_MOTOR = 0;

long prevTime = 0;
long leftTimer = 0;
long rightTimer = 0;
long kickTimer = 0;

const int WHEEL_SPACING_MM = 160;
const int HALF_WHEEL_SPACING_MM = WHEEL_SPACING_MM / 2;

// Byte sizes for variable types
const int DOUBLE_BYTES = 4;
const int INT_BYTES = 2;
const int BOOLEAN_BYTES = 1;

//How long to wait for bytes (milliseconds)
int TIMEOUT = 1000;

//Adapted from Group 9's RobotOpcodes. The PC-side list is in "PcProject > src > pc.command > RobotOpcode"
//Implementations of methods from group 9 are found in the BrickCommClient.java file in the BrickProjet folder.

//Unused in strategy, but useful for testing.
const int STOP = 1;
const int FORWARDS = 2;
const int BACKWARDS = 3;
const int ROTATE_LEFT = 4;
const int ROTATE_RIGHT = 5;

//Used in strategy
const int ROTATE_BY = 6;
const int TRAVEL = 7;
const int ARC_FORWARDS = 8;
const int KICK = 9;
const int QUIT = 10;
const int CATCH = 11;
const int RESET_CATCHER = 12;

//Testing of communications
const int TEST = 14;
const int TEST_INT = 15;
const int TEST_DOUBLE = 16;
const int TEST_INT_AND_DOUBLE = 17;
const int TEST_BOOL = 18;
const int TEST_BYTE = 19;
const int TEST_KICK = 20;

//Robot state. Not used yet.
boolean kickerIsDown = false;
boolean movingForwards = false;
boolean movingBackwards = false;
int turnRadius = 0;


//Initialize the wireless pins and motors using Gary's test code
//Also flushes starting serial buffer, and prints some messages to signal that the robot is on.
void setup() {
  SDPsetup();
  helloWorld();
  delay(2000);
  Serial.flush();
  Serial.print("\nReady to receive. ");
  prevTime = millis();
}

//Every step, check whether any commands have been received, and respond to them.
void loop() {
  doTimers();
  if (Serial.available() > 0) {
    byte sec1 = readByte();
    if(sec1 == 56){
      byte sec2 = readByte();
      if(sec2 == 42){
        byte sec3 = readByte();
        if(sec3 == 91){
           byte nextOpCode = readByte();
           handleOpCode(nextOpCode);
        }
      }
    }
  }
}

//How to handle each of the possible actions.
void handleOpCode(int opCode) {
  switch (opCode) {
    case STOP:         handleStop();         break;
    case FORWARDS:     handleForwards();     break;
    case BACKWARDS:    handleBackwards();    break;
    case KICK:         handleKick();         break;
    case ROTATE_LEFT:  handleRotate(false);  break;
    case ROTATE_RIGHT: handleRotate(true);   break;
    
    case ARC_FORWARDS: handleArcForwards();  break;
    case ROTATE_BY:    handleRotateBy();     break;
    case TRAVEL:       handleTravel();       break;
    case CATCH:        handleCatch();        break;
    case RESET_CATCHER:handleResetCatcher(); break;
    
    case TEST:         handleTest();         break;
    case TEST_INT:     handleTestINT();      break;
    case TEST_INT_AND_DOUBLE: handleTest();  break;
    case TEST_DOUBLE:  handleTestDOUBLE();   break;
    case TEST_BOOL:    handleTestBOOL();     break;
    case TEST_BYTE:    handleTestBYTE();     break;
    case TEST_KICK:    handleTestKICK();     break;
    case QUIT:         return;
    default:
      Serial.print("\nUnknown opcode: ");
      Serial.print(opCode);
      Serial.print("\n\n");
  }
}

void handleStop() {
  motorAllStop();
  movingForwards = false;
  movingBackwards = false;
  
  Serial.print("\nSTOP");

}

void handleForwards() {  
  Serial.print("\nFORWARDS");
  handleMovementCommand(true, true);
}

void handleBackwards() {
  Serial.print("\nBACKWARDS");
  handleMovementCommand(false, false);
}

void handleRotate(boolean clockwise) {
  if (clockwise) {
    Serial.print("\nROTATE_RIGHT");
    handleMovementCommand(false, true);
  } else {
    Serial.print("\nROTATE_LEFT");
    handleMovementCommand(true, false);
  }
}

void handleMovementCommand(boolean leftForwards, boolean rightForwards){
  //Read in the arguments
  int leftPower = readInt();
  int rightPower = readInt();
  int delayTime = readInt();
  
  //Display the arguments for debugging
  Serial.print("\n Left: ");
  Serial.print(leftPower);

  Serial.print("\nRight: ");
  Serial.print(rightPower);
  
  Serial.print("\n Delay: ");
  Serial.print(delayTime);
  
  Serial.print("\n\n");
  
  
  //Move the wheels forwards or backwards
  if(!leftForwards){
    motorForward(LEFT_MOTOR, leftPower);
  }else{
    motorBackward(LEFT_MOTOR, leftPower);
  }
  
  if(!rightForwards){
    motorForward(RIGHT_MOTOR, rightPower);
  }else{
    motorBackward(RIGHT_MOTOR, rightPower);
  }

  //Leave the motors running for delayTime, and then stop them.
  switchOffAfter(LEFT_MOTOR, delayTime);
  switchOffAfter(RIGHT_MOTOR, delayTime);
}


void handleRotateBy() {
  // some advanced implementation, disregard of speed, non linear, power is at 60%, the accuracy of the angle is between +/- 10 degrees, assume tested at the full battery
  int turnAngle = readInt();
  float rotateSpeed = readFloat();
  boolean unused = readBoolean();
  float delayAmount;
  
  //delayAmount = abs((double)((690 / 90) * turnAngle)); // from basic testing: 690ms / 90 degree at full power
  
  if (turnAngle < 0){  // rotate left
     turnAngle = turnAngle * -1;
     if (turnAngle > 0 && turnAngle < 4){ // exclusive of 0
       delayAmount = 139;
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 4 && turnAngle < 7){
       delayAmount = 150;
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 7 && turnAngle < 10){
       delayAmount = 200;
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 10 && turnAngle < 30){
       delayAmount = abs((double) ((320 / 21.399999999999999) * turnAngle));
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 30 && turnAngle < 60){
       delayAmount = abs((double) ((525 / 37.877777777777782) * turnAngle));
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 60 && turnAngle < 90){
       delayAmount = abs((double) (0.9 * (690 / 90) * turnAngle)); // further test needed
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 90 && turnAngle < 120){
       delayAmount = abs((double) (0.8 * (690 / 90) * turnAngle)); // further test needed
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 120 && turnAngle < 150){
       delayAmount = abs((double) (0.72 * (690 / 90) * turnAngle)); // further test needed
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
     else if (turnAngle >= 150 && turnAngle <= 180){ // inclusive of -180
       delayAmount = abs((double) (0.65 * (690 / 90) * turnAngle));
       motorForward(LEFT_MOTOR, 60);
       motorBackward(RIGHT_MOTOR, 60);
     }
  } 
  else {  // rotate right
    if (turnAngle >= 0 && turnAngle < 4){ // inclusive of 0
       delayAmount = 139;
       motorBackward(LEFT_MOTOR, 60);
       motorForward(RIGHT_MOTOR, 60); 
    }
    else if (turnAngle >= 4 && turnAngle < 7){
       delayAmount = 150;
       motorBackward(LEFT_MOTOR, 60);
       motorForward(RIGHT_MOTOR, 60);
    }
    else if (turnAngle >= 7 && turnAngle < 10){
     delayAmount = 200;
     motorBackward(LEFT_MOTOR, 60);
     motorForward(RIGHT_MOTOR, 60);
    }
    else if (turnAngle >= 10 && turnAngle < 30){
     delayAmount = abs((double) ((320 / 21.399999999999999) * turnAngle));
     motorBackward(LEFT_MOTOR, 60);
     motorForward(RIGHT_MOTOR, 60);
    }
    else if (turnAngle >= 30 && turnAngle < 60){
     delayAmount = abs((double) ((525 / 37.877777777777782) * turnAngle));
     motorBackward(LEFT_MOTOR, 60);
     motorForward(RIGHT_MOTOR, 60);
    }
    else if (turnAngle >= 60 && turnAngle < 90){
     delayAmount = abs((double) (0.9 * (690 / 90) * turnAngle)); // further test needed
     motorBackward(LEFT_MOTOR, 60);
     motorForward(RIGHT_MOTOR, 60);
    }
    else if (turnAngle >= 90 && turnAngle < 120){
     delayAmount = abs((double) (0.8 * (690 / 90) * turnAngle)); // further test needed
     motorBackward(LEFT_MOTOR, 60);
     motorForward(RIGHT_MOTOR, 60);
    }
    else if (turnAngle >= 120 && turnAngle < 150){
     delayAmount = abs((double) (0.72 * (690 / 90) * turnAngle)); // further test needed
     motorBackward(LEFT_MOTOR, 60);
     motorForward(RIGHT_MOTOR, 60);
    }
    else if (turnAngle >= 150 && turnAngle <= 180){ // // inclusive of 180
     delayAmount = abs((double) (0.65 * (690 / 90) * turnAngle));
     motorBackward(LEFT_MOTOR, 60);
     motorForward(RIGHT_MOTOR, 60);
    }
  }
   
  /*Serial.print("\nDelayAmount is ");
  Serial.print(delayAmount);
  int delayInt = (int) delayAmount;
  Serial.print("As int:");
  Serial.print(delayInt);
  Serial.print("\nROTATE_BY ");
  Serial.print(turnAngle); */
  
  switchOffAfter(LEFT_MOTOR,(int) delayAmount);
  switchOffAfter(RIGHT_MOTOR,(int) delayAmount); // convert the float to int
  
  Serial.print("\nDelayAmount is ");
  Serial.print(delayAmount);
  Serial.print("\ndelayInt");
  Serial.print((int) delayAmount);
  Serial.print("\nROTATE_BY ");
  Serial.print(turnAngle);

}

  
  // old implementation
  /* turnAngles less than 0, or greater than 180 -> turn left; otherwise turn right.
  int turnAngle = readInt();
  float rotateSpeed = readFloat();
  float delayAmount;
  int delayCoeff = 10000;
  delayAmount = (double)(turnAngle / rotateSpeed) * delayCoeff;
  Serial.print("\nROTATE_SPEED: ");
  Serial.print(rotateSpeed);
  
  if ( (turnAngle < 0) | (turnAngle > 180)){  // rotate left
    if (turnAngle < 0){ // convert negative angle to positive;
      turnAngle = turnAngle * -1;
      motorForward(LEFT_MOTOR, rotateSpeed);
      motorBackward(RIGHT_MOTOR, rotateSpeed);
      // delay = (turn angle / rotatespeed) * some coefficient.
    }
    else {
      turnAngle = (360 - turnAngle);
      motorForward(LEFT_MOTOR, rotateSpeed);
      motorBackward(RIGHT_MOTOR, rotateSpeed);
    }
  } else {  // rotate right
    motorBackward(LEFT_MOTOR, rotateSpeed);
    motorForward(RIGHT_MOTOR, rotateSpeed);
  }
  delay(delayAmount);
  motorAllStop();
  Serial.print("\nROTATE_BY ");
  Serial.print(turnAngle);
} */

// All distances are in MILLIMETERS
//travel(distance, speed)
void handleTravel() {
  Serial.print("\nTRAVEL");
  // Set travel speed and distance then move

  int distance = readInt();  // positive value moves forwards, negative moves back
  int speedToTravel = readInt();
  boolean isForward = (distance > 0);
  int timeMs = getTimeFromDistance(distance, isForward, 60);
  
  Serial.print("\n Distance = ");
  Serial.print(distance);
  Serial.print("\n Motor Power = 60");  
  Serial.print("\n TimeMs = ");
  Serial.print(timeMs);
  
  if (distance > 0){
    leftForwards(timeMs, speedToTravel);
    rightForwards(timeMs, speedToTravel);
  }else{
    leftBackwards(timeMs, speedToTravel);
    rightBackwards(timeMs, speedToTravel);
  }
  
  
  switchOffAfter(LEFT_MOTOR, timeMs);
  switchOffAfter(RIGHT_MOTOR, timeMs);
}


// Actually handles all arc patterns, forwards left, forwards right, backwards left, backwards right
//arc(radius, distance, 
void handleArcForwards() {
  Serial.print("\nARC");
  float radius = readFloat();  // positive arcs left, negative arcs right
  
  if(radius < HALF_WHEEL_SPACING_MM+1 && radius > 0){
    radius = HALF_WHEEL_SPACING_MM+1;
  }else if(radius > HALF_WHEEL_SPACING_MM-1 && radius < 0){
    radius = -HALF_WHEEL_SPACING_MM-1;
  }

  int distance = readInt();    // positive arcs forwards, negative arcs backwards
  int speedToTravel = readInt(); 
  
  //Calculate how long to run both wheels to get the desired distance at the desired speed.
  boolean isForward = (distance > 0);
  int time = getTimeFromDistance(distance, isForward, 60);

  Serial.print("\nDistance");
  Serial.print(distance);
  
  Serial.print("\nSpeed");
  Serial.print(speedToTravel);

  Serial.print("\nRadius");
  Serial.print(radius);

  Serial.print("\nTime");
  Serial.print(time);
  
  //Calculate how much the speeds of the outer and inner wheels need to differ
  //in order to get an arc motion.
  float absRad = abs(radius);
  float innerRatio = (absRad - HALF_WHEEL_SPACING_MM) / absRad;
  float outerRatio = (absRad + HALF_WHEEL_SPACING_MM) / absRad;
    
  int innerWheelSpeedMMS = (int) (innerRatio * speedToTravel);
  int outerWheelSpeedMMS = (int) (outerRatio * speedToTravel);
  
  Serial.print("\nInnerRatio = ");
  Serial.print(innerRatio);
  
  Serial.print("\nOuterRatio = ");
  Serial.print(outerRatio);
  
  Serial.print("\nInnerWheelSpeedMMS = ");
  Serial.print(innerWheelSpeedMMS);
  Serial.print("\nOuterWheelSpeedMMS = ");
  Serial.print(outerWheelSpeedMMS);
  
  if (distance < 0) {
    //time = (distance * -1) / speedToTravel * 15;  // treat distance as positive
    //ms = mm / motorpower(mm/ms)
    // 50 cm/100 ms
    // 50000 mm/100ms
    // -> multiplier x50
    // Find out mm/s travelled by robot with different motor powers; apply case statements if not linear?
  
    if (radius < 0) {  // backwards right
      Serial.print("\nARC_BACKWARDS_RIGHT");
      leftBackwardsArc(time, outerRatio);
      rightBackwardsArc(time, innerRatio);
    } else {           // backwards left
      Serial.print("\nARC_BACKWARDS_LEFT");
      leftBackwardsArc(time, innerRatio);
      rightBackwardsArc(time, outerRatio);
    }
  } else {
    if (radius < 0) {  // forwards right
      Serial.print("\nARC_FORWARDS_RIGHT");
      leftForwardsArc(time, outerRatio);
      rightForwardsArc(time, innerRatio);
    } else {           // forwards left
      Serial.print("\nARC_FORWARDS_LEFT");
      leftForwardsArc(time, innerRatio);
      rightForwardsArc(time, outerRatio);
    }
  }
  
  switchOffAfter(LEFT_MOTOR, time);
  switchOffAfter(RIGHT_MOTOR, time); 
  
  
}
void handleKick(){
  Serial.print("\nKICK ");
  int kickSpeed =  readInt();
  Serial.print(kickSpeed);
  
  motorForward(KICKER_MOTOR, kickSpeed);
  // adjust delay according to kick speed?
  switchOffAfter(KICKER_MOTOR, 300);
  kickerIsDown = false;
}

void handleTestKICK() {
  Serial.print("\nTEST_KICK");
  
  int kickUpPower = readInt();    // Power for kicker to move to up position
  int kickDownPower = readInt();  // ... power for down position
  int delayUpTime = readInt();    // Time to switch the motor on for on the upward swing.
  int delayDownTime = readInt();  // ... and the downward swing.
  
  Serial.print("\nPowerUp%: ");
  Serial.print(kickUpPower);
  
  Serial.print("\n PowerDown%: ");
  Serial.print(kickDownPower);
  
  Serial.print("\n DelayUpTime: ");
  Serial.print(delayUpTime);
  
  Serial.print("\n DelayDownTime: ");
  Serial.print(delayDownTime);
  
  Serial.print("\n\n");
  
  if (kickUpPower > 100) { // Range is 0-100%
    kickUpPower = 100;
  }
  if (kickDownPower > 100) {
    kickDownPower = 100;
  }

  motorForward(KICKER_MOTOR, kickUpPower);
  delay(delayUpTime);
  motorBackward(KICKER_MOTOR, kickDownPower);
  delay(delayDownTime);
  motorStop(KICKER_MOTOR);
}

void handleQuit() {
  Serial.print("\nQUIT");
  // Shut off arduino, close connection
  digitalWrite(8, LOW);
  exit(0);
}

void handleCatch() {
  Serial.print("\nCATCH");
  
  // Move kicker to down position, catching the ball
  /* if (k) {
   Serial.print("\nI cannot catch, as the kicker is in the wrong position :(");
    return; // kicker in wrong position
  } */
  motorBackward(KICKER_MOTOR, 50);
  switchOffAfter(KICKER_MOTOR, 500); // initial was 300
  kickerIsDown = true;
}

void handleResetCatcher() {
  Serial.print("\nRESET_CATCHER");
  // Move kicker to up position, ready to catch the ball
  motorForward(KICKER_MOTOR, 50);
  switchOffAfter(KICKER_MOTOR, 1000); // initial is 400
  kickerIsDown = false;
}

void handleTest() {
  Serial.print("\nTesting communications.");
}


// Functions to test readByte(), readInt(), readFloat() and readBoolean() function correctly
void handleTestINT() {
  Serial.print("\nTEST_INT\n");
  
  int testInt = readInt();
  
  Serial.print("\nThe integer read was: ");
  Serial.print(testInt);
  Serial.print("\n\n");
}

void handleTestDOUBLE() {
  Serial.print("\nTEST_DOUBLE\n");

  float testFloat = readFloat();

  Serial.print("\nThe float read was: ");
  Serial.print(testFloat);
  Serial.print("\n\n");
}

void handleTestBOOL() {
  Serial.print("\nTEST_BOOL\n");
  
  boolean testBool = readBoolean();
  
  Serial.print("\nThe boolean read was: ");
  Serial.print(testBool);
  Serial.print("\n\n");
}

void handleTestBYTE() {
  Serial.print("\nTEST_BYTE\n");
  
  boolean testByte = readByte();
  
  Serial.print("\nThe byte read was: ");
  Serial.print(testByte);
  Serial.print("\n\n");
}


//For handling different data types.


//Wait for a given number of bytes to appear in the serial buffer,
//but time out if it is taking too long (to avoid getting stuck in an infinite wait loop).
boolean waitForBytes(int numBytes){
  int timer = 0;
  while(Serial.available() < numBytes && timer < TIMEOUT){
    delay(1);
    timer++;
  }
  boolean finished = Serial.available() >= numBytes;
  if(!finished){
    Serial.print("\nTimed out\n");
  }
  return finished;
}

//Read in a single byte (waiting for time-out if necessary.
byte readByte() { 
  if(waitForBytes(1)){
    return Serial.read();
  }
  return 0;
}

//Read a single 2 byte integer from the serial port.
int readInt() { 
  if(waitForBytes(INT_BYTES)){
    int lhs = Serial.read();
    int rhs = Serial.read();
    int result = (lhs << 8) | rhs ;
    
    /*
    Serial.print("\nLHS = ");
    Serial.print(lhs);
    Serial.print("\n RHS = ");
    Serial.print(rhs);
    Serial.print("\n result = ");
    Serial.print(result);
    Serial.print("\n\n");
    */
    return result;
  }
  return 0;
}

//Reads a single byte from the serial port, and converts it to a boolean. 
//Zero is false, everything else is true.
boolean readBoolean() { // readBoolean from input
  if(waitForBytes(BOOLEAN_BYTES)){
    return (Serial.read() != 0);
  }
  return false;
}

typedef union {
  float asFloat;
  byte asBytes[4];
} FloatBytes;

//Read a single 4 byte floating point number from the serial port
double readFloat(){
  FloatBytes fb;
  if(waitForBytes(DOUBLE_BYTES)){
    Serial.readBytes(fb.asBytes, DOUBLE_BYTES);
    return fb.asFloat;
  }
  return 0.0;
}

//Movement primatives for time and speed.

void leftForwards(int timeMs, int speedMMS){
  int leftPower = getLeftPower(timeMs, speedMMS, true);
  motorBackward(LEFT_MOTOR, 60);
}

void rightForwards(int timeMs, int speedMMS){
  int rightPower = getRightPower(timeMs, speedMMS, true);
  motorBackward(RIGHT_MOTOR, 60);
}

void leftBackwards(int timeMs, int speedMMS){
  int leftPower = getLeftPower(timeMs, speedMMS, false);
  motorForward(LEFT_MOTOR, 60);
}

void rightBackwards(int timeMs, int speedMMS){
  int rightPower = getRightPower(timeMs, speedMMS, false);
  motorForward(RIGHT_MOTOR, 60);
}


//Movement primatives for arc (Speeds of wheels have to be different)
void leftForwardsArc(int timeMs, float ratio){
  int leftPower = (int)(60 * ratio);
  motorBackward(LEFT_MOTOR, leftPower);
}

void rightForwardsArc(int timeMs, float ratio){
  int rightPower = (int)(60 * ratio);
  motorBackward(RIGHT_MOTOR, rightPower);
}

void leftBackwardsArc(int timeMs, float ratio){
  int leftPower = (int)(60 * ratio);
  motorForward(LEFT_MOTOR, leftPower);
}

void rightBackwardsArc(int timeMs, float ratio){
  int rightPower = (int)(60 * ratio);
  motorForward(RIGHT_MOTOR, rightPower);
}


int getLeftPower(int timeMs, int speedMMS, boolean forwards){
  int power = getPower(timeMs, speedMMS);
  Serial.print("\nLeft Power = ");
  Serial.print(power);
  return power;
}

int getRightPower(int timeMs, int speedMMS, boolean forwards){
  int power = getPower(timeMs, speedMMS);
  Serial.print("\nRight Power = ");
  Serial.print(power);
  return power;
}

int getTimeMs(int distanceMM, int speedMMS){
  float timeSeconds = (float) distanceMM / (float) speedMMS;
  int timeMs = (int) 1.5 * abs((1000 * timeSeconds));
  timeMs = min(5000, max(180, timeMs));
  return timeMs;
}

int getTimeFromDistance(int distance, boolean isForward, int power){
  
  distance = abs(distance);
  power = 60;
  int timeMs;
  float frac;
    
  if (distance < 10){
    return 0.0;
  }
  
  if (distance <= 100){
    frac = 1.75;
  }else if (distance > 100 && distance <= 150){
    frac = 1.7;
  }else if (distance > 150 && distance <= 200){
    frac = 1.7;
  }else if (distance > 200 && distance <= 250){
    frac = 1.3;
  }else if (distance > 250 && distance <= 300){
    frac = 1.2;
  }else if (distance > 300 && distance <= 400){
    frac = 1.2;
  }else if (distance > 400 && distance <= 500){
    frac = 1.15;
  }else{
    frac = 1.1;
  }
   
  
  
  float mult = (((frac - 1) /(-180)) * distance) + frac;
  
  if (isForward) {
    //timeMs = -(0.0069296811*distance*distance) + (3.8960706204*distance) + 102.2026324623;  Previous Quadratic Equation
    //if (distance > 300)
    //distance = distance + (-4/5 * distance) + 20;
    //distance = distance + ((-1/1250*distance)+0.2)*distance; //magical formula which compensates for linear equation errors
    distance = distance * mult;
    timeMs = (2.4320228393 * distance) + 138.0398556193;
  } else {
    distance = distance * mult;
    //distance = distance + ((-4/5 * distance))/  100*distance;
    //timeMs = -(0.0064246103*distance*distance) + (3.8173355044*distance) + 107.5654125931;  Previous Quadratic Equation
    //distance = distance + ((-1/1000*distance)+0.3)*distance; //magical formula which compensates for linear equation errors
    timeMs = (2.4510739857 * distance) + 137.8878281623;
  }  
  return timeMs;
}

int getPower(int timeMs, int speedMMS){
  float speedCmS = ((float)speedMMS)/10;
  Serial.print("\n SpeedCmS = ");
  Serial.print(speedCmS);
  
  float maxSpeedCmS = 60.0;
  
  float percentageOfMax = min(100, 100 * (speedCmS / maxSpeedCmS));

  return percentageOfMax;
}


                                               
void doTimers(){
  unsigned long currentTime = millis();
  long timeDelta = currentTime - prevTime;
  
  prevTime = currentTime;
  
  //Serial.print("\nTimeDelta");
  //Serial.print(timeDelta);

  if(leftTimer > timeDelta){
    leftTimer -= timeDelta;
  }else{
    leftTimer = 0;
    motorStop(LEFT_MOTOR);
  }
  
  if(rightTimer > timeDelta){
    rightTimer -= timeDelta;
  }else{
    rightTimer = 0;
    motorStop(RIGHT_MOTOR);
  }
  
  if(kickTimer > timeDelta){
    kickTimer -= timeDelta;
  }else{
    kickTimer = 0;
    motorStop(KICKER_MOTOR);
  }
}

void switchOffAfter(int motorNumber, int timeMs){
  switch(motorNumber){
  case LEFT_MOTOR : leftTimer = timeMs; break;
  case RIGHT_MOTOR : rightTimer = timeMs; break;
  case KICKER_MOTOR : kickTimer = timeMs; break;
  }
}  
